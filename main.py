import re
import os
import glob
import time
import logging
import traceback
import pandas as pd
from win32com.client import *
from PyQt5.QtGui import QPixmap
from openpyxl import load_workbook
from joblib import Parallel, delayed
from configparser import ConfigParser
from datetime import datetime, timedelta
from PyQt5 import QtWidgets, QtGui, QtCore
from serial import Serial, SerialException
from ui.main_window.main_window import Ui_MainWindow
from ui.main_window.about_window import Ui_AboutWindow
from openpyxl.styles import Font, Alignment, PatternFill
from ui.main_window.report_window import Ui_report_window
from ui.main_window.settings_window import Ui_settings_window
from ui.main_window.edit_vehicle import Ui_edit_vehicle_window
from ui.main_window.edit_standard import Ui_edit_standard_window
from PyQt5.QtWidgets import QHeaderView, QTableView, QMessageBox, QFileDialog
from ui.funct.comm import RequestByte, mcu_enable_sensor, mcu_check_sensor, mcu_check_mcu_status, \
    mcu_turn_on_ecu_power, mcu_turn_off_ecu_power, mcu_turn_on_rev_power, mcu_turn_off_rev_power, \
    ProgrammablePowerSupply, mcu_end_operation_cmd
from PyQt5.QtCore import Qt, QDateTime, QRect, QAbstractTableModel, QThread, pyqtSignal, QVariant, QDate

# USAGE
USE_IF_MCU = True
USE_MOTOR = True
USE_POWER_SUPPLY = False
BYPASS_MCU_STATUS = True
VERBOSE_SYSTEM_LOG = False

# PATH
EXE_FILE_NAME = f'{os.path.splitext(os.path.basename(__file__))[0]}.exe'
VEHICLE_CONFIG_PATH = "config/vehicle/"
DEVICE_CONFIG_FILE = "config/connectedDevices/devices_config.ini"
STANDARD_CONFIG_FILE_PATH = "config/standard/"
LOG_DIR = 'logs/operation logs'
FORMAT_REPORT_PATH = 'config/reportFormat/report_format.xlsx'
SOFTWARE_INFO_PATH = 'config/softwareInfo/info.ini'
SYSTEM_LOG_FILE_PATH = os.path.expanduser('~/Documents/RPAS Simulator/logs/')
REPORT_GENERATED_FOLDER_PATH = os.path.expanduser('~/Documents/RPAS Simulator/report_output/')


# PyInstaller snippet
if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
    os.chdir(sys._MEIPASS)


def init_log():
    # Update version GUI
    update_version_gui()

    config_object = ConfigParser()
    config_object.read(SOFTWARE_INFO_PATH)
    version = config_object.get("INFO", "version")

    motor_usage = "Yes" if USE_MOTOR else "No"
    mcu_usage = "Yes" if USE_IF_MCU else "No"
    power_supply_usage = "Yes" if USE_POWER_SUPPLY else "No"
    bypass_mcu_status = "Yes" if BYPASS_MCU_STATUS else "No"

    log_format = "[%(asctime)s] %(levelname)s - %(funcName)s:%(lineno)d - %(message)s"
    log_dir = SYSTEM_LOG_FILE_PATH
    # Check dir log
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    filename = log_dir + "system_log.log"
    logging.basicConfig(filename=filename, filemode='a', format=log_format,
                        datefmt='%d-%m-%y %H:%M:%S', level=logging.INFO)
    logging.info("".center(65, "="))
    logging.info("RPAS SIMULATOR".center(65, " "))
    logging.info("".center(65, "-"))
    logging.info(f"\tProgram Version : {version}")
    if VERBOSE_SYSTEM_LOG:
        logging.info(f"\tUse IF MCU : {mcu_usage}")
        logging.info(f"\tUse motor : {motor_usage}")
        logging.info(f"\tUse power supply : {power_supply_usage}")
        logging.info(f"\tBypass MCU status : {bypass_mcu_status}")
    logging.info("".center(65, "="))


def update_version_gui():
    # Update version UI
    exe_path = EXE_FILE_NAME
    version_str = get_version_number(exe_path)
    config_object = ConfigParser()
    config_object.read(SOFTWARE_INFO_PATH)
    config_object['INFO']['version'] = version_str
    with open(SOFTWARE_INFO_PATH, 'w') as configfile:
        config_object.write(configfile)


def get_version_number(f_path):
    information_parser = Dispatch("Scripting.FileSystemObject")
    vers = information_parser.GetFileVersion(f_path)
    vers = vers.split(".")
    vers = vers[:-1]
    vers = ".".join(vers)
    return vers


def excepthook(exc_type, exc_value, exc_tb):
    error_msg = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    logging.error(error_msg)
    QtWidgets.QApplication.quit()
    # or QtWidgets.QApplication.exit(0)


class MainWindow(QtWidgets.QMainWindow):
    """
    MAIN WINDOW UI
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        """
        INIT GUI WINDOW
        """
        system_log = "Building interface..."
        logging.info(system_log)
        print(system_log)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        """
        INIT CONFIGURATION DATA/VARIABLES
        """
        system_log = "Initialize configuration data..."
        logging.info(system_log)
        print(system_log)
        # Init logs data
        self.save_to_csv = {}
        self.log_df = self.init_operation_log_dataframe()
        # Init icon/img path
        self.coverage_map_path = "img/background/coverage_map.png"
        self.green_circle_icon_path = "img/icon/green_icon.png"
        self.red_circle_icon_path = "img/icon/red_icon.png"
        self.yellow_circle_icon_path = "img/icon/yellow_icon.png"
        self.grey_circle_icon_path = "img/icon/grey_icon.png"
        self.sensor_width_reference_path = "img/background/sensor_width.png"
        self.sensor_distance_rear_path = "img/background/sensor_distance_rear.png"
        self.run_icon_path = "img/icon/run_icon.png"
        self.pause_icon_path = "img/icon/pause_icon.png"
        self.stop_icon_path = "img/icon/stop_icon.png"
        self.run_disable_icon_path = "img/icon/run_disable_icon.png"
        self.pause_disable_icon_path = "img/icon/pause_disable_icon.png"
        self.stop_disable_icon_path = "img/icon/stop_disable_icon.png"
        self.edit_button_icon_path = "img/icon/edit_icon.png"
        self.standard_init_path = "config/standard/init.ini"
        self.error_circle_icon_path = "img/icon/error_icon.png"
        self.green_check_icon_path = "img/icon/tick-circle.png"
        self.red_check_icon_path = "img/icon/cross-circle.png"
        self.quit_icon_path = "img/icon/cross.png"
        self.about_icon_path = "img/icon/question.png"
        self.report_icon_path = "img/icon/report--arrow.png"
        self.settings_icon_path = "img/icon/wrench-screwdriver.png"
        self.move_motor_icon_path = "img/icon/arrow-move.png"
        self.motor_home_icon_path = "img/icon/home--arrow.png"
        # Init attribute button list
        ui_keys = list(self.ui.__dict__.keys())
        self.tp_horizontal_btn_list = [i for i in ui_keys if 'tp_horizontal_' in i]
        self.tp_horizontal_btn_list = sorted(self.tp_horizontal_btn_list, key=lambda x: int(x.split("_")[-1]))
        self.tp_vertical_btn_list = [i for i in ui_keys if 'tp_vertical_' in i]
        self.tp_vertical_btn_list = sorted(self.tp_vertical_btn_list, key=lambda x: int(x.split("_")[-1]))
        # Init device config
        self.device_config = {}
        self.init_device_config()
        # Init vehicle configuration
        self.vehicle_fresh_startup = True
        self.vehicle_dataframe = pd.DataFrame()
        self.init_vehicle_config()
        # Init standard configuration
        self.standard_config = {}
        self.standard_dataframe = pd.DataFrame()
        self.init_standard_config()
        # Init test thread, worker
        self.test_thread = QThread()
        self.test_worker = TestWorker()
        # Init move motor thread, worker
        self.move_motor_thread = QThread()
        self.move_motor_worker = MoveMotorWorker()
        # Init motor home thread, worker
        self.motor_home_thread = QThread()
        self.motor_home_worker = MotorHomeWorker()
        # Init test to run
        self.test_to_run_list = []
        self.get_test_to_run_list(0)
        # Init spinbox
        self.x_spinbox = self.ui.coordinate_x_sbox.value()
        self.y_spinbox = self.ui.coordinate_y_sbox.value()
        # Init horizontal total test point to test
        self.total_horizontal_test_point_to_test = 0
        # Init vertical total test point to test
        self.total_vertical_test_a_point_to_test = 0
        self.total_vertical_test_b_point_to_test = 0
        self.total_vertical_test_c_point_to_test = 0
        # Init limit adding test point
        self.horizontal_limit_add_test_point = 2570
        self.vertical_limit_add_test_point = 2394
        # Init horizontal id adding/remove test point button
        self.horizontal_id_add_remove_test_point_btn_list = [1, 15, 30, 44]
        # Init vertical id adding/remove test point button
        self.vertical_id_add_remove_test_point_btn_list = [1, 15, 16, 30]
        # Init vertical id test C button
        self.vertical_id_test_c_point_btn_list = [4, 12, 19, 27]
        # Init position vertical id test C button
        self.top_right_c_point_id = [self.vertical_id_test_c_point_btn_list[0]]
        self.top_left_c_point_id = [self.vertical_id_test_c_point_btn_list[1]]
        self.bottom_left_c_point_id = [self.vertical_id_test_c_point_btn_list[2]]
        self.bottom_right_c_point_id = [self.vertical_id_test_c_point_btn_list[3]]
        # Init require test type C for vertical
        self.require_test_c = False
        # Init custom point test C
        self.vertical_custom_point = 0
        # Init initial x-axis in (mm)
        self.initial_x_axis_mm = 800
        # Init interval (mm) x-axis based on coverage map
        self.initial_interval_x_axis = 200
        # Init interval (mm) y-axis based on coverage map
        self.initial_interval_y_axis = 100
        # Init initial x, y coordinate for test type C test point (pixel)
        self.initial_x_coordinate_left = 249
        self.initial_x_coordinate_right = 854
        self.initial_y_coordinate_top = 57
        self.initial_y_coordinate_bottom = 407
        # Init pixel increment for every x-axis 200mm interval
        self.x_pixel_offset = 76
        # Init pixel increment for every y-axis 100mm interval
        self.y_pixel_offset = 35
        # Init origin x, y coordinate for tp manual button
        self.initial_coordinate_tp_manual_btn_mm = (0, 400)
        self.initial_coordinate_tp_manual_btn_pixel = (self.ui.tp_manual_btn.x(), self.ui.tp_manual_btn.y())
        # Init vertical test to test dict memory
        self.vertical_test_a_to_test_dict = {}
        self.vertical_test_b_to_test_dict = {}
        self.vertical_test_c_to_test_dict = {}
        for btn in self.tp_vertical_btn_list:
            btn = getattr(self.ui, btn)
            btn_name = btn.objectName()
            self.vertical_test_a_to_test_dict[btn_name] = True
            self.vertical_test_b_to_test_dict[btn_name] = True
            self.vertical_test_c_to_test_dict[btn_name] = True
        # Vertical sub test to test parameter
        self.vertical_sub_test_to_test_dict = {"A": [], "B": [], "C": []}
        # Current horizontal result
        self.current_horizontal_result = ""
        # Current vertical result
        self.current_vertical_result = ""
        # Current detected coverage percentage
        self.current_detected_coverage = 0.0
        # Init flag if test was running
        self.test_running = False
        # Init interrupt flag when test was running
        self.interrupt_requested = False
        # Init horizontal current total detected test point
        self.current_total_detected_test_point = 0
        # Init finished motor respond
        self.motor_respond = False
        # Init power supply com protocol
        self.power_supply = ProgrammablePowerSupply()
        if USE_POWER_SUPPLY:
            self.power_supply.set_port(com_port=self.device_config["power_supply_com_port"],
                                       baudrate=int(self.device_config["power_supply_baud_rate"]),
                                       timeout=float(self.device_config["power_supply_timeout"]))
        # Prep column list for log csv
        with open('config/logFormat/format_csv_log.txt') as f:
            each_line = f.readlines()
            self.csv_cols = [line.rstrip() for line in each_line]

        """
        INIT COMMUNICATION PORT
        """
        system_log = "Initialize communication port..."
        logging.info(system_log)
        print(system_log)
        self.motor_serial = Serial()
        self.init_motor_port()
        self.motor_serial_changed = False
        # Flag if motor NEVER been home since startup program
        self.initial_move_motor_home = False

        """
        INIT WIDGET/FORMS WINDOW
        """
        system_log = "Initialize widgets..."
        logging.info(system_log)
        print(system_log)
        # Init settings window
        self.settings_window = SettingsWindow(self.vehicle_dataframe, self.standard_dataframe, self)
        # Init report window
        self.report_window = ReportWindow(self)
        # Init switch button
        self.ecu_switch_btn = PowerSwitch()
        self.rev_gear_switch_btn = PowerSwitch()
        # Init others
        self.special_delegate = SpecialStyledItemDelegate()
        # Configure UI
        self.config_ui()
        # Init about window
        self.about_window = AboutWindow(self)

        """
        INIT CONNECTING SIGNALS/SLOT
        """
        system_log = "Connecting signal and slot..."
        logging.info(system_log)
        print(system_log)
        # Connect edit vehicle button
        self.ui.settings_menubar.triggered.connect(lambda: self.settings_window.show_window())
        # Connect generate report clicked
        self.ui.generate_report_menubar.triggered.connect(lambda: self.report_window.show_window())
        # Connect vehicle family cbox
        self.ui.vehicle_family_cbox.currentIndexChanged.connect(self.on_vehicle_family_cbox_changed)
        # Connect vehicle id cbox
        self.ui.vehicle_id_cbox.currentIndexChanged.connect(self.on_vehicle_id_cbox_changed)
        # Connect standard cbox
        self.ui.standard_cbox.currentIndexChanged.connect(self.on_standard_cbox_changed)
        # Connect switch button
        self.ecu_switch_btn.clicked.connect(lambda: self.ecu_switch_pressed())
        self.rev_gear_switch_btn.clicked.connect(lambda: self.rev_gear_switch_pressed())
        # Connect move motor button
        self.ui.move_motor_btn.clicked.connect(self.on_move_motor_clicked)
        # Connect motor home button
        self.ui.motor_home_btn.clicked.connect(self.on_motor_home_clicked)
        # Connect all Test Point button
        for btn in self.tp_horizontal_btn_list:
            btn = getattr(self.ui, btn)
            btn.clicked.connect(self.horizontal_test_point_clicked)
        for btn in self.tp_vertical_btn_list:
            btn = getattr(self.ui, btn)
            btn.clicked.connect(self.vertical_test_point_clicked)
        self.ui.tp_manual_btn.clicked.connect(self.manual_test_point_clicked)
        # Connect test type comboBox
        self.on_test_type_cbox_changed()
        self.ui.test_type_cbox.currentIndexChanged.connect(self.on_test_type_cbox_changed)
        # Connect vertical test type combobox
        self.on_vertical_test_type_cbox_changed()
        self.ui.vertical_test_type_cbox.currentIndexChanged.connect(self.on_vertical_test_type_cbox_changed)
        # Connect run button
        self.ui.run_test_btn.clicked.connect(self.run_button_clicked)
        # Connect pause button
        self.ui.pause_test_btn.clicked.connect(self.pause_button_clicked)
        # Connect stop button
        self.ui.stop_test_btn.clicked.connect(self.stop_button_clicked)
        # Connect spinbox
        self.ui.coordinate_x_sbox.valueChanged.connect(self.check_current_spinbox)
        self.ui.coordinate_y_sbox.valueChanged.connect(self.check_current_spinbox)
        # Connect quit menubar
        self.ui.quit_menubar.triggered.connect(lambda: self.quit_window())
        # Connect about menu bar
        self.ui.about_menubar.triggered.connect(lambda: self.show_about_software())

        system_log = "Starting procedure completed."
        logging.info(system_log)
        print(system_log)

    def quit_window(self):
        button_clicked = self.show_dialog_popup("Are you sure want to quit?")

        if button_clicked == QMessageBox.Yes:
            self.close()
        else:
            pass

    def show_about_software(self):
        self.about_window.show_window()

    def init_motor_port(self):
        if USE_MOTOR:
            # if not self.motor_serial.isOpen():
            # Open motor port
            try:
                self.motor_serial = Serial(self.device_config['motor_com_port'],
                                           int(self.device_config['motor_baud_rate']))
                time.sleep(float(self.device_config['motor_start_del']))
            except SerialException:
                self.motor_serial = Serial()
        else:
            self.motor_serial = Serial()

    def check_motor_port(self):
        if USE_MOTOR:
            serial_before_open_new = self.motor_serial
            try:
                self.motor_serial = Serial(self.device_config['motor_com_port'],
                                           int(self.device_config['motor_baud_rate']))
                time.sleep(float(self.device_config['motor_start_del']))
                self.motor_serial_changed = True
            except SerialException:
                self.motor_serial = serial_before_open_new
                self.motor_serial_changed = False
        else:
            self.motor_serial = Serial()
            self.motor_serial_changed = False

    def init_device_config(self):
        # Prepare connected devices settings
        config_object = ConfigParser()
        config_object.read(DEVICE_CONFIG_FILE)
        self.device_config = {}
        for section in config_object.sections():
            section_list = config_object.items(section)
            for each_key, each_value in section_list:
                self.device_config[each_key] = each_value

    def check_current_spinbox(self):
        self.x_spinbox = self.ui.coordinate_x_sbox.value()
        self.y_spinbox = self.ui.coordinate_y_sbox.value()

        # Calculate offset increment pixel per mm
        btn = self.ui.tp_manual_btn
        x_coordinate = self.x_spinbox
        y_coordinate = self.y_spinbox
        width = btn.geometry().width()
        height = btn.geometry().height()
        x_interval = self.initial_interval_x_axis
        x_offset = self.x_pixel_offset
        y_interval = self.initial_interval_y_axis
        y_offset = self.y_pixel_offset
        x_pixel_per_mm = x_offset / x_interval
        y_pixel_per_mm = y_offset / y_interval

        # Get pixel increment
        x_diff_increment_mm = x_coordinate - self.initial_coordinate_tp_manual_btn_mm[0]
        y_diff_increment_mm = y_coordinate - self.initial_coordinate_tp_manual_btn_mm[1]
        x_pixel_increment = x_pixel_per_mm * abs(x_diff_increment_mm)
        y_pixel_increment = y_pixel_per_mm * abs(y_diff_increment_mm)

        # Calculate new coordinate x-axis, y-axis
        if x_diff_increment_mm < 0:
            new_x_axis = self.initial_coordinate_tp_manual_btn_pixel[0] - x_pixel_increment
        else:
            new_x_axis = self.initial_coordinate_tp_manual_btn_pixel[0] + x_pixel_increment
        new_y_axis = self.initial_coordinate_tp_manual_btn_pixel[1] - y_pixel_increment

        # Move button
        btn.setGeometry(new_x_axis, new_y_axis, width, height)

    def get_test_to_run_list(self, test_type_index):
        self.test_to_run_list = []
        if test_type_index == 0:  # Horizontal
            for btn in self.tp_horizontal_btn_list:
                btn = getattr(self.ui, btn)
                button_stylesheet = btn.styleSheet()
                border_image_style = button_stylesheet.split("border-image: url")[-1]
                border_image_path = re.sub(r"[([{})\]]", "", str(border_image_style))
                if border_image_path == self.grey_circle_icon_path:
                    self.test_to_run_list.append(False)
                else:
                    self.test_to_run_list.append(True)
        elif test_type_index == 1:  # Vertical
            for btn in self.tp_vertical_btn_list:
                btn = getattr(self.ui, btn)
                button_name = btn.objectName()
                visibility = btn.isVisible()

                # button_stylesheet = btn.styleSheet()
                # border_image_style = button_stylesheet.split("border-image: url")[-1]
                # border_image_path = re.sub(r"[([{})\]]", "", str(border_image_style))

                if self.ui.vertical_test_type_cbox.currentIndex() == 0:  # Test A
                    if self.vertical_test_a_to_test_dict[button_name] and visibility:
                        self.test_to_run_list.append(True)
                    else:
                        self.test_to_run_list.append(False)
                elif self.ui.vertical_test_type_cbox.currentIndex() == 1:  # Test B
                    if self.vertical_test_b_to_test_dict[button_name] and visibility:
                        self.test_to_run_list.append(True)
                    else:
                        self.test_to_run_list.append(False)
                else:  # Test C
                    if self.vertical_test_c_to_test_dict[button_name] and visibility:
                        self.test_to_run_list.append(True)
                    else:
                        self.test_to_run_list.append(False)
        else:  # Manual
            pass

        test_to_run = self.test_to_run_list

        return test_to_run

    def update_test_point_to_test(self, test_to_run):
        test_type_index = self.ui.test_type_cbox.currentIndex()
        if test_type_index == 0:  # Horizontal
            for idx, btn in enumerate(self.tp_horizontal_btn_list):
                btn = getattr(self.ui, btn)
                button_stylesheet = btn.styleSheet()
                border_image_style = button_stylesheet.split("border-image: url")[-1]
                border_image_path = re.sub(r"[([{})\]]", "", str(border_image_style))
                if border_image_path == self.yellow_circle_icon_path:
                    test_to_run[idx] = True
                elif border_image_path == self.grey_circle_icon_path:
                    test_to_run[idx] = False
                else:
                    test_to_run[idx] = "SKIP"
        elif test_type_index == 1:
            for idx, btn in enumerate(self.tp_vertical_btn_list):
                btn = getattr(self.ui, btn)
                button_stylesheet = btn.styleSheet()
                border_image_style = button_stylesheet.split("border-image: url")[-1]
                border_image_path = re.sub(r"[([{})\]]", "", str(border_image_style))
                if border_image_path == self.yellow_circle_icon_path:
                    test_to_run[idx] = True
                elif border_image_path == self.grey_circle_icon_path:
                    test_to_run[idx] = False
                else:
                    test_to_run[idx] = "SKIP"
        else:
            pass

        return test_to_run

    def horizontal_test_point_clicked(self):
        button = self.sender()
        if isinstance(button, QtWidgets.QPushButton):
            button_name = button.objectName()
            system_log = "Pressed %s!" % button_name
            logging.info(system_log)
            print(system_log)
            button_stylesheet = button.styleSheet()
            border_image_style = button_stylesheet.split("border-image: url")[-1]
            border_image_path = re.sub(r"[([{})\]]", "", str(border_image_style))
            if border_image_path == self.grey_circle_icon_path:
                button.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                self.total_horizontal_test_point_to_test += 1
            else:
                button.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                self.total_horizontal_test_point_to_test -= 1

            # Update in result table
            self.ui.test_result_tab.item(0, 2).setText(str(self.total_horizontal_test_point_to_test))

    def vertical_test_point_clicked(self):
        button = self.sender()
        if isinstance(button, QtWidgets.QPushButton):
            button_name = button.objectName()
            system_log = "Pressed %s!" % button_name
            logging.info(system_log)
            print(system_log)
            button_stylesheet = button.styleSheet()
            border_image_style = button_stylesheet.split("border-image: url")[-1]
            border_image_path = re.sub(r"[([{})\]]", "", str(border_image_style))
            if border_image_path == self.grey_circle_icon_path:
                button.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                to_test = True
                if self.ui.vertical_test_type_cbox.currentIndex() == 0:
                    self.total_vertical_test_a_point_to_test += 1
                elif self.ui.vertical_test_type_cbox.currentIndex() == 1:
                    self.total_vertical_test_b_point_to_test += 1
                else:
                    self.total_vertical_test_c_point_to_test += 1
            else:
                button.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                to_test = False
                if self.ui.vertical_test_type_cbox.currentIndex() == 0:
                    self.total_vertical_test_a_point_to_test -= 1
                elif self.ui.vertical_test_type_cbox.currentIndex() == 1:
                    self.total_vertical_test_b_point_to_test -= 1
                else:
                    self.total_vertical_test_c_point_to_test -= 1

            # Check current vertical test type
            if self.ui.vertical_test_type_cbox.currentIndex() == 0:  # Test A
                self.vertical_test_a_to_test_dict[button_name] = to_test
                # Update in result table
                if self.require_test_c:
                    pass
                else:
                    self.ui.test_result_tab.item(1, 2).setText(str(self.total_vertical_test_a_point_to_test))
            elif self.ui.vertical_test_type_cbox.currentIndex() == 1:  # Test B
                self.vertical_test_b_to_test_dict[button_name] = to_test
                # Update in result table
                if self.require_test_c:
                    self.ui.test_result_tab.item(1, 2).setText(str(self.total_vertical_test_b_point_to_test))
                else:
                    self.ui.test_result_tab.item(2, 2).setText(str(self.total_vertical_test_b_point_to_test))
            else:  # Test C
                self.vertical_test_c_to_test_dict[button_name] = to_test
                # Update in result table
                if self.require_test_c:
                    self.ui.test_result_tab.item(2, 2).setText(str(self.total_vertical_test_c_point_to_test))
                else:
                    pass

    @staticmethod
    def manual_test_point_clicked():
        system_log = "Manual btn clicked"
        logging.info(system_log)
        print(system_log)

    def on_move_motor_clicked(self):
        system_log = "Move motor clicked"
        logging.info(system_log)
        print(system_log)
        # Check if motor serial opened
        self.check_motor_port()

        # Disabled widget
        self.ui.coordinate_x_sbox.setDisabled(True)
        self.ui.coordinate_y_sbox.setDisabled(True)
        self.ui.move_motor_btn.setDisabled(True)
        self.ui.motor_home_btn.setDisabled(True)

        # Update status
        self.ui.result_lab.setText("MOVING MOTOR")
        self.ui.result_lab.setStyleSheet("background-color: rgb(255, 255, 0)")  # Yellow

        # Run thread
        self.run_move_motor_thread()

    def on_motor_home_clicked(self):
        system_log = "Motor home clicked"
        logging.info(system_log)
        print(system_log)
        # Check if motor serial opened
        self.check_motor_port()

        # Disabled widget
        self.ui.coordinate_x_sbox.setDisabled(True)
        self.ui.coordinate_y_sbox.setDisabled(True)
        self.ui.move_motor_btn.setDisabled(True)
        self.ui.motor_home_btn.setDisabled(True)

        # Update status
        self.ui.result_lab.setText("MOVING MOTOR")
        self.ui.result_lab.setStyleSheet("background-color: rgb(255, 255, 0)")  # Yellow

        # Run thread
        self.run_motor_home_thread()

    def run_move_motor_thread(self):
        self.move_motor_thread = QThread()
        self.move_motor_worker = MoveMotorWorker(x_spinbox=self.x_spinbox, y_spinbox=self.y_spinbox,
                                                 device_config=self.device_config, motor_serial=self.motor_serial,
                                                 motor_serial_changed=self.motor_serial_changed,
                                                 initial_move_motor_home=self.initial_move_motor_home)
        self.move_motor_worker.moveToThread(self.move_motor_thread)

        # Connect signals and slots
        self.move_motor_thread.started.connect(self.move_motor_worker.run)
        self.move_motor_worker.finished.connect(self.move_motor_thread.quit)
        self.move_motor_worker.finished.connect(self.move_motor_worker.deleteLater)
        self.move_motor_thread.finished.connect(self.move_motor_thread.deleteLater)
        # self.move_motor_worker.motor_moving_progress.connect(self.update_motor_moving_progress)
        self.move_motor_worker.motor_respond_progress.connect(self.update_motor_moving_finished)
        self.move_motor_worker.motor_home_first.connect(self.motor_home_first_notification)
        self.move_motor_worker.initial_motor_home_respond.connect(self.update_initial_motor_home_flag)

        # Start the thread
        self.move_motor_thread.start()

        # After thread finished
        self.move_motor_thread.finished.connect(self.after_move_motor_thread_finished)

    def run_motor_home_thread(self):
        self.motor_home_thread = QThread()
        self.motor_home_worker = MotorHomeWorker(device_config=self.device_config, motor_serial=self.motor_serial)
        self.motor_home_worker.moveToThread(self.motor_home_thread)

        # Connect signals and slots
        self.motor_home_thread.started.connect(self.motor_home_worker.run)
        self.motor_home_worker.finished.connect(self.motor_home_thread.quit)
        self.motor_home_worker.finished.connect(self.motor_home_worker.deleteLater)
        self.motor_home_thread.finished.connect(self.motor_home_thread.deleteLater)
        # self.motor_home_worker.motor_moving_progress.connect(self.update_motor_moving_progress)
        self.motor_home_worker.motor_respond_progress.connect(self.update_motor_moving_finished)
        self.motor_home_worker.initial_motor_home_respond.connect(self.update_initial_motor_home_flag)

        # Start the thread
        self.motor_home_thread.start()

        # After thread finished
        self.motor_home_thread.finished.connect(self.after_move_motor_thread_finished)

    def update_initial_motor_home_flag(self, motor_home_respond):
        if motor_home_respond:
            self.initial_move_motor_home = True
        else:
            pass

    def sensor_faulty_notification(self, sensor_faulty):
        if sensor_faulty:
            self.ui.statusbar.showMessage("Error due to ECU fault!", 1000)

    def motor_home_first_notification(self, motor_home_emitted):
        motor_home_required = motor_home_emitted[0]
        motor_home_type_msg = motor_home_emitted[1]

        if motor_home_required:
            # 0: Motor home due to connection changed
            if motor_home_type_msg == 0:
                self.show_info_popup("Motor connect/disconnect detected!\n" 
                                     "Requiring motor to go home first.")

            # Motor home due to NEVER been home since program started
            elif motor_home_type_msg == 1:
                self.show_info_popup("Motor NEVER been home since program started.\n"
                                     "Requiring motor to go home first.")

            else:
                self.show_info_popup("UNKNOWN MESSAGE")
        else:
            pass

    def update_motor_moving_finished(self, response):
        if response:
            self.motor_respond = True
        else:
            self.motor_respond = False

    def after_move_motor_thread_finished(self):
        # Update status
        self.ui.result_lab.setText("READY")
        self.ui.result_lab.setStyleSheet("background-color: rgb(0, 128, 255)")

        if self.motor_respond:
            self.show_info_popup("Moving motor successful!")
        else:
            self.show_alert_popup("Moving motor fail!")

        # Enable widget
        self.ui.coordinate_x_sbox.setEnabled(True)
        self.ui.coordinate_y_sbox.setEnabled(True)
        self.ui.move_motor_btn.setEnabled(True)
        self.ui.motor_home_btn.setEnabled(True)

    def ecu_switch_pressed(self):
        if self.ecu_switch_btn.isChecked():
            # Power supply
            if USE_POWER_SUPPLY:
                self.power_supply.set_output_voltage(channel=1, milli_volt=24000)
                self.power_supply.turn_on_output()

            self.switch_on_ecu()
            system_log = "ECU ON"
            logging.info(system_log)
            print(system_log)
        else:
            # Power supply
            if USE_POWER_SUPPLY:
                self.power_supply.turn_off_output()

            self.switch_off_ecu()
            system_log = "ECU OFF"
            logging.info(system_log)
            print(system_log)

    def rev_gear_switch_pressed(self):
        if self.rev_gear_switch_btn.isChecked():
            self.switch_on_rev()
            system_log = "Reverse gear ON"
            logging.info(system_log)
            print(system_log)
        else:
            self.switch_off_rev()
            system_log = "Reverse gear OFF"
            logging.info(system_log)
            print(system_log)

    def switch_on_ecu(self):
        if USE_IF_MCU:
            mcu_cmd = self.device_config['cmd_switch_on_ecu']
            mcu_port = self.device_config['mcu_com_port']
            mcu_baud_rate = self.device_config['mcu_baud_rate']
            mcu_start_false_cmd = self.device_config['cmd_start_false']
            mcu_repetition_request = self.device_config['mcu_repetition_request_time']
            timeout_set = self.device_config['tmcu_switch_on_ecu']
            mcu_resp_len = self.device_config['rlmcu_switch_on_ecu']
            data_pos = ['true_false']

            mcu_respond = mcu_turn_on_ecu_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
                                                mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        else:
            mcu_respond = True

        if not mcu_respond:
            self.show_alert_popup("Fail to switch on!")
            self.ecu_switch_btn.setChecked(False)
        else:
            pass

    def switch_off_ecu(self, fail_popup_notification=True):
        if USE_IF_MCU:
            mcu_cmd = self.device_config['cmd_switch_off_ecu']
            mcu_port = self.device_config['mcu_com_port']
            mcu_baud_rate = self.device_config['mcu_baud_rate']
            mcu_start_false_cmd = self.device_config['cmd_start_false']
            mcu_repetition_request = self.device_config['mcu_repetition_request_time']
            timeout_set = self.device_config['tmcu_switch_off_ecu']
            mcu_resp_len = self.device_config['rlmcu_switch_off_ecu']
            data_pos = ['true_false']

            mcu_respond = mcu_turn_off_ecu_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
                                                 mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        else:
            mcu_respond = True

        if not mcu_respond:
            if fail_popup_notification:
                self.show_alert_popup("Fail to switch off!")
            self.ecu_switch_btn.setChecked(True)
        else:
            pass

    def switch_on_rev(self):
        if USE_IF_MCU:
            mcu_cmd = self.device_config['cmd_switch_on_rev']
            mcu_port = self.device_config['mcu_com_port']
            mcu_baud_rate = self.device_config['mcu_baud_rate']
            mcu_start_false_cmd = self.device_config['cmd_start_false']
            mcu_repetition_request = self.device_config['mcu_repetition_request_time']
            timeout_set = self.device_config['tmcu_switch_on_rev']
            mcu_resp_len = self.device_config['rlmcu_switch_on_rev']
            data_pos = ['true_false']

            mcu_respond = mcu_turn_on_rev_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
                                                mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        else:
            mcu_respond = True

        if not mcu_respond:
            self.show_alert_popup("Fail to switch on!")
            self.rev_gear_switch_btn.setChecked(False)
        else:
            pass

    def switch_off_rev(self):
        if USE_IF_MCU:
            mcu_cmd = self.device_config['cmd_switch_off_rev']
            mcu_port = self.device_config['mcu_com_port']
            mcu_baud_rate = self.device_config['mcu_baud_rate']
            mcu_start_false_cmd = self.device_config['cmd_start_false']
            mcu_repetition_request = self.device_config['mcu_repetition_request_time']
            timeout_set = self.device_config['tmcu_switch_off_rev']
            mcu_resp_len = self.device_config['rlmcu_switch_off_rev']
            data_pos = ['true_false']

            mcu_respond = mcu_turn_off_rev_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
                                                 mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        else:
            mcu_respond = True

        if not mcu_respond:
            self.show_alert_popup("Fail to switch off!")
            self.rev_gear_switch_btn.setChecked(True)
        else:
            pass

    def config_ui(self):
        """ComboBox settings"""
        self.ui.vehicle_family_cbox.view().setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.ui.vehicle_id_cbox.view().setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.ui.standard_cbox.view().setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.ui.test_type_cbox.view().setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        # Vehicle combobox
        # Filter based on parameter
        vehicle_df = self.vehicle_dataframe.copy()
        if not vehicle_df.empty:
            vehicle_family_value = vehicle_df[["vehicle_family"]].values.ravel()
            vehicle_family_unique_value = list(pd.unique(vehicle_family_value))
            vehicle_family_unique_value = [str(i) for i in vehicle_family_unique_value]
            self.ui.vehicle_family_cbox.addItems(vehicle_family_unique_value)
            current_vehicle_family = self.ui.vehicle_family_cbox.currentText()
            vehicle_family = [current_vehicle_family]
            vehicle_family_id_filtered = VehicleHandler(vehicle_df=vehicle_df,
                                                        vehicle_family=vehicle_family).filter_result()
            vehicle_id_value = vehicle_family_id_filtered[["vehicle_id"]].values.ravel()
            vehicle_id_unique_value = list(pd.unique(vehicle_id_value))
            vehicle_id_unique_value = [str(i) for i in vehicle_id_unique_value]
            self.ui.vehicle_id_cbox.addItems(vehicle_id_unique_value)
        # Standard combobox
        # Filter based on parameter
        standard_df = self.standard_dataframe.copy()
        if not standard_df.empty:
            standard_name_value = standard_df[["standard_name"]].values.ravel()
            standard_name_unique_value = list(pd.unique(standard_name_value))
            standard_name_unique_value = [str(i) for i in standard_name_unique_value]
            self.ui.standard_cbox.addItems(standard_name_unique_value)

        # Test point button
        for btn in self.tp_horizontal_btn_list:
            getattr(self.ui, btn).setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
        for btn in self.tp_vertical_btn_list:
            getattr(self.ui, btn).setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
        self.ui.tp_manual_btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")

        # Set run, pause, stop icon
        self.ui.run_test_btn.setEnabled(True)
        self.ui.pause_test_btn.setDisabled(True)
        self.ui.stop_test_btn.setDisabled(True)
        self.ui.run_test_btn.setStyleSheet("border-image: url(" + self.run_icon_path + ")")
        self.ui.pause_test_btn.setStyleSheet("border-image: url(" + self.pause_disable_icon_path + ")")
        self.ui.stop_test_btn.setStyleSheet("border-image: url(" + self.stop_disable_icon_path + ")")

        # Set move motor icon
        self.ui.move_motor_btn.setIcon(QtGui.QIcon(self.move_motor_icon_path))
        self.ui.motor_home_btn.setIcon(QtGui.QIcon(self.motor_home_icon_path))

        # Set menubar icon
        self.ui.quit_menubar.setIcon(QtGui.QIcon(self.quit_icon_path))
        self.ui.about_menubar.setIcon(QtGui.QIcon(self.about_icon_path))
        self.ui.generate_report_menubar.setIcon(QtGui.QIcon(self.report_icon_path))
        self.ui.settings_menubar.setIcon(QtGui.QIcon(self.settings_icon_path))

        # Config result table
        self.ui.test_result_tab.setHorizontalHeader(WrapHeader(QtCore.Qt.Horizontal, self.ui.test_result_tab))
        model = self.ui.test_result_tab.model()
        default = self.ui.test_result_tab.horizontalHeader().defaultAlignment()
        default |= QtCore.Qt.TextWordWrap
        for col in range(self.ui.test_result_tab.columnCount()):
            alignment = model.headerData(
                col, QtCore.Qt.Horizontal, QtCore.Qt.TextAlignmentRole)
            if alignment:
                alignment |= QtCore.Qt.TextWordWrap
            else:
                alignment = default
            model.setHeaderData(col, QtCore.Qt.Horizontal, alignment, QtCore.Qt.TextAlignmentRole)
            self.ui.test_result_tab.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)
        self.ui.test_result_tab.horizontalHeader().setSectionResizeMode(6, QHeaderView.ResizeToContents)
        self.ui.test_result_tab.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.test_result_tab.verticalHeader().setSectionResizeMode(QHeaderView.Stretch)
        # Show standard in result table
        # Horizontal
        self.ui.test_result_tab.item(0, 2).setText("0")
        self.ui.test_result_tab.item(0, 4).setText("-")
        self.ui.test_result_tab.item(0, 5).setText("-")
        self.ui.test_result_tab.item(0, 6).setText("-")
        self.ui.test_result_tab.item(0, 6).setData(QtCore.Qt.BackgroundRole, QVariant())
        # Vertical test 1
        self.ui.test_result_tab.item(1, 2).setText("0")
        self.ui.test_result_tab.item(1, 4).setText("-")
        self.ui.test_result_tab.item(1, 5).setText("-")
        self.ui.test_result_tab.item(1, 6).setText("-")
        self.ui.test_result_tab.item(1, 6).setData(QtCore.Qt.BackgroundRole, QVariant())
        # Vertical test 2
        self.ui.test_result_tab.item(2, 2).setText("0")
        self.ui.test_result_tab.item(2, 4).setText("-")
        self.ui.test_result_tab.item(2, 5).setText("-")
        self.ui.test_result_tab.item(2, 6).setText("-")
        self.ui.test_result_tab.item(2, 6).setData(QtCore.Qt.BackgroundRole, QVariant())
        if not standard_df.empty:
            current_standard = self.ui.standard_cbox.currentText()
            self.show_standard_info(current_standard)
        else:
            self.reset_display_standard_info()
        # Span cell
        self.ui.test_result_tab.setSpan(1, 0, 2, 1)  # Vertical type test column
        self.ui.test_result_tab.setSpan(1, 3, 2, 1)  # Vertical minimum coverage column
        self.ui.test_result_tab.setSpan(1, 5, 2, 1)  # Vertical detected coverage column
        self.ui.test_result_tab.setSpan(1, 6, 2, 1)  # Vertical result column

        # Config model info table
        # Info tab
        self.special_delegate = SpecialStyledItemDelegate()
        self.ui.model_info_tab.setItemDelegate(self.special_delegate)
        self.special_delegate.add_text("VEHICLE WIDTH", 0, 0)
        self.special_delegate.add_text("WIDTH BETWEEN SENSOR", 0, 1)
        self.special_delegate.add_text("HEIGHT OF THE SENSOR FROM GROUND", 0, 6)
        self.special_delegate.add_text("DISTANCE FROM EXTREME REAR FACE", 0, 10)
        self.ui.model_info_tab.setSpan(0, 0, 2, 1)
        self.ui.model_info_tab.setSpan(0, 1, 1, 5)
        self.ui.model_info_tab.setSpan(0, 6, 1, 4)
        self.ui.model_info_tab.setSpan(0, 10, 1, 4)
        self.ui.model_info_tab.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.ui.model_info_tab.verticalHeader().setSectionResizeMode(QHeaderView.Stretch)
        # Reference tab
        self.ui.sensor_width_reference_lab.setPixmap(QtGui.QPixmap(self.sensor_width_reference_path))
        self.ui.sensor_distance_rear_reference_lab.setPixmap(QtGui.QPixmap(self.sensor_distance_rear_path))
        # Show in vehicle info table
        if not vehicle_df.empty:
            current_vehicle_family = self.ui.vehicle_family_cbox.currentText()
            current_vehicle_id = self.ui.vehicle_id_cbox.currentText()
            self.show_vehicle_info(current_vehicle_family, current_vehicle_id)
        else:
            self.reset_display_model_info()

        # Control box section
        self.ui.control_box_layout.addWidget(self.ecu_switch_btn, 0, 1)
        self.ui.control_box_layout.addWidget(self.rev_gear_switch_btn, 1, 1)
        self.ui.control_box_layout.setColumnMinimumWidth(1, 80)

        # Result section
        # Result color background
        self.ecu_switch_btn.setChecked(False)
        self.rev_gear_switch_btn.setChecked(False)
        self.ui.result_lab.setText("READY")
        self.ui.result_lab.setStyleSheet("background-color: rgb(0, 128, 255)")

        # Coverage map section
        self.ui.horizontal_frame.setStyleSheet("border-image: url(" + self.coverage_map_path + ")")
        self.ui.vertical_frame.setStyleSheet("border-image: url(" + self.coverage_map_path + ")")
        self.ui.manual_frame.setStyleSheet("border-image: url(" + self.coverage_map_path + ")")

    def init_vehicle_config(self):
        config_file_list = glob.glob(VEHICLE_CONFIG_PATH + "*.ini")
        if len(config_file_list) == 1:
            self.vehicle_fresh_startup = True
        else:
            self.vehicle_fresh_startup = False

        for file in config_file_list:
            config_file = file.split("\\")[-1]
            config_without_extension = config_file.replace(".ini", "")
            if config_without_extension == "init":
                pass
            else:
                vehicle_config = ConfigParser()
                vehicle_config.read(VEHICLE_CONFIG_PATH + "/" + config_file)
                vehicle_family = vehicle_config["INFO"]["info_family"]
                vehicle_id = vehicle_config["INFO"]["info_id"]
                vehicle_width = float(vehicle_config["INFO"]["info_width"])
                wbs_point_a = float(vehicle_config["WIDTH BETWEEN SENSOR"]["wbs_point_a"])
                wbs_point_b = float(vehicle_config["WIDTH BETWEEN SENSOR"]["wbs_point_b"])
                wbs_point_c = float(vehicle_config["WIDTH BETWEEN SENSOR"]["wbs_point_c"])
                wbs_point_d = float(vehicle_config["WIDTH BETWEEN SENSOR"]["wbs_point_d"])
                wbs_point_z = float(vehicle_config["WIDTH BETWEEN SENSOR"]["wbs_point_z"])
                hos_sensor1 = float(vehicle_config["HEIGHT OF SENSOR"]["hos_sensor1"])
                hos_sensor2 = float(vehicle_config["HEIGHT OF SENSOR"]["hos_sensor2"])
                hos_sensor3 = float(vehicle_config["HEIGHT OF SENSOR"]["hos_sensor3"])
                hos_sensor4 = float(vehicle_config["HEIGHT OF SENSOR"]["hos_sensor4"])
                drf_sensor1 = float(vehicle_config["DISTANCE REAR FACE"]["drf_sensor1"])
                drf_sensor2 = float(vehicle_config["DISTANCE REAR FACE"]["drf_sensor2"])
                drf_sensor3 = float(vehicle_config["DISTANCE REAR FACE"]["drf_sensor3"])
                drf_sensor4 = float(vehicle_config["DISTANCE REAR FACE"]["drf_sensor4"])
                new_model = pd.DataFrame({"vehicle_family": [vehicle_family],
                                          "vehicle_id": [vehicle_id],
                                          "vehicle_width": [vehicle_width],
                                          "width_between_sensor_point_a": [wbs_point_a],
                                          "width_between_sensor_point_b": [wbs_point_b],
                                          "width_between_sensor_point_c": [wbs_point_c],
                                          "width_between_sensor_point_d": [wbs_point_d],
                                          "width_between_sensor_point_z": [wbs_point_z],
                                          "height_of_sensor1": [hos_sensor1],
                                          "height_of_sensor2": [hos_sensor2],
                                          "height_of_sensor3": [hos_sensor3],
                                          "height_of_sensor4": [hos_sensor4],
                                          "distance_rear_face_sensor1": [drf_sensor1],
                                          "distance_rear_face_sensor2": [drf_sensor2],
                                          "distance_rear_face_sensor3": [drf_sensor3],
                                          "distance_rear_face_sensor4": [drf_sensor4]
                                          })

                # Add model in productmodel_df
                self.vehicle_dataframe = self.vehicle_dataframe.append(new_model, ignore_index=True)

    def init_standard_config(self):
        config_file_list = glob.glob(STANDARD_CONFIG_FILE_PATH + "*.ini")
        if len(config_file_list) == 1:
            # Prepare standard settings
            config_object = ConfigParser()
            config_object.read(STANDARD_CONFIG_FILE_PATH + "init.ini")
            self.standard_config = {}
            for section in config_object.sections():
                section_list = config_object.items(section)
                for each_key, each_value in section_list:
                    self.standard_config[each_key] = each_value
        else:
            # Prepare standard settings
            config_file = config_file_list[1]
            config_file = config_file.split("\\")[-1]
            config_object = ConfigParser()
            config_object.read(STANDARD_CONFIG_FILE_PATH + "/" + config_file)
            self.standard_config = {}
            for section in config_object.sections():
                section_list = config_object.items(section)
                for each_key, each_value in section_list:
                    self.standard_config[each_key] = each_value

        for file in config_file_list:
            config_file = file.split("\\")[-1]
            config_without_extension = config_file.replace(".ini", "")
            if config_without_extension == "init":
                pass
            else:
                standard_config = ConfigParser()
                standard_config.read(STANDARD_CONFIG_FILE_PATH + "/" + config_file)
                standard_name = standard_config["INFO"]["standard_name"]
                standard_coverage_section = standard_config["COVERAGE"]
                standard_horizontal_min_coverage = standard_coverage_section["coverage_horizontal_minimum_percentage"]
                standard_vertical_min_coverage = standard_coverage_section["coverage_vertical_minimum_percentage"]
                new_standard = pd.DataFrame({"standard_name": [standard_name],
                                             "standard_horizontal_minimum_coverage":
                                                 [standard_horizontal_min_coverage],
                                             "standard_vertical_minimum_coverage":
                                                 [standard_vertical_min_coverage]})

                # Add standard in standard_df
                self.standard_dataframe = self.standard_dataframe.append(new_standard, ignore_index=True)

    def init_operation_log_dataframe(self):
        all_files = glob.glob(LOG_DIR + "/*.pkl")
        df_log = Parallel(n_jobs=-1, prefer="threads")(delayed(self.read_pickle_loop)(f) for f in all_files)

        if len(df_log) != 0:
            df_log = pd.concat(df_log, ignore_index=True)
        else:
            df_log = pd.read_pickle(LOG_DIR + "/init.pkl")

        return df_log

    @staticmethod
    def read_pickle_loop(filename):
        return pd.read_pickle(filename)

    def reset_horizontal_test_point(self):
        for btn in self.tp_horizontal_btn_list:
            btn = getattr(self.ui, btn)
            btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")

    def reset_vertical_test_point(self):
        for btn in self.tp_vertical_btn_list:
            btn = getattr(self.ui, btn)
            btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")

    def show_vehicle_info(self, vehicle_family, vehicle_id):
        # Copy dataframe
        vehicle_df = self.vehicle_dataframe.copy()

        if vehicle_id:
            if not vehicle_df.empty:
                # Filter
                vehicle_family_id_filtered = VehicleHandler(vehicle_df=vehicle_df,
                                                            vehicle_family=[vehicle_family],
                                                            vehicle_id=[vehicle_id]).filter_result()

                self.ui.model_info_tab.item(2, 0).setText(str(vehicle_family_id_filtered["vehicle_width"].values[0]))
                self.ui.model_info_tab.item(2, 1).setText(str(vehicle_family_id_filtered["width_between_sensor_point_a"]
                                                              .values[0]))
                self.ui.model_info_tab.item(2, 2).setText(str(vehicle_family_id_filtered["width_between_sensor_point_b"]
                                                              .values[0]))
                self.ui.model_info_tab.item(2, 3).setText(str(vehicle_family_id_filtered["width_between_sensor_point_c"]
                                                              .values[0]))
                self.ui.model_info_tab.item(2, 4).setText(str(vehicle_family_id_filtered["width_between_sensor_point_d"]
                                                              .values[0]))
                self.ui.model_info_tab.item(2, 5).setText(str(vehicle_family_id_filtered["width_between_sensor_point_z"]
                                                              .values[0]))
                self.ui.model_info_tab.item(2, 6).setText(str(vehicle_family_id_filtered["height_of_sensor1"]
                                                              .values[0]))
                self.ui.model_info_tab.item(2, 7).setText(str(vehicle_family_id_filtered["height_of_sensor2"]
                                                              .values[0]))
                self.ui.model_info_tab.item(2, 8).setText(str(vehicle_family_id_filtered["height_of_sensor3"]
                                                              .values[0]))
                self.ui.model_info_tab.item(2, 9).setText(str(vehicle_family_id_filtered["height_of_sensor4"]
                                                              .values[0]))
                self.ui.model_info_tab.item(2, 10).setText(str(vehicle_family_id_filtered["distance_rear_face_sensor1"].
                                                               values[0]))
                self.ui.model_info_tab.item(2, 11).setText(str(vehicle_family_id_filtered["distance_rear_face_sensor2"].
                                                               values[0]))
                self.ui.model_info_tab.item(2, 12).setText(str(vehicle_family_id_filtered["distance_rear_face_sensor3"].
                                                               values[0]))
                self.ui.model_info_tab.item(2, 13).setText(str(vehicle_family_id_filtered["distance_rear_face_sensor4"].
                                                               values[0]))

                # Update vertical test type
                height_sensor_1 = vehicle_family_id_filtered["height_of_sensor1"].values[0]
                height_sensor_2 = vehicle_family_id_filtered["height_of_sensor2"].values[0]
                height_sensor_3 = vehicle_family_id_filtered["height_of_sensor3"].values[0]
                height_sensor_4 = vehicle_family_id_filtered["height_of_sensor4"].values[0]
                height_sensor_list = [height_sensor_1, height_sensor_2, height_sensor_3, height_sensor_4]

                self.require_test_c = False
                for obj in height_sensor_list:
                    if obj >= 800:
                        self.require_test_c = True
                        break

                if self.require_test_c:
                    self.ui.vertical_test_type_cbox.model().item(0).setEnabled(False)
                    self.ui.vertical_test_type_cbox.model().item(1).setEnabled(True)
                    self.ui.vertical_test_type_cbox.model().item(2).setEnabled(True)
                    self.ui.vertical_test_type_cbox.setCurrentIndex(1)

                    # Update result tab sub type
                    self.ui.test_result_tab.item(1, 1).setText("B")
                    self.ui.test_result_tab.item(2, 1).setText("C")
                else:
                    self.ui.vertical_test_type_cbox.model().item(0).setEnabled(True)
                    self.ui.vertical_test_type_cbox.model().item(1).setEnabled(True)
                    self.ui.vertical_test_type_cbox.model().item(2).setEnabled(False)
                    self.ui.vertical_test_type_cbox.setCurrentIndex(0)

                    # Update result tab sub type
                    self.ui.test_result_tab.item(1, 1).setText("A")
                    self.ui.test_result_tab.item(2, 1).setText("B")

                # Show test point based on vehicle width
                # Horizontal test
                vehicle_width = int(vehicle_family_id_filtered["vehicle_width"].values[0])
                if vehicle_width >= self.horizontal_limit_add_test_point:
                    for btn in self.tp_horizontal_btn_list:
                        btn = getattr(self.ui, btn)
                        btn_name = btn.objectName()
                        btn_id = int(btn_name.split("_")[-1])
                        self.total_horizontal_test_point_to_test += 1

                        if btn_id in self.horizontal_id_add_remove_test_point_btn_list:
                            btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                            btn.setVisible(True)
                        else:
                            pass
                else:
                    for btn in self.tp_horizontal_btn_list:
                        btn = getattr(self.ui, btn)
                        btn_name = btn.objectName()
                        btn_id = int(btn_name.split("_")[-1])

                        if btn_id in self.horizontal_id_add_remove_test_point_btn_list:
                            btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                            btn.setVisible(False)
                        else:
                            self.total_horizontal_test_point_to_test += 1

                # Vertical test
                self.total_vertical_test_a_point_to_test = 26
                self.total_vertical_test_b_point_to_test = 26
                self.total_vertical_test_c_point_to_test = 4
                vehicle_width = int(vehicle_family_id_filtered["vehicle_width"].values[0])
                if vehicle_width >= self.vertical_limit_add_test_point:
                    for btn in self.tp_vertical_btn_list:
                        btn = getattr(self.ui, btn)
                        btn_name = btn.objectName()
                        btn_id = int(btn_name.split("_")[-1])

                        if btn_id in self.vertical_id_add_remove_test_point_btn_list:
                            btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                            btn.setVisible(True)
                            self.vertical_test_a_to_test_dict[btn_name] = True
                            self.vertical_test_b_to_test_dict[btn_name] = True
                            self.vertical_test_c_to_test_dict[btn_name] = True
                        else:
                            pass
                else:
                    for btn in self.tp_vertical_btn_list:
                        btn = getattr(self.ui, btn)
                        btn_name = btn.objectName()
                        btn_id = int(btn_name.split("_")[-1])

                        if btn_id in self.vertical_id_add_remove_test_point_btn_list:
                            btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                            btn.setVisible(False)
                            self.vertical_test_a_to_test_dict[btn_name] = False
                            self.vertical_test_b_to_test_dict[btn_name] = False
                            self.vertical_test_c_to_test_dict[btn_name] = False
                        else:
                            pass
                    self.total_vertical_test_a_point_to_test = self.total_vertical_test_a_point_to_test - 4
                    self.total_vertical_test_b_point_to_test = self.total_vertical_test_b_point_to_test - 4

                # Update total test point in result table
                self.ui.test_result_tab.item(0, 2).setText(str(self.total_horizontal_test_point_to_test))
                if self.require_test_c:
                    self.ui.test_result_tab.item(1, 2).setText(str(self.total_vertical_test_b_point_to_test))
                    self.ui.test_result_tab.item(2, 2).setText(str(self.total_vertical_test_c_point_to_test))
                else:
                    self.ui.test_result_tab.item(1, 2).setText(str(self.total_vertical_test_a_point_to_test))
                    self.ui.test_result_tab.item(2, 2).setText(str(self.total_vertical_test_b_point_to_test))

                # Update vertical test type icon check
                self.ui.vertical_test_type_cbox.setItemIcon(0, QtGui.QIcon(self.red_check_icon_path))
                self.ui.vertical_test_type_cbox.setItemIcon(1, QtGui.QIcon(self.red_check_icon_path))
                self.ui.vertical_test_type_cbox.setItemIcon(2, QtGui.QIcon(self.red_check_icon_path))
        else:
            pass

    def reset_display_model_info(self):
        for col in range(self.ui.model_info_tab.columnCount()):
            self.ui.model_info_tab.item(2, col).setText("-")

    def on_vehicle_family_cbox_changed(self):
        # Copy dataframe
        vehicle_df = self.vehicle_dataframe.copy()

        if not vehicle_df.empty:
            # Get current vehicle family
            vehicle_family = self.ui.vehicle_family_cbox.currentText()
            vehicle_family = [vehicle_family]

            # Filter based on parameter
            vehicle_family_id_filtered = VehicleHandler(vehicle_df=vehicle_df,
                                                        vehicle_family=vehicle_family).filter_result()
            vehicle_id_value = vehicle_family_id_filtered[["vehicle_id"]].values.ravel()
            vehicle_id_unique_value = list(pd.unique(vehicle_id_value))
            vehicle_id_unique_value = [str(i) for i in vehicle_id_unique_value]

            # Fill up cbox
            self.ui.vehicle_id_cbox.clear()
            self.ui.vehicle_id_cbox.addItems(vehicle_id_unique_value)

    def on_vehicle_id_cbox_changed(self):
        # Reset test point display
        self.reset_horizontal_test_point()
        self.reset_vertical_test_point()

        # Reset vertical test point display dict
        for obj in self.vertical_test_a_to_test_dict.keys():
            self.vertical_test_a_to_test_dict[obj] = True
        for obj in self.vertical_test_b_to_test_dict.keys():
            self.vertical_test_b_to_test_dict[obj] = True
        for obj in self.vertical_test_c_to_test_dict.keys():
            self.vertical_test_c_to_test_dict[obj] = True

        # Reset total test point
        self.total_horizontal_test_point_to_test = 0
        self.total_vertical_test_a_point_to_test = 0
        self.total_vertical_test_b_point_to_test = 0
        self.total_vertical_test_c_point_to_test = 0
        self.ui.test_result_tab.item(0, 2).setText("0")
        self.ui.test_result_tab.item(1, 2).setText("0")
        self.ui.test_result_tab.item(2, 2).setText("0")

        # Reset vertical sub test parameter
        self.vertical_sub_test_to_test_dict = {"A": [], "B": [], "C": []}

        # Reset result table
        self.ui.test_result_tab.item(0, 4).setText("-")
        self.ui.test_result_tab.item(0, 5).setText("-")
        self.ui.test_result_tab.item(0, 6).setText("-")
        self.ui.test_result_tab.item(0, 6).setData(QtCore.Qt.BackgroundRole, QVariant())
        self.ui.test_result_tab.item(1, 4).setText("-")
        self.ui.test_result_tab.item(1, 5).setText("-")
        self.ui.test_result_tab.item(1, 6).setText("-")
        self.ui.test_result_tab.item(1, 6).setData(QtCore.Qt.BackgroundRole, QVariant())
        self.ui.test_result_tab.item(2, 4).setText("-")
        self.ui.test_result_tab.item(2, 5).setText("-")
        self.ui.test_result_tab.item(2, 6).setText("-")
        self.ui.test_result_tab.item(2, 6).setData(QtCore.Qt.BackgroundRole, QVariant())

        vehicle_family = self.ui.vehicle_family_cbox.currentText()
        vehicle_id = self.ui.vehicle_id_cbox.currentText()
        if vehicle_id == "":
            vehicle_id = False
        else:
            vehicle_id = vehicle_id
        self.show_vehicle_info(vehicle_family, vehicle_id)

    def on_standard_cbox_changed(self):
        standard_name = self.ui.standard_cbox.currentText()
        if standard_name == "":
            standard_name = False
        else:
            # Prepare standard settings
            config_object = ConfigParser()
            config_object.read(STANDARD_CONFIG_FILE_PATH + "/std_" + standard_name + ".ini")
            self.standard_config = {}
            for section in config_object.sections():
                section_list = config_object.items(section)
                for each_key, each_value in section_list:
                    self.standard_config[each_key] = each_value

        # Update table
        self.show_standard_info(standard_name)

    def on_test_type_cbox_changed(self):
        # Copy dataframe
        vehicle_df = self.vehicle_dataframe.copy()

        if not vehicle_df.empty:
            # Enable horizontal and vertical test option
            self.ui.test_type_cbox.model().item(0).setEnabled(True)
            self.ui.test_type_cbox.model().item(1).setEnabled(True)
            self.ui.test_type_cbox.model().item(2).setEnabled(True)

            # Get parameter
            current_test_type_index = self.ui.test_type_cbox.currentIndex()
            vehicle_family = self.ui.vehicle_family_cbox.currentText()
            vehicle_id = self.ui.vehicle_id_cbox.currentText()

            # Filter
            vehicle_family_id_filtered = VehicleHandler(vehicle_df=vehicle_df,
                                                        vehicle_family=[vehicle_family],
                                                        vehicle_id=[vehicle_id]).filter_result()

            if current_test_type_index == 0:  # Horizontal
                # Enable run
                self.ui.run_test_btn.setEnabled(True)
                self.ui.run_test_btn.setStyleSheet("border-image: url(" + self.run_icon_path + ")")
                self.ui.coverage_map_stacked.setCurrentIndex(1)
                self.ui.vertical_test_type_cbox.setDisabled(True)
                self.disable_manual_control_box()

                # Update test point
                vehicle_width = int(vehicle_family_id_filtered["vehicle_width"].values[0])
                if vehicle_width >= self.horizontal_limit_add_test_point:
                    for btn in self.tp_horizontal_btn_list:
                        btn = getattr(self.ui, btn)
                        btn_name = btn.objectName()
                        btn_id = int(btn_name.split("_")[-1])

                        if btn_id in self.horizontal_id_add_remove_test_point_btn_list:
                            btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                            btn.setVisible(True)
                        else:
                            pass
                else:
                    for btn in self.tp_horizontal_btn_list:
                        btn = getattr(self.ui, btn)
                        btn_name = btn.objectName()
                        btn_id = int(btn_name.split("_")[-1])

                        if btn_id in self.horizontal_id_add_remove_test_point_btn_list:
                            btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                            btn.setVisible(False)
                        else:
                            pass

            elif current_test_type_index == 1:  # Vertical
                # Enable run
                self.ui.run_test_btn.setEnabled(True)
                self.ui.run_test_btn.setStyleSheet("border-image: url(" + self.run_icon_path + ")")
                self.ui.coverage_map_stacked.setCurrentIndex(0)
                self.ui.vertical_test_type_cbox.setEnabled(True)
                self.disable_manual_control_box()

                # Update test point
                vehicle_width = int(vehicle_family_id_filtered["vehicle_width"].values[0])
                if vehicle_width >= self.vertical_limit_add_test_point:
                    for btn in self.tp_vertical_btn_list:
                        btn = getattr(self.ui, btn)
                        btn_name = btn.objectName()
                        btn_id = int(btn_name.split("_")[-1])

                        if btn_id in self.vertical_id_add_remove_test_point_btn_list:
                            btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                            btn.setVisible(True)
                        else:
                            pass
                else:
                    for btn in self.tp_vertical_btn_list:
                        btn = getattr(self.ui, btn)
                        btn_name = btn.objectName()
                        btn_id = int(btn_name.split("_")[-1])

                        if btn_id in self.vertical_id_add_remove_test_point_btn_list:
                            btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                            btn.setVisible(False)
                        else:
                            pass

                # Change vertical test type current index
                if self.require_test_c:
                    self.ui.vertical_test_type_cbox.setCurrentIndex(1)
                else:
                    self.ui.vertical_test_type_cbox.setCurrentIndex(0)

            else:  # Manual
                # Disable run
                self.ui.run_test_btn.setDisabled(True)
                self.ui.run_test_btn.setStyleSheet("border-image: url(" + self.run_disable_icon_path + ")")

                self.ui.coverage_map_stacked.setCurrentIndex(2)
                self.ui.vertical_test_type_cbox.setDisabled(True)
                self.enable_manual_control_box()
        else:
            # Set test combobox to manual test
            self.ui.test_type_cbox.setCurrentIndex(2)
            # Disable horizontal and vertical test option
            self.ui.test_type_cbox.model().item(0).setEnabled(False)
            self.ui.test_type_cbox.model().item(1).setEnabled(False)
            self.ui.test_type_cbox.model().item(2).setEnabled(True)

            # Disable run
            self.ui.run_test_btn.setDisabled(True)
            self.ui.run_test_btn.setStyleSheet("border-image: url(" + self.run_disable_icon_path + ")")

            self.ui.coverage_map_stacked.setCurrentIndex(2)
            self.ui.vertical_test_type_cbox.setDisabled(True)
            self.enable_manual_control_box()

    def enable_manual_control_box(self):
        self.ui.manual_control_gbox.setEnabled(True)
        self.ecu_switch_btn.setEnabled(True)

    def disable_manual_control_box(self):
        self.ui.manual_control_gbox.setDisabled(True)
        self.ecu_switch_btn.setDisabled(True)

    def on_vertical_test_type_cbox_changed(self):
        # Copy dataframe
        vehicle_df = self.vehicle_dataframe.copy()

        if not vehicle_df.empty:
            # Get parameter
            current_vertical_test_type_index = self.ui.vertical_test_type_cbox.currentIndex()
            vehicle_family = self.ui.vehicle_family_cbox.currentText()
            vehicle_id = self.ui.vehicle_id_cbox.currentText()

            # Filter
            vehicle_family_id_filtered = VehicleHandler(vehicle_df=vehicle_df,
                                                        vehicle_family=[vehicle_family],
                                                        vehicle_id=[vehicle_id]).filter_result()
            vehicle_width = int(vehicle_family_id_filtered["vehicle_width"].values[0])

            if current_vertical_test_type_index == 0 or current_vertical_test_type_index == 1:
                for btn in self.tp_vertical_btn_list:
                    btn = getattr(self.ui, btn)
                    btn_name = btn.objectName()
                    btn_id = int(btn_name.split("_")[-1])

                    if btn_id in self.vertical_id_test_c_point_btn_list:
                        width = btn.geometry().width()
                        height = btn.geometry().height()
                        btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                        btn.setVisible(False)
                        self.vertical_test_a_to_test_dict[btn_name] = False
                        self.vertical_test_b_to_test_dict[btn_name] = False
                        if btn_id in self.top_right_c_point_id:
                            btn.setGeometry(self.initial_x_coordinate_right, self.initial_y_coordinate_top, width,
                                            height)
                        elif btn_id in self.top_left_c_point_id:
                            btn.setGeometry(self.initial_x_coordinate_left, self.initial_y_coordinate_top, width,
                                            height)
                        elif btn_id in self.bottom_left_c_point_id:
                            btn.setGeometry(self.initial_x_coordinate_left, self.initial_y_coordinate_bottom, width,
                                            height)
                        elif btn_id in self.bottom_right_c_point_id:
                            btn.setGeometry(self.initial_x_coordinate_right, self.initial_y_coordinate_bottom, width,
                                            height)
                    elif btn_id in self.vertical_id_add_remove_test_point_btn_list:
                        if vehicle_width >= self.vertical_limit_add_test_point:
                            btn.setVisible(True)
                            if current_vertical_test_type_index == 0:
                                if self.vertical_test_a_to_test_dict[btn_name]:
                                    btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                                else:
                                    btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                            elif current_vertical_test_type_index == 1:
                                if self.vertical_test_b_to_test_dict[btn_name]:
                                    btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                                else:
                                    btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                        else:
                            btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                            btn.setVisible(False)
                    else:
                        btn.setVisible(True)
                        if current_vertical_test_type_index == 0:
                            if self.vertical_test_a_to_test_dict[btn_name]:
                                btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                            else:
                                btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                        elif current_vertical_test_type_index == 1:
                            if self.vertical_test_b_to_test_dict[btn_name]:
                                btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                            else:
                                btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
            else:
                # Calculate new c point(mm) based on formula; (vehicle width / 2) - 400
                c_point_x_axis = (vehicle_width / 2) - 400
                self.vertical_custom_point = c_point_x_axis

                for btn in self.tp_vertical_btn_list:
                    btn = getattr(self.ui, btn)
                    btn_name = btn.objectName()
                    btn_id = int(btn_name.split("_")[-1])

                    if btn_id in self.vertical_id_test_c_point_btn_list:
                        if self.vertical_test_c_to_test_dict[btn_name]:
                            btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                        else:
                            btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                        btn.setVisible(True)

                        # Calculate new coordinate x-axis
                        x_coordinate = btn.geometry().x()
                        width = btn.geometry().width()
                        height = btn.geometry().height()
                        x_interval = self.initial_interval_x_axis
                        offset = self.x_pixel_offset
                        diff_increment_mm = c_point_x_axis - self.initial_x_axis_mm
                        pixel_per_mm = offset / x_interval
                        pixel_increment = pixel_per_mm * abs(diff_increment_mm)

                        if btn_id in self.top_right_c_point_id:
                            if diff_increment_mm < 0:
                                new_x_axis = x_coordinate - pixel_increment
                            else:
                                new_x_axis = x_coordinate + pixel_increment
                            btn.setGeometry(new_x_axis, self.initial_y_coordinate_top, width, height)
                        elif btn_id in self.top_left_c_point_id:
                            if diff_increment_mm < 0:
                                new_x_axis = x_coordinate + pixel_increment
                            else:
                                new_x_axis = x_coordinate - pixel_increment
                            btn.setGeometry(new_x_axis, self.initial_y_coordinate_top, width, height)
                        elif btn_id in self.bottom_left_c_point_id:
                            if diff_increment_mm < 0:
                                new_x_axis = x_coordinate + pixel_increment
                            else:
                                new_x_axis = x_coordinate - pixel_increment
                            btn.setGeometry(new_x_axis, self.initial_y_coordinate_bottom, width, height)
                        elif btn_id in self.bottom_right_c_point_id:
                            if diff_increment_mm < 0:
                                new_x_axis = x_coordinate - pixel_increment
                            else:
                                new_x_axis = x_coordinate + pixel_increment
                            btn.setGeometry(new_x_axis, self.initial_y_coordinate_bottom, width, height)
                    else:
                        btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
                        btn.setVisible(False)
                        self.vertical_test_c_to_test_dict[btn_name] = False
        else:
            for btn in self.tp_vertical_btn_list:
                btn = getattr(self.ui, btn)
                btn_name = btn.objectName()
                btn_id = int(btn_name.split("_")[-1])

                if btn_id in self.vertical_id_test_c_point_btn_list:
                    btn.setVisible(False)

    def show_standard_info(self, standard_name):
        if standard_name:
            # Copy dataframe
            standard_df = self.standard_dataframe.copy()

            if not standard_df.empty:
                # Filter
                standard_df_filtered = StandardHandler(standard_df=standard_df,
                                                       standard_name=[standard_name]).filter_result()

                self.ui.test_result_tab.item(0, 3).setText(str(
                    standard_df_filtered["standard_horizontal_minimum_coverage"].values[0]))
                self.ui.test_result_tab.item(1, 3).setText(str(
                    standard_df_filtered["standard_vertical_minimum_coverage"].values[0]))
        else:
            pass

    def reset_display_standard_info(self):
        self.ui.test_result_tab.item(0, 3).setText("-")
        self.ui.test_result_tab.item(1, 3).setText("-")

    def run_button_clicked(self):
        current_test_type = self.ui.test_type_cbox.currentIndex()  # 0:Horizontal; 1:Vertical, 2:Manual

        # Get test point to run
        test_to_run = self.get_test_to_run_list(current_test_type)
        total_test_to_run = test_to_run.count(True)

        # Update test point list if continuation from interrupt flow
        if self.interrupt_requested:
            test_to_run = self.update_test_point_to_test(test_to_run)

        # Get current test mode
        horizontal_mode = False
        vertical_mode = False
        manual_mode = False
        if current_test_type == 0:
            horizontal_mode = True
            logging_test_str = "horizontal"
        elif current_test_type == 1:
            vertical_mode = True
            logging_test_str = "vertical"
            # Fill up parameter
            # vertical_sub_test_to_test_dict = [total test point to test, detected test point]
            if self.ui.vertical_test_type_cbox.currentIndex() == 0:
                self.vertical_sub_test_to_test_dict["A"] = [self.total_vertical_test_a_point_to_test, 0]
            elif self.ui.vertical_test_type_cbox.currentIndex() == 1:
                self.vertical_sub_test_to_test_dict["B"] = [self.total_vertical_test_b_point_to_test, 0]
            else:
                self.vertical_sub_test_to_test_dict["C"] = [self.total_vertical_test_c_point_to_test, 0]
        else:
            manual_mode = True
            logging_test_str = "manual"

        # Prepare parameter to save to log
        operator_name = self.ui.operator_ledit.text()
        if not self.interrupt_requested:
            start_date = QDateTime.currentDateTime().toString('yyyy/MM/dd hh:mm:ss')
        else:
            start_date = self.save_to_csv["start_date"]
        end_date = ""
        vehicle_family = self.ui.vehicle_family_cbox.currentText()
        vehicle_id = self.ui.vehicle_id_cbox.currentText()
        vehicle_width = self.ui.model_info_tab.item(2, 0).text()
        width_sensor_a = self.ui.model_info_tab.item(2, 1).text()
        width_sensor_b = self.ui.model_info_tab.item(2, 2).text()
        width_sensor_c = self.ui.model_info_tab.item(2, 3).text()
        width_sensor_d = self.ui.model_info_tab.item(2, 4).text()
        width_sensor_z = self.ui.model_info_tab.item(2, 5).text()
        height_sensor1 = self.ui.model_info_tab.item(2, 6).text()
        height_sensor2 = self.ui.model_info_tab.item(2, 7).text()
        height_sensor3 = self.ui.model_info_tab.item(2, 8).text()
        height_sensor4 = self.ui.model_info_tab.item(2, 9).text()
        dist_rear_sensor1 = self.ui.model_info_tab.item(2, 10).text()
        dist_rear_sensor2 = self.ui.model_info_tab.item(2, 11).text()
        dist_rear_sensor3 = self.ui.model_info_tab.item(2, 12).text()
        dist_rear_sensor4 = self.ui.model_info_tab.item(2, 13).text()
        standard_name = self.ui.standard_cbox.currentText()
        if self.ui.test_type_cbox.currentIndex() == 0:
            standard_spec_coverage = self.ui.test_result_tab.item(0, 3).text()
            test_type = "Horizontal"
            test_horizontal_total_test_point = self.ui.test_result_tab.item(0, 2).text()
            test_horizontal_detected_test_point = ""
            test_sub_type_first_name = "-"
            test_sub_type_first_total_test_point = "-"
            test_sub_type_first_detected_test_point = "-"
            test_sub_type_second_name = "-"
            test_sub_type_second_total_test_point = "-"
            test_sub_type_second_detected_test_point = "-"
        elif self.ui.test_type_cbox.currentIndex() == 1:
            standard_spec_coverage = self.ui.test_result_tab.item(1, 3).text()
            test_type = "Vertical"
            test_horizontal_total_test_point = "-"
            test_horizontal_detected_test_point = "-"
            if not self.require_test_c:
                test_sub_type_first_name = "A"
                test_sub_type_first_total_test_point = ""
                test_sub_type_first_detected_test_point = ""
                test_sub_type_second_name = "B"
                test_sub_type_second_total_test_point = ""
                test_sub_type_second_detected_test_point = ""
            else:
                test_sub_type_first_name = "B"
                test_sub_type_first_total_test_point = ""
                test_sub_type_first_detected_test_point = ""
                test_sub_type_second_name = "C"
                test_sub_type_second_total_test_point = ""
                test_sub_type_second_detected_test_point = ""
        else:
            standard_spec_coverage = ""
            test_type = ""
            test_horizontal_total_test_point = "-"
            test_horizontal_detected_test_point = "-"
            test_sub_type_first_name = "-"
            test_sub_type_first_total_test_point = "-"
            test_sub_type_first_detected_test_point = "-"
            test_sub_type_second_name = "-"
            test_sub_type_second_total_test_point = "-"
            test_sub_type_second_detected_test_point = "-"

        detected_coverage = ""
        overall_result = ""
        parameter_list = [operator_name, start_date, end_date, vehicle_family, vehicle_id, vehicle_width,
                          width_sensor_a, width_sensor_b, width_sensor_c, width_sensor_d, width_sensor_z,
                          height_sensor1, height_sensor2, height_sensor3, height_sensor4,
                          dist_rear_sensor1, dist_rear_sensor2, dist_rear_sensor3, dist_rear_sensor4,
                          standard_name, standard_spec_coverage, test_type, test_horizontal_total_test_point,
                          test_horizontal_detected_test_point,
                          test_sub_type_first_name,
                          test_sub_type_first_total_test_point,
                          test_sub_type_first_detected_test_point,
                          test_sub_type_second_name,
                          test_sub_type_second_total_test_point,
                          test_sub_type_second_detected_test_point,
                          detected_coverage, overall_result]

        # Store in dict
        for idx, col in enumerate(self.csv_cols):
            self.save_to_csv[col] = parameter_list[idx]

        # Check if motor serial opened
        self.check_motor_port()

        # Start test thread
        self.run_test_thread(test_to_run, total_test_to_run, horizontal_mode, vertical_mode, manual_mode)
        system_log = f"Run clicked on {logging_test_str} test, Vehicle family: {vehicle_family}, " \
                     f"Vehicle ID: {vehicle_id}, Standard: {standard_name}, Operator: {operator_name}"
        logging.info(system_log)
        print(system_log)

    def pause_button_clicked(self):
        self.interrupt_requested = True
        self.test_worker.pause_interupt = True
        system_log = "Pause clicked"
        logging.info(system_log)
        print(system_log)

    def stop_button_clicked(self):
        self.interrupt_requested = False
        if self.test_running:
            self.test_worker.stop_interupt = True
        else:
            # Turn OFF ECU
            self.switch_off_ecu(fail_popup_notification=False)

            # Power supply
            if USE_POWER_SUPPLY:
                self.power_supply.turn_off_output()

            # Finalizing updating result table
            if self.ui.test_type_cbox.currentIndex() == 0:
                result = self.current_horizontal_result
            else:
                result = self.current_horizontal_result
            self.update_overall_result(result)
            self.report_status_progress(result)

            self.after_test_interpretation()
        system_log = "Stop clicked"
        logging.info(system_log)
        print(system_log)

    def report_progress(self, msg):
        self.ui.statusbar.showMessage(msg)

    def report_status_progress(self, msg):
        # Check overall status
        if msg == "READY":
            overall_status = "READY"
        elif msg == "PROCESSING":
            overall_status = "PROCESSING"
        elif msg == "PASS":
            overall_status = "PASS"
        elif msg == "NO TEST":
            overall_status = "NO TEST"
        elif msg == "FAIL":
            overall_status = "FAIL"
        elif msg == "PAUSED":
            overall_status = "PAUSED"
        else:
            overall_status = "ERROR"

        # Store in vertical result
        if self.ui.test_type_cbox.currentIndex() == 1:
            self.current_vertical_result = overall_status

        # Check overall status PASS/FAIL depend on test type
        if overall_status == "PASS" or overall_status == "FAIL":
            if self.ui.test_type_cbox.currentIndex() == 0:
                pass
            elif self.ui.test_type_cbox.currentIndex() == 1:
                all_test, all_sub_to_test = self.check_required_sub_test_successfully_run()
                if all_sub_to_test:
                    total_to_test = 0
                    total_detected = 0
                    for sub_test in all_test:
                        sub_test_parameter = self.vertical_sub_test_to_test_dict[sub_test]
                        sub_test_to_test = sub_test_parameter[0]
                        sub_test_detected = sub_test_parameter[1]
                        total_to_test += sub_test_to_test
                        total_detected += sub_test_detected

                    try:
                        total_coverage = total_detected / total_to_test
                    except ZeroDivisionError:
                        total_coverage = 0

                    total_coverage_percentage = total_coverage * 100
                    total_coverage_percentage = float("{:.2f}".format(total_coverage_percentage))

                    self.ui.test_result_tab.item(1, 5).setText(str(total_coverage_percentage))

                    self.current_detected_coverage = total_coverage_percentage

                    # Check detected coverage for vertical
                    current_vertical_minimum_coverage = float(self.ui.test_result_tab.item(1, 3).text())

                    # Check overall result
                    if self.current_detected_coverage >= current_vertical_minimum_coverage:
                        overall_status = "PASS"
                    else:
                        overall_status = "FAIL"
                else:
                    overall_status = "SUB TEST INCOMPLETE"
            else:
                pass

        # Check background color
        if overall_status == "READY" or overall_status == "NO TEST":
            self.ui.result_lab.setStyleSheet("background-color: rgb(0, 128, 255)")  # Blue
        elif overall_status == "PROCESSING" or overall_status == "SUB TEST INCOMPLETE" or overall_status == "PAUSED":
            self.ui.result_lab.setStyleSheet("background-color: rgb(255, 255, 0)")  # Yellow
        elif overall_status == "PASS":
            self.ui.result_lab.setStyleSheet("background-color: rgb(0, 255, 0)")  # Green
        else:  # Error/Fail
            self.ui.result_lab.setStyleSheet("background-color: rgb(255, 0, 0)")  # Red

        # Check statusbar msg
        if overall_status == "READY":
            self.ui.statusbar.showMessage("Ready.")
        elif overall_status == "NO TEST":
            self.ui.statusbar.showMessage("No test selected.")

        self.ui.result_lab.setText(overall_status)

    def reset_result_before_test(self):
        if self.ui.test_type_cbox.currentIndex() == 0:
            if not self.interrupt_requested:
                self.ui.test_result_tab.item(0, 4).setText("0")
                self.ui.test_result_tab.item(0, 5).setText("0")
                self.ui.test_result_tab.item(0, 6).setText("-")
            else:
                pass
        elif self.ui.test_type_cbox.currentIndex() == 1:
            self.ui.test_result_tab.item(1, 5).setText("-")
            if self.ui.vertical_test_type_cbox.currentIndex() == 0:
                self.ui.test_result_tab.item(1, 4).setText("0")
                self.ui.test_result_tab.item(1, 5).setText("-")
                self.ui.test_result_tab.item(1, 6).setText("-")
            elif self.ui.vertical_test_type_cbox.currentIndex() == 1:
                if self.require_test_c:
                    self.ui.test_result_tab.item(1, 4).setText("0")
                    self.ui.test_result_tab.item(1, 5).setText("-")
                    self.ui.test_result_tab.item(1, 6).setText("-")
                else:
                    self.ui.test_result_tab.item(2, 4).setText("0")
                    self.ui.test_result_tab.item(2, 5).setText("-")
                    self.ui.test_result_tab.item(2, 6).setText("-")
            else:
                self.ui.test_result_tab.item(2, 4).setText("0")
                self.ui.test_result_tab.item(2, 5).setText("-")
                self.ui.test_result_tab.item(2, 6).setText("-")
        else:
            self.ui.test_result_tab.item(0, 4).setText("0")
            self.ui.test_result_tab.item(0, 5).setText("0")
            self.ui.test_result_tab.item(0, 6).setText("-")

            self.ui.test_result_tab.item(1, 4).setText("0")
            self.ui.test_result_tab.item(1, 5).setText("0")
            self.ui.test_result_tab.item(1, 6).setText("-")

            self.ui.test_result_tab.item(2, 4).setText("0")
            self.ui.test_result_tab.item(2, 5).setText("0")
            self.ui.test_result_tab.item(2, 6).setText("-")

    def reset_result_after_test(self):
        # self.ui.test_result_tab.item(0, 4).setText("-")
        # self.ui.test_result_tab.item(0, 5).setText("-")
        self.ui.test_result_tab.item(0, 6).setText("-")
        self.ui.test_result_tab.item(0, 6).setData(QtCore.Qt.BackgroundRole, QVariant())

        # Check if all required vertical sub test successfully run, then calculate detected coverage
        _, all_sub_to_test = self.check_required_sub_test_successfully_run()

        if all_sub_to_test:
            # self.ui.test_result_tab.item(1, 4).setText("-")
            # self.ui.test_result_tab.item(1, 5).setText("-")
            self.ui.test_result_tab.item(1, 6).setText("-")
            self.ui.test_result_tab.item(1, 6).setData(QtCore.Qt.BackgroundRole, QVariant())

            # self.ui.test_result_tab.item(2, 4).setText("-")
            # self.ui.test_result_tab.item(2, 5).setText("-")
            self.ui.test_result_tab.item(2, 6).setText("-")
            self.ui.test_result_tab.item(2, 6).setData(QtCore.Qt.BackgroundRole, QVariant())
        else:
            self.ui.test_result_tab.item(1, 6).setText("-")
            self.ui.test_result_tab.item(1, 6).setData(QtCore.Qt.BackgroundRole, QVariant())
            self.ui.test_result_tab.item(2, 6).setText("-")
            self.ui.test_result_tab.item(2, 6).setData(QtCore.Qt.BackgroundRole, QVariant())

    def update_detected_coverage(self, detected_coverage_dict):
        # Update total detected test point
        self.current_total_detected_test_point = detected_coverage_dict["total_detected"]

        current_test_type = self.ui.test_type_cbox.currentIndex()  # 0:Horizontal; 1:Vertical, 2:Manual
        if current_test_type == 0:
            self.ui.test_result_tab.item(0, 4).setText(str(detected_coverage_dict["total_detected"]))
            self.ui.test_result_tab.item(0, 5).setText(str(detected_coverage_dict["total_coverage"]))
        elif current_test_type == 1:
            # Fill up parameter
            # vertical_sub_test_to_test_dict = {"A": [total test point to test, detected test point]}
            if self.ui.vertical_test_type_cbox.currentIndex() == 0:
                self.vertical_sub_test_to_test_dict["A"] = [self.total_vertical_test_a_point_to_test,
                                                            detected_coverage_dict["total_detected"]]
                self.ui.test_result_tab.item(1, 4).setText(str(detected_coverage_dict["total_detected"]))
            elif self.ui.vertical_test_type_cbox.currentIndex() == 1:
                self.vertical_sub_test_to_test_dict["B"] = [self.total_vertical_test_b_point_to_test,
                                                            detected_coverage_dict["total_detected"]]
                if self.require_test_c:
                    self.ui.test_result_tab.item(1, 4).setText(str(detected_coverage_dict["total_detected"]))
                else:
                    self.ui.test_result_tab.item(2, 4).setText(str(detected_coverage_dict["total_detected"]))
            else:
                self.vertical_sub_test_to_test_dict["C"] = [self.total_vertical_test_c_point_to_test,
                                                            detected_coverage_dict["total_detected"]]
                self.ui.test_result_tab.item(2, 4).setText(str(detected_coverage_dict["total_detected"]))
        else:
            pass

    def update_overall_result(self, result):
        if result == "READY" or result == "NO TEST":
            background_color = QtGui.QBrush(QtGui.QColor(0, 128, 255))  # blue
        elif result == "PROCESSING":
            background_color = QtGui.QBrush(QtGui.QColor(255, 255, 0))  # yellow
        elif result == "PASS":
            background_color = QtGui.QBrush(QtGui.QColor(0, 255, 0))  # green
        else:  # Error/Fail
            background_color = QtGui.QBrush(QtGui.QColor(255, 0, 0))  # red

        if self.ui.test_type_cbox.currentIndex() == 0:
            self.ui.test_result_tab.item(0, 6).setText(result)
            self.ui.test_result_tab.item(0, 6).setBackground(background_color)
        elif self.ui.test_type_cbox.currentIndex() == 1:
            self.current_vertical_result = result
            all_test, all_sub_to_test = self.check_required_sub_test_successfully_run()

            if all_sub_to_test:
                total_to_test = 0
                total_detected = 0
                for sub_test in all_test:
                    sub_test_parameter = self.vertical_sub_test_to_test_dict[sub_test]
                    sub_test_to_test = sub_test_parameter[0]
                    sub_test_detected = sub_test_parameter[1]
                    total_to_test += sub_test_to_test
                    total_detected += sub_test_detected

                try:
                    total_coverage = total_detected / total_to_test
                except ZeroDivisionError:
                    total_coverage = 0

                total_coverage_percentage = total_coverage * 100
                total_coverage_percentage = float("{:.2f}".format(total_coverage_percentage))

                self.ui.test_result_tab.item(1, 5).setText(str(total_coverage_percentage))

                self.current_detected_coverage = total_coverage_percentage

                # Check detected coverage for vertical
                current_vertical_minimum_coverage = float(self.ui.test_result_tab.item(1, 3).text())

                # Check overall result
                if result == "PASS" or result == "FAIL":
                    if self.current_detected_coverage >= current_vertical_minimum_coverage:
                        result = "PASS"
                        background_color = QtGui.QBrush(QtGui.QColor(0, 255, 0))  # green
                    else:
                        result = "FAIL"
                        background_color = QtGui.QBrush(QtGui.QColor(255, 0, 0))  # red
                else:
                    pass

                self.ui.test_result_tab.item(1, 6).setText(result)
                self.ui.test_result_tab.item(1, 6).setBackground(background_color)
            else:
                self.current_detected_coverage = 0.0
                self.ui.test_result_tab.item(1, 6).setText("-")
                self.ui.test_result_tab.item(1, 6).setData(QtCore.Qt.BackgroundRole, QVariant())

    def update_overall_result_interrupted(self, result):
        if self.ui.test_type_cbox.currentIndex() == 0:
            self.current_horizontal_result = result
        elif self.ui.test_type_cbox.currentIndex() == 1:
            self.current_vertical_result = result

    def run_test_thread(self, test_to_run, total_test_to_run, horizontal_mode, vertical_mode, manual_mode):
        run_from_interupted = self.interrupt_requested

        # Reset flag
        self.test_running = True
        self.interrupt_requested = False

        self.test_thread = QThread()
        self.test_worker = TestWorker(test_to_run=test_to_run, total_test_to_run=total_test_to_run,
                                      device_config=self.device_config, standard_config=self.standard_config,
                                      horizontal_mode=horizontal_mode, vertical_mode=vertical_mode,
                                      vertical_custom_point=self.vertical_custom_point, manual_mode=manual_mode,
                                      x_spinbox=self.x_spinbox, y_spinbox=self.y_spinbox,
                                      curr_total_detected_tp=self.current_total_detected_test_point,
                                      run_from_interrupted=run_from_interupted, motor_serial=self.motor_serial,
                                      motor_serial_changed=self.motor_serial_changed,
                                      power_supply_serial=self.power_supply)
        self.test_worker.moveToThread(self.test_thread)
        # Connect signals and slots
        self.test_thread.started.connect(self.reset_button_before_test)
        self.test_thread.started.connect(self.reset_result_before_test)
        self.test_thread.started.connect(self.test_worker.run)
        self.test_worker.started.connect(self.report_progress)
        self.test_worker.finished.connect(self.test_thread.quit)
        self.test_worker.finished.connect(self.test_worker.deleteLater)
        self.test_thread.finished.connect(self.test_thread.deleteLater)
        self.test_worker.finished.connect(self.report_progress)
        self.test_worker.horizontal_test_point_progress.connect(self.horizontal_test_point_progress)
        self.test_worker.vertical_test_point_progress.connect(self.vertical_test_point_progress)
        self.test_worker.run_status_progress.connect(self.report_status_progress)
        self.test_worker.total_test_point_detected_progress.connect(self.update_detected_coverage)
        self.test_worker.test_result_progress.connect(self.update_overall_result)
        self.test_worker.motor_home_first.connect(self.motor_home_first_notification)
        self.test_worker.test_interrupted_progress.connect(self.update_overall_result_interrupted)
        self.test_worker.initial_motor_home_respond.connect(self.update_initial_motor_home_flag)
        self.test_worker.sensor_faulty_signal.connect(self.sensor_faulty_notification)

        # Start the thread
        self.test_thread.start()

        # After thread finished
        self.test_thread.finished.connect(self.after_test_interpretation)

    def update_interrupt_flag(self, is_interrupt):
        if is_interrupt:
            self.interrupt_requested = True
        else:
            self.interrupt_requested = False

    def reset_test_point_after_test_display(self):
        if self.ui.test_type_cbox.currentIndex() == 0:  # Horizontal
            for idx, btn in enumerate(self.tp_horizontal_btn_list):
                btn = getattr(self.ui, btn)
                btn_to_run = self.test_to_run_list[idx]

                if btn_to_run:
                    btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                else:
                    btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
        elif self.ui.test_type_cbox.currentIndex() == 1:  # Vertical
            for idx, btn in enumerate(self.tp_vertical_btn_list):
                btn = getattr(self.ui, btn)
                btn_to_run = self.test_to_run_list[idx]

                if btn_to_run:
                    btn.setStyleSheet("border-image: url(" + self.yellow_circle_icon_path + ")")
                else:
                    btn.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")
        else:
            pass

    def reset_button_before_test(self):
        # Disable combo box
        self.ui.test_type_cbox.setDisabled(True)

        # Disable run
        self.ui.run_test_btn.setDisabled(True)
        self.ui.run_test_btn.setStyleSheet("border-image: url(" + self.run_disable_icon_path + ")")

        # Enable pause
        self.ui.pause_test_btn.setEnabled(True)
        self.ui.pause_test_btn.setStyleSheet("border-image: url(" + self.pause_icon_path + ")")

        # Enable stop
        self.ui.stop_test_btn.setEnabled(True)
        self.ui.stop_test_btn.setStyleSheet("border-image: url(" + self.stop_icon_path + ")")

        # Disable test point
        for btn in self.tp_horizontal_btn_list:
            btn = getattr(self.ui, btn)
            btn.setDisabled(True)
        for btn in self.tp_vertical_btn_list:
            btn = getattr(self.ui, btn)
            btn.setDisabled(True)

    def reset_button_after_test(self):
        # Enable combo box
        self.ui.test_type_cbox.setEnabled(True)

        # Enable run
        self.ui.run_test_btn.setEnabled(True)
        self.ui.run_test_btn.setStyleSheet("border-image: url(" + self.run_icon_path + ")")

        # Disabled pause
        self.ui.pause_test_btn.setDisabled(True)
        self.ui.pause_test_btn.setStyleSheet("border-image: url(" + self.pause_disable_icon_path + ")")

        # Disabled stop
        self.ui.stop_test_btn.setDisabled(True)
        self.ui.stop_test_btn.setStyleSheet("border-image: url(" + self.stop_disable_icon_path + ")")

        # Enable test point
        for btn in self.tp_horizontal_btn_list:
            btn = getattr(self.ui, btn)
            btn.setEnabled(True)
        for btn in self.tp_vertical_btn_list:
            btn = getattr(self.ui, btn)
            btn.setEnabled(True)

    def check_required_sub_test_successfully_run(self):
        sub_test_ran = []
        for keys in self.vertical_sub_test_to_test_dict.keys():
            sub_test_parameter = self.vertical_sub_test_to_test_dict[keys]
            if len(sub_test_parameter) != 0:
                sub_test_ran.append(keys)
        if len(sub_test_ran) == 2:
            all_sub_to_test = True
        else:
            all_sub_to_test = False

        return sub_test_ran, all_sub_to_test

    def update_vertical_sub_test_combobox(self):
        if self.current_vertical_result == "PASS" or self.current_vertical_result == "FAIL":
            vertical_run_successfully = True
        else:
            vertical_run_successfully = False
            if self.ui.vertical_test_type_cbox.currentIndex() == 0:
                self.vertical_sub_test_to_test_dict["A"] = []
            elif self.ui.vertical_test_type_cbox.currentIndex() == 1:
                self.vertical_sub_test_to_test_dict["B"] = []
            else:
                self.vertical_sub_test_to_test_dict["C"] = []

        # Update tickbox in combobox
        if self.ui.vertical_test_type_cbox.currentIndex() == 0:
            if vertical_run_successfully:
                self.ui.vertical_test_type_cbox.setItemIcon(0, QtGui.QIcon(self.green_check_icon_path))
            else:
                self.ui.vertical_test_type_cbox.setItemIcon(0, QtGui.QIcon(self.red_check_icon_path))
        elif self.ui.vertical_test_type_cbox.currentIndex() == 1:
            if vertical_run_successfully:
                self.ui.vertical_test_type_cbox.setItemIcon(1, QtGui.QIcon(self.green_check_icon_path))
            else:
                self.ui.vertical_test_type_cbox.setItemIcon(1, QtGui.QIcon(self.red_check_icon_path))
        else:
            if vertical_run_successfully:
                self.ui.vertical_test_type_cbox.setItemIcon(2, QtGui.QIcon(self.green_check_icon_path))
            else:
                self.ui.vertical_test_type_cbox.setItemIcon(2, QtGui.QIcon(self.red_check_icon_path))

        # Check if all required vertical sub test successfully run, then calculate detected coverage
        all_test, all_sub_to_test = self.check_required_sub_test_successfully_run()

        if all_sub_to_test:
            total_to_test = 0
            total_detected = 0
            for sub_test in all_test:
                sub_test_parameter = self.vertical_sub_test_to_test_dict[sub_test]
                sub_test_to_test = sub_test_parameter[0]
                sub_test_detected = sub_test_parameter[1]
                total_to_test += sub_test_to_test
                total_detected += sub_test_detected

            try:
                total_coverage = total_detected / total_to_test
            except ZeroDivisionError:
                total_coverage = 0

            total_coverage_percentage = total_coverage * 100
            total_coverage_percentage = float("{:.2f}".format(total_coverage_percentage))

            self.ui.test_result_tab.item(1, 5).setText(str(total_coverage_percentage))

            self.current_detected_coverage = total_coverage_percentage
        else:
            self.current_detected_coverage = 0.0

    def reset_button_after_paused(self):
        # Enable run
        self.ui.run_test_btn.setEnabled(True)
        self.ui.run_test_btn.setStyleSheet("border-image: url(" + self.run_icon_path + ")")

        # Disabled pause
        self.ui.pause_test_btn.setDisabled(True)
        self.ui.pause_test_btn.setStyleSheet("border-image: url(" + self.pause_disable_icon_path + ")")

    def add_test_to_log_df(self):
        # Prepare csv path
        current_date = QDateTime.currentDateTime().toString('yyMMdd')
        csv_file = f'{current_date}_log.pkl'

        # Prepare dir
        csv_outdir = 'logs/operation logs'
        os.makedirs(csv_outdir, exist_ok=True)
        csv_path = f'{csv_outdir}/{csv_file}'

        # Create csv if does not exist
        if not os.path.exists(csv_path):
            df_col = pd.read_pickle(LOG_DIR + "/init.pkl")
            df_col.to_pickle(csv_path)

        # Get parameter need to update
        end_date = QDateTime.currentDateTime().toString('yyyy/MM/dd hh:mm:ss')
        if self.ui.test_type_cbox.currentIndex() == 0:
            horizontal_detected_test_point = self.ui.test_result_tab.item(0, 4).text()
            total_detected_coverage = self.ui.test_result_tab.item(0, 5).text()
            overall_result = self.ui.test_result_tab.item(0, 6).text()

            # Update save to csv dict
            self.save_to_csv["end_date"] = end_date
            self.save_to_csv["test_horizontal_detected_test_point"] = horizontal_detected_test_point
            self.save_to_csv["test_detected_coverage"] = total_detected_coverage
            self.save_to_csv["overall_result"] = overall_result

            # Write to pickle
            df = pd.read_pickle(csv_path)
            new_row = {}
            for each_result in self.save_to_csv:
                selected_category = each_result
                selected_result = self.save_to_csv[each_result]
                new_row[selected_category] = str(selected_result)
            df = df.append(new_row, ignore_index=True)
            df.to_pickle(csv_path)

            # Update in overall df log
            self.log_df = self.log_df.append(new_row, ignore_index=True)

            # Output report
            self.output_report()

        elif self.ui.test_type_cbox.currentIndex() == 1:
            # Check if all required vertical sub test successfully run, then calculate detected coverage
            _, all_sub_to_test = self.check_required_sub_test_successfully_run()
            if all_sub_to_test:
                first_total_test_point = self.ui.test_result_tab.item(1, 2).text()
                first_detected_test_point = self.ui.test_result_tab.item(1, 4).text()
                second_total_test_point = self.ui.test_result_tab.item(2, 2).text()
                second_detected_test_point = self.ui.test_result_tab.item(2, 4).text()
                total_detected_coverage = self.ui.test_result_tab.item(1, 5).text()
                overall_result = self.ui.test_result_tab.item(1, 6).text()

                # Update save to csv dict
                self.save_to_csv["end_date"] = end_date
                self.save_to_csv["test_sub_type_first_total_test_point"] = first_total_test_point
                self.save_to_csv["test_sub_type_first_detected_test_point"] = first_detected_test_point
                self.save_to_csv["test_sub_type_second_total_test_point"] = second_total_test_point
                self.save_to_csv["test_sub_type_second_detected_test_point"] = second_detected_test_point
                self.save_to_csv["test_detected_coverage"] = total_detected_coverage
                self.save_to_csv["overall_result"] = overall_result

                # Write to pickle
                df = pd.read_pickle(csv_path)
                new_row = {}
                for each_result in self.save_to_csv:
                    selected_category = each_result
                    selected_result = self.save_to_csv[each_result]
                    new_row[selected_category] = str(selected_result)
                df = df.append(new_row, ignore_index=True)
                df.to_pickle(csv_path)

                # Update in overall df log
                self.log_df = self.log_df.append(new_row, ignore_index=True)

                # Output report
                self.output_report()
        else:
            pass

    def after_test_interpretation(self):
        # Test worker flag
        self.test_running = False

        if self.interrupt_requested:
            self.report_status_progress("PAUSED")
            self.reset_button_after_paused()
        else:
            if self.ui.test_type_cbox.currentIndex() == 0:
                self.add_test_to_log_df()
                pass
            elif self.ui.test_type_cbox.currentIndex() == 1:
                self.update_vertical_sub_test_combobox()
                self.add_test_to_log_df()
            else:
                pass

            self.show_info_popup("TEST FINISHED")

            self.reset_button_after_test()
            self.reset_result_after_test()
            self.reset_test_point_after_test_display()
            self.report_status_progress("READY")

            # Reset flag
            self.save_to_csv = {}
            self.current_total_detected_test_point = 0

    def output_report(self):
        # self.ui.statusbar.showMessage("Saving to log...")

        # Copy dataframe
        df_log_copy = self.log_df.copy()

        # Get latest date of saved log
        df_log_copy["start_date"] = pd.to_datetime(df_log_copy['start_date'], format='%Y/%m/%d %H:%M:%S')
        latest_date = df_log_copy["start_date"].max()
        date_parameter = latest_date.strftime("%d/%m/%Y")
        date_filename = latest_date.strftime('%y%m%d')

        # Filter log df
        filtered_df = ResultHandler(log_df=df_log_copy, operator_name=False, vehicle_family=False,
                                    vehicle_id=False, standard=False, test_type=False,
                                    start_date=date_parameter, end_date=date_parameter).filter_result()

        # Check availability output path
        filename = f'{date_filename}_Operation Test Report.xlsx'
        dir_path = REPORT_GENERATED_FOLDER_PATH
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        file_path = dir_path + filename

        # Output excel based on latest date
        try:
            OutputExcelHandler(format_excel_path=FORMAT_REPORT_PATH, output_excel_path=file_path,
                               log_df=filtered_df).generate()

            system_log = f'Result successfully saved in {file_path}'
            print(system_log)
            logging.info(system_log)
        except PermissionError:
            system_log = f'{file_path} fail to save!'
            print(system_log)
            logging.error(system_log)

    def horizontal_test_point_progress(self, test_point_dict):
        test_point_index = test_point_dict["index"]
        test_point_result = test_point_dict["result"]

        button_config = self.tp_horizontal_btn_list[test_point_index - 1]
        button_config = getattr(self.ui, button_config)

        if test_point_result == "PASS":
            button_config.setStyleSheet("border-image: url(" + self.green_circle_icon_path + ")")
        elif test_point_result == "NG":
            button_config.setStyleSheet("border-image: url(" + self.red_circle_icon_path + ")")
        elif test_point_result == "ERROR":
            button_config.setStyleSheet("border-image: url(" + self.error_circle_icon_path + ")")
        elif test_point_result == "SKIP":
            pass
        else:  # NO TEST
            button_config.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")

    def vertical_test_point_progress(self, test_point_dict):
        test_point_index = test_point_dict["index"]
        test_point_result = test_point_dict["result"]

        button_config = self.tp_vertical_btn_list[test_point_index - 1]
        button_config = getattr(self.ui, button_config)

        if test_point_result == "PASS":
            button_config.setStyleSheet("border-image: url(" + self.green_circle_icon_path + ")")
        elif test_point_result == "NG":
            button_config.setStyleSheet("border-image: url(" + self.red_circle_icon_path + ")")
        elif test_point_result == "ERROR":
            button_config.setStyleSheet("border-image: url(" + self.error_circle_icon_path + ")")
        elif test_point_result == "SKIP":
            pass
        else:  # NO TEST
            button_config.setStyleSheet("border-image: url(" + self.grey_circle_icon_path + ")")

    def update_vehicle_combobox(self):
        # Copy dataframe
        vehicle_df = self.vehicle_dataframe.copy()

        if not vehicle_df.empty:
            self.ui.vehicle_family_cbox.clear()
            # Vehicle combobox
            # Filter based on parameter
            vehicle_family_value = vehicle_df[["vehicle_family"]].values.ravel()
            vehicle_family_unique_value = list(pd.unique(vehicle_family_value))
            vehicle_family_unique_value = [str(i) for i in vehicle_family_unique_value]
            self.ui.vehicle_family_cbox.addItems(vehicle_family_unique_value)

            # Update test combo box
            self.on_test_type_cbox_changed()
        else:
            # Update combobox
            self.ui.vehicle_family_cbox.clear()
            self.ui.vehicle_id_cbox.clear()

            # Update test combo box
            self.on_test_type_cbox_changed()

            # Reset display model info
            self.reset_display_model_info()

    def update_standard_combobox(self):
        # Copy dataframe
        standard_df = self.standard_dataframe.copy()

        if not standard_df.empty:
            self.ui.standard_cbox.clear()
            # Standard combobox
            # Filter based on parameter
            standard_name_value = standard_df[["standard_name"]].values.ravel()
            standard_name_unique_value = list(pd.unique(standard_name_value))
            standard_name_unique_value = [str(i) for i in standard_name_unique_value]
            self.ui.standard_cbox.addItems(standard_name_unique_value)
        else:
            # Update combo box
            self.ui.standard_cbox.clear()

            # Reset display standard
            self.reset_display_standard_info()

    @staticmethod
    def show_info_popup(val):
        msg = QMessageBox()
        msg.setWindowTitle("Info")
        msg.setText(val)
        msg.setIcon(QMessageBox.Information)
        msg.setWindowModality(QtCore.Qt.NonModal)
        msg.exec_()

    @staticmethod
    def show_alert_popup(val):
        msg = QMessageBox()
        msg.setWindowTitle("Error")
        msg.setText(val)
        msg.setIcon(QMessageBox.Critical)
        msg.setWindowModality(QtCore.Qt.NonModal)
        msg.exec_()

    @staticmethod
    def show_dialog_popup(val, details=None):
        msg = QMessageBox()
        msg.setWindowTitle("Info")
        msg.setText(val)
        msg.setIcon(QMessageBox.Question)
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        if details:
            msg.setDetailedText(details)
        ans = msg.exec_()
        return ans


class TestWorker(QtCore.QObject):
    started = pyqtSignal(str)
    finished = pyqtSignal(str)
    progress = pyqtSignal(str)
    mcu_progress = pyqtSignal(str)
    horizontal_test_point_progress = pyqtSignal(dict)
    vertical_test_point_progress = pyqtSignal(dict)
    total_test_point_detected_progress = pyqtSignal(dict)
    run_status_progress = pyqtSignal(str)
    test_result_progress = pyqtSignal(str)
    motor_home_first = pyqtSignal(tuple)
    test_interrupted_progress = pyqtSignal(str)
    initial_motor_home_respond = pyqtSignal(bool)
    sensor_faulty_signal = pyqtSignal(bool)

    def __init__(self, test_to_run=None, total_test_to_run=None, model_config_param=None, device_config=None,
                 standard_config=None, horizontal_mode=None, vertical_mode=None, vertical_custom_point=None,
                 manual_mode=None, x_spinbox=None, y_spinbox=None, curr_total_detected_tp=None,
                 run_from_interrupted=None, motor_serial=None, motor_serial_changed=None, power_supply_serial=None):
        super().__init__()
        self.test_to_run = test_to_run
        self.model_config = model_config_param
        self.dev_config = device_config
        self.standard_config = standard_config
        self.horizontal_mode = horizontal_mode
        self.vertical_mode = vertical_mode
        self.manual_mode = manual_mode
        self.x_spinbox = x_spinbox
        self.y_spinbox = y_spinbox
        self.vertical_custom_point = vertical_custom_point
        self.run_from_interrupted = run_from_interrupted
        self.motor_serial = motor_serial
        self.motor_serial_changed = motor_serial_changed
        self.power_supply = power_supply_serial

        # Init sensor
        self.sensor1_enabled = False
        self.sensor2_enabled = False
        self.sensor3_enabled = False
        self.sensor4_enabled = False

        # Init horizontal line test
        self.horizontal_line1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        self.horizontal_line2 = [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]
        self.horizontal_line3 = [30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44]
        self.horizontal_line4 = [45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58]

        # Init vertical line test
        self.vertical_line1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        self.vertical_line2 = [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]

        # Init total test to run
        self.total_test_to_run = total_test_to_run

        # Init total test point detected
        self.total_test_point_detected = curr_total_detected_tp

        # Init detected coverage percentage
        self.detected_coverage_percentage = 0.0

        # Init motor offset
        self.motor_offset = 1400

        # Init pause/stop flag
        self.pause_interupt = False
        self.stop_interupt = False

    def check_motor_axis_speed(self, test_idx):
        # zone1_xspeed = int(self.dev_config["zone1_speedx"])
        # zone1_yspeed = int(self.dev_config["zone1_speedy"])
        # zone1_timeout = int(self.dev_config["zone1_timeout"])
        # zone2_xspeed = int(self.dev_config["zone2_speedx"])
        # zone2_yspeed = int(self.dev_config["zone2_speedy"])
        # zone2_timeout = int(self.dev_config["zone2_timeout"])
        zone3_xspeed = int(self.dev_config["zone3_speedx"])
        zone3_yspeed = int(self.dev_config["zone3_speedy"])
        zone3_timeout = int(self.dev_config["zone3_timeout"])
        x_axis, x_speed, y_axis, y_speed, motor_timeout = 0, zone3_xspeed, 0, zone3_yspeed, zone3_timeout

        if self.horizontal_mode:
            if test_idx == 1:  # LINE 1
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 100, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 2:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 200, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 3:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 400, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 4:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 600, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 5:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 800, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 6:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1000, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 7:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1200, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 8:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1400, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 9:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1600, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 10:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1800, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 11:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2000, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 12:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2200, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 13:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2400, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 14:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2600, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 15:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2700, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 16:  # LINE 2
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2700, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 17:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2500, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 18:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2300, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 19:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2100, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 20:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1900, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 21:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1700, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 22:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1500, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 23:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1300, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 24:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1100, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 25:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 900, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 26:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 700, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 27:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 500, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 28:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 300, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 29:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 100, zone3_xspeed, 800, zone3_yspeed, zone3_timeout
            elif test_idx == 30:  # LINE 3
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 100, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 31:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 200, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 32:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 400, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 33:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 600, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 34:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 800, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 35:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1000, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 36:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1200, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 37:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1400, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 38:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1600, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 39:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1800, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 40:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2000, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 41:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2200, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 42:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2400, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 43:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2600, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 44:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2700, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 45:  # LINE 4
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2700, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 46:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2500, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 47:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2300, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 48:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2100, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 49:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1900, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 50:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1700, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 51:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1500, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 52:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1300, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 53:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1100, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 54:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 900, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 55:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 700, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 56:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 500, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 57:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 300, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            elif test_idx == 58:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 100, zone3_xspeed, 400, zone3_yspeed, zone3_timeout
            else:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 0, zone3_xspeed, 0, zone3_yspeed, zone3_timeout

        if self.vertical_mode:
            right_pos = self.vertical_custom_point
            left_pos = self.vertical_custom_point * -1
            right_pos_x_axis = int(self.motor_offset - right_pos)
            left_pos_x_axis = int(self.motor_offset - left_pos)

            if test_idx == 1:  # LINE 1
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 200, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 2:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 400, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 3:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 600, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 4:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = right_pos_x_axis, zone3_xspeed, 1600, zone3_yspeed, \
                                                                  zone3_timeout
            elif test_idx == 5:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 800, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 6:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1000, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 7:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1200, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 8:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1400, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 9:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1600, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 10:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1800, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 11:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2000, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 12:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = left_pos_x_axis, zone3_xspeed, 1600, zone3_yspeed, \
                                                                  zone3_timeout
            elif test_idx == 13:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2200, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 14:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2400, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 15:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2600, zone3_xspeed, 1600, zone3_yspeed, zone3_timeout
            elif test_idx == 16:  # LINE 2
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2600, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 17:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2400, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 18:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2200, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 19:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = left_pos_x_axis, zone3_xspeed, 600, zone3_yspeed, \
                                                                  zone3_timeout
            elif test_idx == 20:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 2000, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 21:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1800, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 22:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1600, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 23:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1400, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 24:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1200, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 25:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 1000, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 26:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 800, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 27:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = right_pos_x_axis, zone3_xspeed, 600, zone3_yspeed, \
                                                                  zone3_timeout
            elif test_idx == 28:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 600, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 29:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 400, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            elif test_idx == 30:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 200, zone3_xspeed, 600, zone3_yspeed, zone3_timeout
            else:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = 0, zone3_xspeed, 0, zone3_yspeed, zone3_timeout

        return x_axis, x_speed, y_axis, y_speed, motor_timeout

    def move_motor_home(self):
        self.mcu_progress.emit("Send motor home...")
        # Send motor Home
        x_speed, x_dist, y_speed, y_dist = 0, 0, 0, 0
        motor_req_byte = RequestByte(command_val='H', char_cond=True,
                                     data_val_list=[x_speed, x_dist, y_speed, y_dist],
                                     data_zfill_list=[3, 4, 3, 4]).out_bytearray()

        # Open Com port for Motor Driver
        try:
            if not self.motor_serial.isOpen():
                self.motor_serial.open()
                time.sleep(float(self.dev_config['motor_start_del']))

            system_log = "SEND HOME REQUEST:", motor_req_byte.hex()
            logging.info(system_log)
            print(system_log)
            self.motor_serial.timeout = float(self.dev_config['home_timeout'])
            self.motor_serial.write(motor_req_byte)
            self.motor_serial.reset_input_buffer()
            motor_response = self.motor_serial.read(5)
            system_log = "MOTOR HOME RESPOND:", motor_response.hex()
            logging.info(system_log)
            print(system_log)

            # Start OK
            if motor_response == b'[T\x00T]':
                # motor_ser.timeout = float(self.dev_config['motor_end_home_del'])
                motor_response = self.motor_serial.read(5)
                # Confirm End
                if motor_response == b'[E\x00E]':
                    system_log = "CONFIRM END SIGNAL RECEIVED: OK"
                    logging.info(system_log)
                    print(system_log)
                    motor_respond = True
                    pass
                # Waiting for confirm end timed out
                else:
                    system_log = "CONFIRM END SIGNAL NOT RECEIVED"
                    logging.info(system_log)
                    print(system_log)
                    motor_respond = False
            # Start NOK
            elif motor_response == b'[F\x00F]':
                system_log = "CONFIRM END SIGNAL RECEIVED: NOK"
                logging.info(system_log)
                print(system_log)
                motor_respond = False
            # No response
            else:
                system_log = "CONFIRM START SIGNAL NOT RECEIVED"
                logging.info(system_log)
                print(system_log)
                motor_respond = False
        except SerialException:
            motor_respond = False

        return motor_respond

    def move_motor_to_point(self, x_axis, x_speed, y_axis, y_speed, motor_timeout):

        # Motor request
        motor_req_byte = RequestByte(command_val='M', char_cond=True, data_val_list=[x_speed, x_axis, y_speed, y_axis],
                                     data_zfill_list=[3, 4, 3, 4]).out_bytearray()

        system_log = f"MOVE MOTOR REQUEST: {motor_req_byte.hex()}"
        logging.info(system_log)
        print(system_log)

        # Open Com port for Motor Driver
        try:
            if not self.motor_serial.isOpen():
                self.motor_serial.open()
                time.sleep(float(self.dev_config['motor_start_del']))

            self.motor_serial.timeout = int(motor_timeout)
            self.motor_serial.write(motor_req_byte)
            self.motor_serial.reset_input_buffer()
            serial_string = self.motor_serial.read(5)
            serial_string_hex = serial_string.hex()
            system_log = f"MOVE MOTOR RESPONSE: {serial_string_hex}"
            logging.info(system_log)
            print(system_log)

            if serial_string == b'[T\x00T]':
                system_log = "CONFIRM START SIGNAL: OK"
                logging.info(system_log)
                print(system_log)
                motor_response = self.motor_serial.read(5)
                # Confirm End
                if motor_response == b'[E\x00E]':
                    system_log = f"CONFIRM END SIGNAL RECEIVED: {motor_response.hex()}"
                    logging.info(system_log)
                    print(system_log)
                    motor_respond = True
                # Waiting for confirm end timed out
                else:
                    system_log = f"CONFIRM END SIGNAL NOT RECEIVED: {motor_response.hex()}"
                    logging.info(system_log)
                    print(system_log)
                    motor_respond = False
            elif serial_string == b'[F\x00F]':
                system_log = "CONFIRM START SIGNAL: NOK"
                logging.info(system_log)
                print(system_log)
                motor_respond = False
            else:
                system_log = f"CONFIRM START SIGNAL NOT RECEIVED: {serial_string.hex()}"
                logging.info(system_log)
                print(system_log)
                motor_respond = False
        except SerialException:
            motor_respond = False

        return motor_respond

    def check_sensor_enable(self):

        # Enable sensor 1
        if not self.sensor1_enabled:
            mcu_cmd = self.dev_config['cmd_enable_sensor1']
            mcu_port = self.dev_config['mcu_com_port']
            mcu_baud_rate = self.dev_config['mcu_baud_rate']
            mcu_start_false_cmd = self.dev_config['cmd_start_false']
            mcu_repetition_request = self.dev_config['mcu_repetition_request_time']
            timeout_set = self.dev_config['tmcu_enable_sensor1']
            mcu_resp_len = self.dev_config['rlmcu_enable_sensor1']
            data_pos = ['true_false']

            enable_sensor1_respond = mcu_enable_sensor(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
                                                       mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
            self.sensor1_enabled = enable_sensor1_respond

        # # Enable sensor 2
        # if not self.sensor2_enabled:
        #     mcu_cmd = self.dev_config['cmd_start_of_operation']
        #     mcu_port = self.dev_config['mcu_com_port']
        #     mcu_baud_rate = self.dev_config['mcu_baud_rate']
        #     mcu_start_false_cmd = self.dev_config['cmd_start_false']
        #     mcu_repetition_request = self.dev_config['mcu_repetition_request_time']
        #     timeout_set = self.dev_config['tmcu_mcu_status']
        #     mcu_resp_len = self.dev_config['rlmcu_start_of_operation']
        #     data_pos = ['true_false']
        #
        #     enable_sensor2_respond = mcu_enable_sensor(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
        #                                                mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        #     self.sensor2_enabled = enable_sensor2_respond
        #
        # # Enable sensor 3
        # if not self.sensor3_enabled:
        #     mcu_cmd = self.dev_config['cmd_start_of_operation']
        #     mcu_port = self.dev_config['mcu_com_port']
        #     mcu_baud_rate = self.dev_config['mcu_baud_rate']
        #     mcu_start_false_cmd = self.dev_config['cmd_start_false']
        #     mcu_repetition_request = self.dev_config['mcu_repetition_request_time']
        #     timeout_set = self.dev_config['tmcu_mcu_status']
        #     mcu_resp_len = self.dev_config['rlmcu_start_of_operation']
        #     data_pos = ['true_false']
        #
        #     enable_sensor3_respond = mcu_enable_sensor(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
        #                                                mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        #     self.sensor3_enabled = enable_sensor3_respond
        #
        # # Enable sensor 4
        # if not self.sensor4_enabled:
        #     mcu_cmd = self.dev_config['cmd_start_of_operation']
        #     mcu_port = self.dev_config['mcu_com_port']
        #     mcu_baud_rate = self.dev_config['mcu_baud_rate']
        #     mcu_start_false_cmd = self.dev_config['cmd_start_false']
        #     mcu_repetition_request = self.dev_config['mcu_repetition_request_time']
        #     timeout_set = self.dev_config['tmcu_mcu_status']
        #     mcu_resp_len = self.dev_config['rlmcu_start_of_operation']
        #     data_pos = ['true_false']
        #
        #     enable_sensor4_respond = mcu_enable_sensor(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
        #                                                mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        #     self.sensor4_enabled = enable_sensor4_respond

        all_sensor_enable_respond = all([self.sensor1_enabled])

        return all_sensor_enable_respond

    def check_sensor_request(self, test_no):
        mcu_cmd = self.dev_config['cmd_check_sensor1']
        mcu_port = self.dev_config['mcu_com_port']
        mcu_baud_rate = self.dev_config['mcu_baud_rate']
        mcu_start_false_cmd = self.dev_config['cmd_start_false']
        mcu_repetition_request = self.dev_config['mcu_repetition_request_time']
        timeout_set = self.dev_config['tmcu_zone3']
        mcu_resp_len = self.dev_config['rlmcu_zone3']
        data_pos = ['buzzer_high', 'buzzer_low']

        run_stat, result_list = mcu_check_sensor(test_no, mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
                                                   mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        return run_stat, result_list

    def check_mcu_status(self):
        if not BYPASS_MCU_STATUS:
            mcu_cmd = self.dev_config['cmd_mcu_status']
            mcu_port = self.dev_config['mcu_com_port']
            mcu_baud_rate = self.dev_config['mcu_baud_rate']
            mcu_start_false_cmd = self.dev_config['cmd_start_false']
            mcu_repetition_request = self.dev_config['mcu_repetition_request_time']
            timeout_set = self.dev_config['tmcu_mcu_status']
            mcu_resp_len = self.dev_config['rlmcu_mcu_status']
            data_pos = ['true_false']

            mcu_respond = mcu_check_mcu_status(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
                                               mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        else:
            mcu_respond = True

        return mcu_respond

    def switch_on_ecu(self):
        if USE_IF_MCU:
            mcu_cmd = self.dev_config['cmd_switch_on_ecu']
            mcu_port = self.dev_config['mcu_com_port']
            mcu_baud_rate = self.dev_config['mcu_baud_rate']
            mcu_start_false_cmd = self.dev_config['cmd_start_false']
            mcu_repetition_request = self.dev_config['mcu_repetition_request_time']
            timeout_set = self.dev_config['tmcu_switch_on_ecu']
            mcu_resp_len = self.dev_config['rlmcu_switch_on_ecu']
            data_pos = ['true_false']

            mcu_respond = mcu_turn_on_ecu_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
                                                mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        else:
            mcu_respond = True

        return mcu_respond

    def switch_off_ecu(self):
        if USE_IF_MCU:
            mcu_cmd = self.dev_config['cmd_switch_off_ecu']
            mcu_port = self.dev_config['mcu_com_port']
            mcu_baud_rate = self.dev_config['mcu_baud_rate']
            mcu_start_false_cmd = self.dev_config['cmd_start_false']
            mcu_repetition_request = self.dev_config['mcu_repetition_request_time']
            timeout_set = self.dev_config['tmcu_switch_off_ecu']
            mcu_resp_len = self.dev_config['rlmcu_switch_off_ecu']
            data_pos = ['true_false']

            mcu_respond = mcu_turn_off_ecu_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
                                                 mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        else:
            mcu_respond = True

        return mcu_respond

    def switch_on_rev(self):
        if USE_IF_MCU:
            mcu_cmd = self.dev_config['cmd_switch_on_rev']
            mcu_port = self.dev_config['mcu_com_port']
            mcu_baud_rate = self.dev_config['mcu_baud_rate']
            mcu_start_false_cmd = self.dev_config['cmd_start_false']
            mcu_repetition_request = self.dev_config['mcu_repetition_request_time']
            timeout_set = self.dev_config['tmcu_switch_on_rev']
            mcu_resp_len = self.dev_config['rlmcu_switch_on_rev']
            data_pos = ['true_false']

            mcu_respond = mcu_turn_on_rev_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
                                                mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        else:
            mcu_respond = True

        return mcu_respond

    def switch_off_rev(self):
        if USE_IF_MCU:
            mcu_cmd = self.dev_config['cmd_switch_off_rev']
            mcu_port = self.dev_config['mcu_com_port']
            mcu_baud_rate = self.dev_config['mcu_baud_rate']
            mcu_start_false_cmd = self.dev_config['cmd_start_false']
            mcu_repetition_request = self.dev_config['mcu_repetition_request_time']
            timeout_set = self.dev_config['tmcu_switch_off_rev']
            mcu_resp_len = self.dev_config['rlmcu_switch_off_rev']
            data_pos = ['true_false']

            mcu_respond = mcu_turn_off_rev_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
                                                 mcu_repetition_request, timeout_set, mcu_resp_len, data_pos)
        else:
            mcu_respond = True

        return mcu_respond

    def check_overall_result(self, result_list):
        all_passed = all(x == "PASS" for x in result_list)
        all_failed = all(x == "NG" for x in result_list)
        all_no_test = all(x == "NO TEST" for x in result_list)

        if not self.manual_mode:
            if "ERROR" in result_list:
                overall_result = "ERROR"
            else:
                if all_no_test:
                    overall_result = "NO TEST"
                else:
                    minimum_coverage = float(self.standard_config["coverage_horizontal_minimum_percentage"])
                    if self.detected_coverage_percentage >= minimum_coverage:
                        overall_result = "PASS"
                    else:
                        overall_result = "FAIL"
        else:
            overall_result = "PASS"
            if "ERROR" in result_list:
                overall_result = "ERROR"
            else:
                if not all_passed and not all_failed and not all_no_test:
                    if "NG" in result_list:
                        overall_result = "FAIL"
                    else:
                        overall_result = "PASS"
                else:
                    if all_passed:
                        overall_result = "PASS"
                    if all_failed:
                        overall_result = "FAIL"
                    if all_no_test:
                        overall_result = "NO TEST"

        return overall_result

    def check_sensor_result(self, current_result, run_status):
        sensor_faulty = False
        test_no = current_result[0]
        try:
            current_valh = int(current_result[1])
            current_vall = int(current_result[2])
        except IndexError:
            current_valh = False
            current_vall = False

        if current_valh is not False and current_vall is not False:
            # If buzzer high value is 1000 and buzzer low is 0, set buzzer high value to 300 and buzzer low to 0
            # If buzzer high & low value is 9999, sensor is faulty
            if current_valh == 1000 and current_vall == 0:
                current_valh = 600
                current_vall = 0
            elif current_valh == 0 and current_vall == 1000:
                current_valh = 0
                current_vall = 600
            elif current_valh == 9999 and current_vall == 9999:
                sensor_faulty = True
                run_status = False
            else:
                pass

            if not sensor_faulty:
                # Manual mode spec
                current_valspech = 600
                current_valspecl = 0
                current_thresh = 5
                # Horizontal mode spec
                if self.horizontal_mode:
                    if test_no in self.horizontal_line1:
                        current_valspech = 300
                        current_valspecl = 300
                        current_thresh = 5
                    elif test_no in self.horizontal_line2:
                        current_valspech = 600
                        current_valspecl = 0
                        current_thresh = 5
                    elif test_no in self.horizontal_line3:
                        current_valspech = 600
                        current_valspecl = 0
                        current_thresh = 5
                    else:  # line 4
                        current_valspech = 600
                        current_valspecl = 0
                        current_thresh = 5
                # Vertical mode spec
                if self.vertical_mode:
                    if test_no in self.vertical_line1:
                        current_valspech = 300
                        current_valspecl = 300
                        current_thresh = 5
                    else:  # line 2
                        current_valspech = 600
                        current_valspecl = 0
                        current_thresh = 5

                thresh_posh = current_valspech + current_thresh
                thresh_negh = current_valspech - current_thresh
                thresh_posl = current_valspecl + current_thresh
                thresh_negl = current_valspecl - current_thresh

                # Check result for buzzer high
                if thresh_posh >= current_valh >= thresh_negh:
                    cond_high = True
                else:
                    cond_high = False

                # Check result for buzzer low
                if thresh_posl >= current_vall >= thresh_negl:
                    cond_low = True
                else:
                    cond_low = False

                if cond_high and cond_low:
                    overall_result = "PASS"
                else:
                    overall_result = "NG"
            else:
                overall_result = "NG"
        else:
            overall_result = "NG"

        return overall_result, run_status, sensor_faulty

    def check_motor_input_value(self):
        x_speed = int(self.dev_config["zone3_speedx"])
        y_speed = int(self.dev_config["zone3_speedy"])
        x_axis = self.motor_offset - self.x_spinbox
        y_axis = self.y_spinbox
        motor_timeout = int(self.dev_config["zone3_timeout"])

        return x_axis, x_speed, y_axis, y_speed, motor_timeout

    def calculate_detected_coverage(self, result):
        if result == "PASS":
            self.total_test_point_detected += 1
        else:
            pass

        try:
            total_coverage = self.total_test_point_detected / self.total_test_to_run
        except ZeroDivisionError:
            total_coverage = 0
        total_coverage_percentage = total_coverage * 100
        self.detected_coverage_percentage = float("{:.2f}".format(total_coverage_percentage))

        return_dict = {"total_detected": self.total_test_point_detected,
                       "total_coverage": self.detected_coverage_percentage}

        return return_dict

    def normal_test_finished(self, overall_result_list):
        # Check overall result
        overall_result = self.check_overall_result(overall_result_list)

        # Emit signal overall result
        if not self.manual_mode:
            self.test_result_progress.emit(overall_result)
        self.run_status_progress.emit(overall_result)

        # # Send end operation
        # mcu_cmd = self.dev_config['cmd_end_of_operation']
        # mcu_port = self.dev_config['mcu_com_port']
        # mcu_baud_rate = self.dev_config['mcu_baud_rate']
        # mcu_start_false_cmd = self.dev_config['cmd_start_false']
        # mcu_repetition_request = self.dev_config['mcu_repetition_request_time']
        # timeout_set = self.dev_config['tmcu_end_of_operation']
        # mcu_resp_len = self.dev_config['rlmcu_end_of_operation']
        # data_pos = ["true_false"]
        #
        # _ = mcu_end_operation_cmd(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd,
        #                           mcu_repetition_request, timeout_set, mcu_resp_len, False,
        #                           data_pos)

        # Switch OFF ECU
        self.switch_off_ecu()

        # Power supply
        if USE_POWER_SUPPLY:
            self.power_supply.turn_off_output()

        # Motor send home
        # if run_stat:
        if USE_MOTOR:
            _ = self.move_motor_home()
            # motor_serial.close()

        print()

        self.finished.emit("Test finished.")

    def interupted_test_finished(self, overall_result_list):
        # Check overall result
        overall_result = self.check_overall_result(overall_result_list)
        self.test_interrupted_progress.emit(overall_result)

        self.run_status_progress.emit("PAUSED")
        self.finished.emit("Test paused.")

    def run(self):
        # Run status
        run_stat = True

        overall_result_list = []

        self.started.emit("Running test...")
        self.run_status_progress.emit("PROCESSING")

        if USE_MOTOR:
            # Open motor port
            # motor_serial = Serial(self.dev_config['motor_com_port'], int(self.dev_config['motor_baud_rate']))
            # time.sleep(float(self.dev_config['motor_start_del']))
            # if not motor_serial.isOpen():
            #     motor_serial.open()
            #     time.sleep(float(self.dev_config['motor_start_del']))

            # Motor send home
            if not self.run_from_interrupted:
                motor_respond = self.move_motor_home()
                system_log = "MOTOR HOME RESPOND", motor_respond
                logging.info(system_log)
                print(system_log)
                if motor_respond:
                    run_stat = True
                    self.initial_motor_home_respond.emit(True)
                else:
                    run_stat = False
            else:
                # Power supply
                if USE_POWER_SUPPLY:
                    self.power_supply.turn_on_output()

                if self.motor_serial_changed:
                    self.motor_home_first.emit((True, 0))
                    motor_respond = self.move_motor_home()
                    system_log = "MOTOR HOME RESPOND", motor_respond
                    logging.info(system_log)
                    print(system_log)
                    if motor_respond:
                        run_stat = True
                        self.initial_motor_home_respond.emit(True)
                    else:
                        run_stat = False
                else:
                    run_stat = True

        # Run test
        if run_stat:
            if not self.manual_mode:
                overall_result_list = []
                for idx, each_test in enumerate(self.test_to_run, 1):
                    interrupt_requested = any([self.pause_interupt, self.stop_interupt])
                    if not interrupt_requested:
                        sensor_faulty = False
                        if each_test and run_stat:
                            if not each_test == "SKIP":
                                # x_axis, x_speed, y_axis, y_speed, motor_timeout = self.check_motor_axis_speed(idx)
                                # print("TEST POINT:", idx)
                                # print("x-axis:", x_axis)
                                # print("x-speed:", x_speed)
                                # print("y-axis:", y_axis)
                                # print("y-speed:", y_speed)
                                # print("timeout:", motor_timeout)
                                # system_log = f"TEST POINT: {idx}, x-axis: {x_axis}, x-speed: {x_speed}, " \
                                #              f"y_axis: {y_axis}, y_speed: {y_speed}, timeout: {motor_timeout}"
                                # logging.info(system_log)

                                # Move motor
                                if USE_MOTOR:
                                    x_axis, x_speed, y_axis, y_speed, motor_timeout = self.check_motor_axis_speed(idx)
                                    print("TEST POINT:", idx)
                                    print("x-axis:", x_axis)
                                    print("x-speed:", x_speed)
                                    print("y-axis:", y_axis)
                                    print("y-speed:", y_speed)
                                    print("timeout:", motor_timeout)
                                    system_log = f"TEST POINT: {idx}, x-axis: {x_axis}, x-speed: {x_speed}, " \
                                                 f"y_axis: {y_axis}, y_speed: {y_speed}, timeout: {motor_timeout}"
                                    logging.info(system_log)
                                    move_motor_respond = self.move_motor_to_point(x_axis, x_speed, y_axis, y_speed,
                                                                                  motor_timeout)

                                    if not move_motor_respond:
                                        run_stat = False
                                    else:
                                        run_stat = True
                                else:
                                    run_stat = True

                                # Check sensor
                                if run_stat:
                                    if USE_IF_MCU:
                                        # Check MCU status
                                        mcu_status = self.check_mcu_status()

                                        if mcu_status:
                                            # Enable sensor
                                            if not self.run_from_interrupted:
                                                if self.sensor1_enabled:
                                                    sensor_enable_respond = True
                                                else:
                                                    # sensor_enable_respond = self.check_sensor_enable()

                                                    # Turn ON ecu
                                                    sensor_enable_respond = self.switch_on_ecu()
                                            else:
                                                sensor_enable_respond = True

                                            # Check sensor
                                            if sensor_enable_respond:
                                                """
                                                Method 1
                                                """
                                                # # Delay to get accurate reading from MCU
                                                # checking_delay = float(self.dev_config["mcu_start_del"])
                                                # time.sleep(checking_delay)
                                                #
                                                # run_stat, result_list = self.check_sensor_request(idx)
                                                # result, run_stat = self.check_sensor_result(result_list, run_stat)

                                                """
                                                Method 2
                                                """
                                                # Turn ON reverse
                                                turn_on_ok = self.switch_on_rev()

                                                if turn_on_ok:
                                                    # Delay to get accurate reading from MCU
                                                    checking_delay = float(self.dev_config["mcu_start_del"])
                                                    time.sleep(checking_delay)

                                                    run_stat, result_list = self.check_sensor_request(idx)
                                                    result, run_stat, sensor_faulty = self.check_sensor_result(
                                                        result_list, run_stat)

                                                    # Turn OFF reverse
                                                    turn_off_ok = self.switch_off_rev()

                                                    if not turn_off_ok:
                                                        run_stat = False
                                                else:
                                                    result = "NG"
                                                    run_stat = False
                                            else:
                                                result = "NG"
                                                run_stat = False
                                        else:
                                            result = "NG"
                                            run_stat = False
                                    else:
                                        result = "PASS"
                                else:
                                    result = "ERROR"

                                time.sleep(0.5)
                            else:
                                result = "SKIP"
                        elif each_test and not run_stat:
                            result = "ERROR"
                        elif not each_test and run_stat:
                            result = "NO TEST"
                        else:
                            result = "NO TEST"

                        overall_result_list.append(result)
                        test_point_result = {"index": idx, "result": result}

                        # Calculate current coverage percentage
                        total_detected_coverage_dict = self.calculate_detected_coverage(result)

                        # Emit signal
                        if self.horizontal_mode:
                            self.horizontal_test_point_progress.emit(test_point_result)
                        if self.vertical_mode:
                            self.vertical_test_point_progress.emit(test_point_result)
                        self.total_test_point_detected_progress.emit(total_detected_coverage_dict)
                        self.sensor_faulty_signal.emit(sensor_faulty)
                    else:
                        break
            else:
                x_axis, x_speed, y_axis, y_speed, motor_timeout = self.check_motor_input_value()

                print("x-axis:", x_axis)
                print("x-speed:", x_speed)
                print("y-axis:", y_axis)
                print("y-speed:", y_speed)
                print("timeout:", motor_timeout)
                system_log = f"x-axis: {x_axis}, x-speed: {x_speed}, " \
                             f"y_axis: {y_axis}, y_speed: {y_speed}, timeout: {motor_timeout}"
                logging.info(system_log)
                if USE_MOTOR:
                    move_motor_respond = self.move_motor_to_point(x_axis, x_speed, y_axis, y_speed, motor_timeout)
                else:
                    move_motor_respond = True

                if move_motor_respond:
                    # Check sensor
                    if USE_IF_MCU:
                        # Check MCU status
                        mcu_status = self.check_mcu_status()
                        system_log = "MCU STATUS RESPOND", mcu_status
                        logging.info(system_log)
                        print(system_log)
                        mcu_status = True

                        if mcu_status:
                            # Enable sensor
                            if self.sensor1_enabled:
                                sensor_enable_respond = True
                            else:
                                sensor_enable_respond = self.check_sensor_enable()

                            system_log = "SENSOR ENABLE RESPOND", sensor_enable_respond
                            logging.info(system_log)
                            print(system_log)
                            if sensor_enable_respond:
                                run_stat, result_list = self.check_sensor_request(0)
                                result, run_stat, sensor_faulty = self.check_sensor_result(result_list, run_stat)
                                system_log = "CHECK SENSOR RESPOND", result_list
                                logging.info(system_log)
                                print(system_log)
                            else:
                                result = "NG"
                        else:
                            result = "NG"
                    else:
                        result = "PASS"
                    overall_result_list.append(result)
                else:
                    result = "NG"
                    overall_result_list.append(result)
        else:
            result = "NG"
            overall_result_list.append(result)

        # Finished the test worker depend on interupted flag
        if self.pause_interupt and not self.stop_interupt:
            self.interupted_test_finished(overall_result_list)
        elif not self.pause_interupt and self.stop_interupt:
            self.normal_test_finished(overall_result_list)
        else:
            self.normal_test_finished(overall_result_list)


class MoveMotorWorker(QtCore.QObject):
    started = pyqtSignal(str)
    motor_respond_progress = pyqtSignal(bool)
    motor_home_first = pyqtSignal(tuple)
    initial_motor_home_respond = pyqtSignal(bool)
    finished = pyqtSignal(bool)

    def __init__(self, x_spinbox=None, y_spinbox=None, device_config=None, motor_serial=None,
                 motor_serial_changed=None, initial_move_motor_home=None):
        super().__init__()
        self.x_spinbox = x_spinbox
        self.y_spinbox = y_spinbox
        self.dev_config = device_config
        self.motor_serial = motor_serial
        self.motor_serial_changed = motor_serial_changed
        self.initial_move_motor_home = initial_move_motor_home

        # Init motor offset
        self.motor_offset = 1400

    def check_motor_input_value(self):
        x_speed = int(self.dev_config["zone3_speedx"])
        y_speed = int(self.dev_config["zone3_speedy"])
        x_axis = self.motor_offset - self.x_spinbox
        y_axis = self.y_spinbox
        motor_timeout = int(self.dev_config["zone3_timeout"])

        return x_axis, x_speed, y_axis, y_speed, motor_timeout

    def move_motor_home(self):
        # Send motor Home
        x_speed, x_dist, y_speed, y_dist = 0, 0, 0, 0
        motor_req_byte = RequestByte(command_val='H', char_cond=True,
                                     data_val_list=[x_speed, x_dist, y_speed, y_dist],
                                     data_zfill_list=[3, 4, 3, 4]).out_bytearray()

        system_log = f"SEND HOME REQUEST: {motor_req_byte.hex()}"
        logging.info(system_log)
        print(system_log)

        # Open Com port for Motor Driver
        try:
            if not self.motor_serial.isOpen():
                self.motor_serial.open()
                time.sleep(float(self.dev_config['motor_start_del']))

            self.motor_serial.timeout = float(self.dev_config['home_timeout'])
            self.motor_serial.write(motor_req_byte)
            self.motor_serial.reset_input_buffer()
            motor_response = self.motor_serial.read(5)
            system_log = f"MOTOR HOME RESPOND: {motor_response.hex()}"
            logging.info(system_log)
            print(system_log)

            # Start OK
            if motor_response == b'[T\x00T]':
                # motor_ser.timeout = float(self.dev_config['motor_end_home_del'])
                motor_response = self.motor_serial.read(5)
                # Confirm End
                if motor_response == b'[E\x00E]':
                    system_log = "CONFIRM END SIGNAL RECEIVED: OK"
                    logging.info(system_log)
                    print(system_log)
                    motor_respond = True
                    pass
                # Waiting for confirm end timed out
                else:
                    system_log = "CONFIRM END SIGNAL NOT RECEIVED"
                    logging.info(system_log)
                    print(system_log)
                    motor_respond = False
            # Start NOK
            elif motor_response == b'[F\x00F]':
                system_log = "CONFIRM END SIGNAL RECEIVED: NOK"
                logging.info(system_log)
                print(system_log)
                motor_respond = False
            # No response
            else:
                system_log = "CONFIRM START SIGNAL NOT RECEIVED"
                logging.info(system_log)
                print(system_log)
                motor_respond = False
        except SerialException:
            motor_respond = False

        return motor_respond

    def move_motor_to_point(self, x_axis, x_speed, y_axis, y_speed, motor_timeout):

        # Motor request
        motor_req_byte = RequestByte(command_val='M', char_cond=True, data_val_list=[x_speed, x_axis, y_speed, y_axis],
                                     data_zfill_list=[3, 4, 3, 4]).out_bytearray()

        system_log = f"MOVE MOTOR REQUEST: {motor_req_byte.hex()}"
        logging.info(system_log)
        print(system_log)

        # Open Com port for Motor Driver
        try:
            if not self.motor_serial.isOpen():
                self.motor_serial.open()
                time.sleep(float(self.dev_config['motor_start_del']))

            self.motor_serial.timeout = int(motor_timeout)
            self.motor_serial.write(motor_req_byte)
            self.motor_serial.reset_input_buffer()
            serial_string = self.motor_serial.read(5)
            serial_string_hex = serial_string.hex()
            system_log = f"MOVE MOTOR RESPONSE: {serial_string_hex}"
            logging.info(system_log)
            print(system_log)

            if serial_string == b'[T\x00T]':
                system_log = "CONFIRM START SIGNAL: OK"
                logging.info(system_log)
                print(system_log)
                motor_response = self.motor_serial.read(5)
                # Confirm End
                if motor_response == b'[E\x00E]':
                    system_log = f"CONFIRM END SIGNAL RECEIVED: {motor_response.hex()}"
                    logging.info(system_log)
                    print(system_log)
                    motor_respond = True
                # Waiting for confirm end timed out
                else:
                    system_log = f"CONFIRM END SIGNAL NOT RECEIVED: {motor_response.hex()}"
                    logging.info(system_log)
                    print(system_log)
                    motor_respond = False
            elif serial_string == b'[F\x00F]':
                system_log = "CONFIRM START SIGNAL: NOK"
                logging.info(system_log)
                print(system_log)
                motor_respond = False
            else:
                system_log = f"CONFIRM START SIGNAL NOT RECEIVED: {serial_string.hex()}"
                logging.info(system_log)
                print(system_log)
                motor_respond = False
        except SerialException:
            motor_respond = False

        return motor_respond

    def run(self):
        # If serial changed (port connect/disconnect), go home first
        if USE_MOTOR:
            if self.motor_serial_changed or not self.initial_move_motor_home:
                if self.motor_serial_changed:
                    self.motor_home_first.emit((True, 0))

                if not self.initial_move_motor_home:
                    self.motor_home_first.emit((True, 1))

                motor_home_respond = self.move_motor_home()
            else:
                motor_home_respond = True
        else:
            motor_home_respond = True

        # Check x-coordinate, y-coordinate to matching with motor input
        x_axis, x_speed, y_axis, y_speed, motor_timeout = self.check_motor_input_value()
        print("x-axis:", x_axis)
        print("x-speed:", x_speed)
        print("y-axis:", y_axis)
        print("y-speed:", y_speed)
        print("timeout:", motor_timeout)
        system_log = f"x-axis: {x_axis}, x-speed: {x_speed}, " \
                     f"y_axis: {y_axis}, y_speed: {y_speed}, timeout: {motor_timeout}"
        logging.info(system_log)
        if motor_home_respond:
            self.initial_motor_home_respond.emit(True)
            if USE_MOTOR:
                # Open motor port
                # motor_serial = Serial(self.dev_config['motor_com_port'], int(self.dev_config['motor_baud_rate']))
                # time.sleep(float(self.dev_config['motor_start_del']))
                # if not motor_serial.isOpen():
                #     motor_serial.open()
                #     time.sleep(float(self.dev_config['motor_start_del']))

                move_motor_respond = self.move_motor_to_point(x_axis, x_speed, y_axis, y_speed, motor_timeout)
            else:
                move_motor_respond = True
        else:
            move_motor_respond = False

        if move_motor_respond:
            self.motor_respond_progress.emit(True)
        else:
            self.motor_respond_progress.emit(False)

        # if USE_MOTOR:
        #     motor_serial.close()

        self.finished.emit(move_motor_respond)


class MotorHomeWorker(QtCore.QObject):
    started = pyqtSignal(str)
    motor_respond_progress = pyqtSignal(bool)
    initial_motor_home_respond = pyqtSignal(bool)
    finished = pyqtSignal(bool)

    def __init__(self, device_config=None, motor_serial=None):
        super().__init__()
        self.dev_config = device_config
        self.motor_serial = motor_serial

    def move_motor_home(self):
        # Send motor Home
        x_speed, x_dist, y_speed, y_dist = 0, 0, 0, 0
        motor_req_byte = RequestByte(command_val='H', char_cond=True,
                                     data_val_list=[x_speed, x_dist, y_speed, y_dist],
                                     data_zfill_list=[3, 4, 3, 4]).out_bytearray()

        # Open Com port for Motor Driver
        try:
            if not self.motor_serial.isOpen():
                self.motor_serial.open()
                time.sleep(float(self.dev_config['motor_start_del']))

            system_log = "SEND HOME REQUEST:", motor_req_byte.hex()
            logging.info(system_log)
            print(system_log)
            self.motor_serial.timeout = float(self.dev_config['home_timeout'])
            self.motor_serial.write(motor_req_byte)
            self.motor_serial.reset_input_buffer()
            motor_response = self.motor_serial.read(5)
            system_log = "MOTOR HOME RESPOND:", motor_response.hex()
            logging.info(system_log)
            print(system_log)

            # Start OK
            if motor_response == b'[T\x00T]':
                # motor_ser.timeout = float(self.dev_config['motor_end_home_del'])
                motor_response = self.motor_serial.read(5)
                # Confirm End
                if motor_response == b'[E\x00E]':
                    system_log = "CONFIRM END SIGNAL RECEIVED: OK"
                    logging.info(system_log)
                    print(system_log)
                    motor_respond = True
                    pass
                # Waiting for confirm end timed out
                else:
                    system_log = "CONFIRM END SIGNAL NOT RECEIVED"
                    logging.info(system_log)
                    print(system_log)
                    motor_respond = False
            # Start NOK
            elif motor_response == b'[F\x00F]':
                system_log = "CONFIRM END SIGNAL RECEIVED: NOK"
                logging.info(system_log)
                print(system_log)
                motor_respond = False
            # No response
            else:
                system_log = "CONFIRM START SIGNAL NOT RECEIVED"
                logging.info(system_log)
                print(system_log)
                motor_respond = False
        except SerialException:
            motor_respond = False

        return motor_respond

    def run(self):
        # Move motor home
        if USE_MOTOR:
            # Open motor port
            # motor_serial = Serial(self.dev_config['motor_com_port'], int(self.dev_config['motor_baud_rate']))
            # time.sleep(float(self.dev_config['motor_start_del']))
            # if not motor_serial.isOpen():
            #     motor_serial.open()
            #     time.sleep(float(self.dev_config['motor_start_del']))

            motor_respond = self.move_motor_home()
        else:
            motor_respond = True

        if motor_respond:
            self.initial_motor_home_respond.emit(True)
            self.motor_respond_progress.emit(True)
        else:
            self.motor_respond_progress.emit(False)

        # if USE_MOTOR:
        #     motor_serial.close()

        self.finished.emit(motor_respond)


class SpecialStyledItemDelegate(QtWidgets.QStyledItemDelegate):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._row_values = dict()
        self._col_values = dict()

    def add_text(self, text, row, col):
        self._row_values[row] = text
        self._col_values[col] = text

    def initStyleOption(self, option, index):
        super().initStyleOption(option, index)
        row = index.row()
        col = index.column()

        if row in self._row_values and col in self._col_values:
            option.text = self._col_values[col]
            option.displayAlignment = QtCore.Qt.AlignCenter


class WrapHeader(QtWidgets.QHeaderView):
    def sectionSizeFromContents(self, logical_index):
        # get the size returned by the default implementation
        size = super().sectionSizeFromContents(logical_index)
        if self.model():
            if size.width() > self.sectionSize(logical_index):
                text = self.model().headerData(logical_index, self.orientation(), QtCore.Qt.DisplayRole)
                if not text:
                    return size
                # in case the display role is numeric (for example, when header
                # labels are not defined yet), convert it to a string;
                text = str(text)

                option = QtWidgets.QStyleOptionHeader()
                self.initStyleOption(option)
                alignment = self.model().headerData(logical_index, self.orientation(), QtCore.Qt.TextAlignmentRole)
                if alignment is None:
                    alignment = option.textAlignment

                # get the default style margin for header text and create a
                # possible rectangle using the current section size, then use
                # QFontMetrics to get the required rectangle for the wrapped text
                margin = self.style().pixelMetric(
                    QtWidgets.QStyle.PM_HeaderMargin, option, self)
                max_width = self.sectionSize(logical_index) - margin * 2
                rect = option.fontMetrics.boundingRect(
                    QtCore.QRect(0, 0, max_width, 10000),
                    alignment | QtCore.Qt.TextWordWrap,
                    text)

                # add vertical margins to the resulting height
                height = rect.height() + margin * 2
                if height >= size.height():
                    # if the height is bigger than the one provided by the base
                    # implementation, return a new size based on the text rect
                    return QtCore.QSize(rect.width(), height)
        return size


class PowerSwitch(QtWidgets.QPushButton):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setCheckable(True)
        self.setMinimumWidth(66)
        self.setMinimumHeight(22)

    def paintEvent(self, event):
        label = "ON" if self.isChecked() else "OFF"
        if self.isEnabled():
            bg_color = QtGui.QColor(0, 255, 0) if self.isChecked() else QtGui.QColor(255, 0, 0)
        else:
            bg_color = QtGui.QColor(169, 169, 169)

        radius = 10
        width = 32
        center = self.rect().center()

        painter = QtGui.QPainter(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.translate(center)
        painter.setBrush(QtGui.QColor(0, 0, 0))

        pen = QtGui.QPen(Qt.black)
        pen.setWidth(2)
        painter.setPen(pen)

        painter.drawRoundedRect(QRect(-width, -radius, 2*width, 2*radius), radius, radius)
        painter.setBrush(QtGui.QBrush(bg_color))
        sw_rect = QRect(-radius, -radius, width + radius, 2*radius)
        if not self.isChecked():
            sw_rect.moveLeft(-width)
        painter.drawRoundedRect(sw_rect, radius, radius)
        painter.drawText(sw_rect, Qt.AlignCenter, label)


class SettingsWindow(QtWidgets.QWidget):
    def __init__(self, vehicle_dataframe, standard_dataframe, main_window):
        super().__init__()

        # Init GUI window
        self.ui = Ui_settings_window()
        self.ui.setupUi(self)
        self.main_window = main_window

        # Init edit vehicle window
        self.edit_vehicle_window = EditVehicleWindow(self.main_window, self)

        # Init edit standard window
        self.edit_standard_window = EditStandardWindow(self.main_window, self)

        # Init variables
        self.vehicle_dataframe = vehicle_dataframe
        self.vehicle_header_column = ["Vehicle Family", "Vehicle ID", "Vehicle Width", "Width Sensor A",
                                      "Width Sensor B", "Width Sensor C", "Width Sensor D", "Width Sensor Z",
                                      "Height Sensor 1", "Height Sensor 2", "Height Sensor 3", "Height Sensor 4",
                                      "Distance Rear Sensor 1", "Distance Rear Sensor 2", "Distance Rear Sensor 3",
                                      "Distance Rear Sensor 4"]
        self.selected_vehicle_data_dict = {}
        self.standard_dataframe = standard_dataframe
        self.standard_header_column = ["Standard name", "Minimum Horizontal Coverage", "Minimum Vertical Coverage"]
        self.selected_standard_data_dict = {}

        # Init img path
        self.add_icon_path = "img/icon/add_icon.png"
        self.edit_icon_path = "img/icon/edit2_icon.png"
        self.delete_icon_path = "img/icon/delete_icon.png"

        # Init tableview
        model = PandasModel(self.vehicle_dataframe, self.vehicle_header_column)
        self.ui.vehicle_tabv.setModel(model)
        self.vehicle_selectionModel = self.ui.vehicle_tabv.selectionModel()
        self.vehicle_selectionModel.selectionChanged.connect(self.on_vehicle_table_selected)
        model = PandasModel(self.standard_dataframe, self.standard_header_column)
        self.ui.standard_tabv.setModel(model)
        self.standard_selectionModel = self.ui.standard_tabv.selectionModel()
        self.standard_selectionModel.selectionChanged.connect(self.on_standard_table_selected)

        # Connect cancel button
        self.ui.cancel_btn.clicked.connect(self.cancel_clicked)

        # Connect search button
        self.ui.vehicle_search_btn.clicked.connect(self.vehicle_search)
        self.ui.standard_search_btn.clicked.connect(self.standard_search)

        # Connect tableview clicked
        self.ui.vehicle_tabv.clicked.connect(self.get_vehicle_selected_data)
        self.ui.standard_tabv.clicked.connect(self.get_standard_selected_data)

        # Connect add,, edit, delete button
        self.ui.add_vehicle_btn.clicked.connect(self.add_vehicle_button_clicked)
        self.ui.edit_vehicle_btn.clicked.connect(self.edit_vehicle_button_clicked)
        self.ui.delete_vehicle_btn.clicked.connect(self.delete_vehicle_button_clicked)
        self.ui.add_standard_btn.clicked.connect(self.add_standard_button_clicked)
        self.ui.edit_standard_btn.clicked.connect(self.edit_standard_button_clicked)
        self.ui.delete_standard_btn.clicked.connect(self.delete_standard_button_clicked)

        # Config ui
        self.config_ui()

        # If vehicle family combobox changed
        self.ui.vehicle_family_cbox.currentIndexChanged.connect(self.on_vehicle_family_changed)

    def get_vehicle_selected_data(self):
        # Get selected parameter
        index = (self.ui.vehicle_tabv.selectionModel().currentIndex())
        selected_vehicle_family = index.siblingAtColumn(0).data()
        selected_vehicle_id = index.siblingAtColumn(1).data()
        selected_vehicle_width = float(index.siblingAtColumn(2).data())
        selected_vehicle_width_sensor_a = float(index.siblingAtColumn(3).data())
        selected_vehicle_width_sensor_b = float(index.siblingAtColumn(4).data())
        selected_vehicle_width_sensor_c = float(index.siblingAtColumn(5).data())
        selected_vehicle_width_sensor_d = float(index.siblingAtColumn(6).data())
        selected_vehicle_width_sensor_z = float(index.siblingAtColumn(7).data())
        selected_vehicle_height_of_sensor1 = float(index.siblingAtColumn(8).data())
        selected_vehicle_height_of_sensor2 = float(index.siblingAtColumn(9).data())
        selected_vehicle_height_of_sensor3 = float(index.siblingAtColumn(10).data())
        selected_vehicle_height_of_sensor4 = float(index.siblingAtColumn(11).data())
        selected_vehicle_distance_rear_face_sensor1 = float(index.siblingAtColumn(12).data())
        selected_vehicle_distance_rear_face_sensor2 = float(index.siblingAtColumn(13).data())
        selected_vehicle_distance_rear_face_sensor3 = float(index.siblingAtColumn(14).data())
        selected_vehicle_distance_rear_face_sensor4 = float(index.siblingAtColumn(15).data())

        # Store in dict
        self.selected_vehicle_data_dict = {"vehicle family": selected_vehicle_family,
                                           "vehicle id": selected_vehicle_id,
                                           "vehicle width": selected_vehicle_width,
                                           "vehicle width sensor a": selected_vehicle_width_sensor_a,
                                           "vehicle width sensor b": selected_vehicle_width_sensor_b,
                                           "vehicle width sensor c": selected_vehicle_width_sensor_c,
                                           "vehicle width sensor d": selected_vehicle_width_sensor_d,
                                           "vehicle width sensor z": selected_vehicle_width_sensor_z,
                                           "vehicle height sensor1": selected_vehicle_height_of_sensor1,
                                           "vehicle height sensor2": selected_vehicle_height_of_sensor2,
                                           "vehicle height sensor3": selected_vehicle_height_of_sensor3,
                                           "vehicle height sensor4": selected_vehicle_height_of_sensor4,
                                           "vehicle distance rear sensor1": selected_vehicle_distance_rear_face_sensor1,
                                           "vehicle distance rear sensor2": selected_vehicle_distance_rear_face_sensor2,
                                           "vehicle distance rear sensor3": selected_vehicle_distance_rear_face_sensor3,
                                           "vehicle distance rear sensor4": selected_vehicle_distance_rear_face_sensor4}

    def get_standard_selected_data(self):
        # Get selected parameter
        index = (self.ui.standard_tabv.selectionModel().currentIndex())
        selected_standard_name = index.siblingAtColumn(0).data()
        selected_standard_minimum_horizontal = float(index.siblingAtColumn(1).data())
        selected_standard_minimum_vertical = float(index.siblingAtColumn(2).data())

        # Store in dict
        self.selected_standard_data_dict = {"standard name": selected_standard_name,
                                            "minimum horizontal": selected_standard_minimum_horizontal,
                                            "minimum vertical": selected_standard_minimum_vertical}

    def on_vehicle_family_changed(self):
        # Copy dataframe
        vehicle_df = self.vehicle_dataframe.copy()

        if not vehicle_df.empty:
            # Get current vehicle family
            vehicle_family = self.ui.vehicle_family_cbox.currentText()
            if vehicle_family == "--Include All--":
                vehicle_id_unique_value = ["--Include All--"]
            else:
                vehicle_family = [vehicle_family]

                # Filter based on parameter
                vehicle_family_id_filtered = VehicleHandler(vehicle_df=vehicle_df,
                                                            vehicle_family=vehicle_family).filter_result()
                vehicle_id_value = vehicle_family_id_filtered[["vehicle_id"]].values.ravel()
                vehicle_id_unique_value = list(pd.unique(vehicle_id_value))
                vehicle_id_unique_value = [str(i) for i in vehicle_id_unique_value]
                vehicle_id_unique_value.insert(0, "--Include All--")

            # Fill up cbox
            self.ui.vehicle_id_cbox.clear()
            self.ui.vehicle_id_cbox.addItems(vehicle_id_unique_value)

    def on_vehicle_table_selected(self):
        self.ui.edit_vehicle_btn.setEnabled(True)
        self.ui.delete_vehicle_btn.setEnabled(True)

    def on_standard_table_selected(self):
        self.ui.edit_standard_btn.setEnabled(True)
        self.ui.delete_standard_btn.setEnabled(True)

    def show_window(self):
        # Copy dataframe
        vehicle_df = self.vehicle_dataframe.copy()
        standard_df = self.standard_dataframe.copy()

        if not vehicle_df.empty:
            # Fill up combo box
            vehicle_family_value = vehicle_df[["vehicle_family"]].values.ravel()
            vehicle_family_unique_value = list(pd.unique(vehicle_family_value))
            vehicle_family_unique_value.insert(0, "--Include All--")
            self.ui.vehicle_family_cbox.clear()
            self.ui.vehicle_family_cbox.addItems(vehicle_family_unique_value)

            # Get filter parameter
            vehicle_family = self.ui.vehicle_family_cbox.currentText()
            vehicle_id = self.ui.vehicle_id_cbox.currentText()
            if vehicle_family == "--Include All--":
                vehicle_family_unique_value.pop(0)
                vehicle_family = vehicle_family_unique_value
            else:
                vehicle_family = [vehicle_family]
            if vehicle_id == "":
                pass
            elif vehicle_id == "--Include All--":
                vehicle_id = False
            else:
                vehicle_id = [vehicle_id]

            # Filter based on parameter
            vehc_df = VehicleHandler(vehicle_df=vehicle_df, vehicle_family=vehicle_family,
                                     vehicle_id=vehicle_id).filter_result()

            # View in tableview
            vehc_model = PandasModel(vehc_df, self.vehicle_header_column)
            self.ui.vehicle_tabv.setModel(vehc_model)
            self.vehicle_selectionModel = self.ui.vehicle_tabv.selectionModel()
            self.vehicle_selectionModel.selectionChanged.connect(self.on_vehicle_table_selected)

        if not standard_df.empty:
            standard_name_value = standard_df[["standard_name"]].values.ravel()
            standard_name_unique_value = list(pd.unique(standard_name_value))
            standard_name_unique_value.insert(0, "--Include All--")
            self.ui.standard_name_cbox.clear()
            self.ui.standard_name_cbox.addItems(standard_name_unique_value)

            # Get filter parameter
            standard_name = self.ui.standard_name_cbox.currentText()
            if standard_name == "--Include All--":
                standard_name_unique_value.pop(0)
                standard_name = standard_name_unique_value
            else:
                standard_name = [standard_name]

            # Filter based on parameter
            std_df = StandardHandler(standard_df=standard_df, standard_name=standard_name).filter_result()

            # View in tableview
            std_model = PandasModel(std_df, self.standard_header_column)
            self.ui.standard_tabv.setModel(std_model)
            self.standard_selectionModel = self.ui.standard_tabv.selectionModel()
            self.standard_selectionModel.selectionChanged.connect(self.on_standard_table_selected)

        # Disable button
        self.ui.edit_vehicle_btn.setDisabled(True)
        self.ui.delete_vehicle_btn.setDisabled(True)
        self.ui.edit_standard_btn.setDisabled(True)
        self.ui.delete_standard_btn.setDisabled(True)

        # Set startup tab
        self.ui.settings_tabWidget.setCurrentIndex(0)

        # Show window
        self.show()

    def vehicle_search(self):
        # Copy dataframe
        vehicle_df = self.vehicle_dataframe.copy()

        if not vehicle_df.empty:
            # Get filter parameter
            vehicle_family = self.ui.vehicle_family_cbox.currentText()
            vehicle_id = self.ui.vehicle_id_cbox.currentText()
            if vehicle_family == "--Include All--":
                vehicle_family_value = vehicle_df[["vehicle_family"]].values.ravel()
                vehicle_family_unique_value = list(pd.unique(vehicle_family_value))
                vehicle_family = vehicle_family_unique_value
            else:
                vehicle_family = [vehicle_family]
            if vehicle_id == "":
                pass
            elif vehicle_id == "--Include All--":
                vehicle_id = False
            else:
                vehicle_id = [vehicle_id]

            # Filter based on parameter
            df = VehicleHandler(vehicle_df=vehicle_df,
                                vehicle_family=vehicle_family,
                                vehicle_id=vehicle_id).filter_result()

            # View in tableview
            model = PandasModel(df, self.vehicle_header_column)
            self.ui.vehicle_tabv.setModel(model)
            self.vehicle_selectionModel = self.ui.vehicle_tabv.selectionModel()
            self.vehicle_selectionModel.selectionChanged.connect(self.on_vehicle_table_selected)

        # Disabled button
        self.ui.edit_vehicle_btn.setDisabled(True)
        self.ui.delete_vehicle_btn.setDisabled(True)

    def standard_search(self):
        # Copy dataframe
        standard_df = self.standard_dataframe.copy()

        if not standard_df.empty:
            # Get filter parameter
            standard_name = self.ui.standard_name_cbox.currentText()
            if standard_name == "--Include All--":
                standard_name_value = standard_df[["standard_name"]].values.ravel()
                standard_name_unique_value = list(pd.unique(standard_name_value))
                standard_name = standard_name_unique_value
            else:
                standard_name = [standard_name]

            # Filter based on parameter
            df = StandardHandler(standard_df=standard_df, standard_name=standard_name).filter_result()

            # View in tableview
            model = PandasModel(df, self.standard_header_column)
            self.ui.standard_tabv.setModel(model)
            self.standard_selectionModel = self.ui.standard_tabv.selectionModel()
            self.standard_selectionModel.selectionChanged.connect(self.on_standard_table_selected)

        # Disabled button
        self.ui.edit_standard_btn.setDisabled(True)
        self.ui.delete_standard_btn.setDisabled(True)

    def add_vehicle_button_clicked(self):
        self.edit_vehicle_window.setWindowTitle("Add vehicle")
        self.edit_vehicle_window.save_button_status = "add"
        self.edit_vehicle_window.ui.vehicle_family_ledit.setEnabled(True)
        self.edit_vehicle_window.ui.vehicle_id_ledit.setEnabled(True)
        self.edit_vehicle_window.show_window()

    def edit_vehicle_button_clicked(self):
        self.edit_vehicle_window.setWindowTitle("Edit vehicle")
        self.edit_vehicle_window.save_button_status = "edit"
        self.edit_vehicle_window.ui.vehicle_family_ledit.setDisabled(True)
        self.edit_vehicle_window.ui.vehicle_id_ledit.setDisabled(True)
        self.edit_vehicle_window.show_window(show_selected_data=self.selected_vehicle_data_dict)

    def delete_vehicle_button_clicked(self):
        message = "Are you sure want to delete?"
        button_clicked = self.show_dialog_popup(message)
        if button_clicked == QMessageBox.Yes:
            selected_config_file_name = "vehc_" + self.selected_vehicle_data_dict["vehicle family"] + "_id" + \
                                        str(self.selected_vehicle_data_dict["vehicle id"])
            try:
                # Delete file config
                if os.path.exists(VEHICLE_CONFIG_PATH + "/" + selected_config_file_name + ".ini"):
                    os.remove(VEHICLE_CONFIG_PATH + "/" + selected_config_file_name + ".ini")
                else:
                    pass

                # Delete in dataframe
                df_copy = self.vehicle_dataframe.copy()
                df_copy = df_copy.drop(df_copy[(df_copy["vehicle_family"] ==
                                                self.selected_vehicle_data_dict["vehicle family"]) &
                                               (df_copy["vehicle_id"] ==
                                                self.selected_vehicle_data_dict["vehicle id"]) &
                                               (df_copy["vehicle_width"] ==
                                                self.selected_vehicle_data_dict["vehicle width"]) &
                                               (df_copy["width_between_sensor_point_a"] ==
                                                self.selected_vehicle_data_dict["vehicle width sensor a"]) &
                                               (df_copy["width_between_sensor_point_b"] ==
                                                self.selected_vehicle_data_dict["vehicle width sensor b"]) &
                                               (df_copy["width_between_sensor_point_c"] ==
                                                self.selected_vehicle_data_dict["vehicle width sensor c"]) &
                                               (df_copy["width_between_sensor_point_d"] ==
                                                self.selected_vehicle_data_dict["vehicle width sensor d"]) &
                                               (df_copy["width_between_sensor_point_z"] ==
                                                self.selected_vehicle_data_dict["vehicle width sensor z"]) &
                                               (df_copy["height_of_sensor1"] ==
                                                self.selected_vehicle_data_dict["vehicle height sensor1"]) &
                                               (df_copy["height_of_sensor2"] ==
                                                self.selected_vehicle_data_dict["vehicle height sensor2"]) &
                                               (df_copy["height_of_sensor3"] ==
                                                self.selected_vehicle_data_dict["vehicle height sensor3"]) &
                                               (df_copy["height_of_sensor4"] ==
                                                self.selected_vehicle_data_dict["vehicle height sensor4"]) &
                                               (df_copy["distance_rear_face_sensor1"] ==
                                                self.selected_vehicle_data_dict["vehicle distance rear sensor1"]) &
                                               (df_copy["distance_rear_face_sensor2"] ==
                                                self.selected_vehicle_data_dict["vehicle distance rear sensor2"]) &
                                               (df_copy["distance_rear_face_sensor3"] ==
                                                self.selected_vehicle_data_dict["vehicle distance rear sensor3"]) &
                                               (df_copy["distance_rear_face_sensor4"] ==
                                                self.selected_vehicle_data_dict["vehicle distance rear sensor4"])]
                                       .index, inplace=False)

                # Update dataframe in main window
                self.main_window.vehicle_dataframe = df_copy

                # Update dataframe in settings window
                self.vehicle_dataframe = df_copy

                # Update combobox
                self.update_vehicle_combo_box(show_vehicle_family='all', show_vehicle_id='all')

                # Update main window combo box
                self.main_window.update_vehicle_combobox()

                system_log = f"{selected_config_file_name}.ini deleted successfully"
                logging.info(system_log)

                self.show_info_popup("Deleted successfully!")
            except IOError:
                system_log = f"{selected_config_file_name}.ini fail to delete"
                logging.error(system_log)

                self.show_alert_popup("Fail to delete!")
        else:
            pass

    def add_standard_button_clicked(self):
        self.edit_standard_window.setWindowTitle("Add standard")
        self.edit_standard_window.save_button_status = "add"
        self.edit_standard_window.ui.standard_name_ledit.setEnabled(True)
        self.edit_standard_window.show_window()

    def edit_standard_button_clicked(self):
        self.edit_standard_window.setWindowTitle("Edit standard")
        self.edit_standard_window.save_button_status = "edit"
        self.edit_standard_window.ui.standard_name_ledit.setDisabled(True)
        self.edit_standard_window.show_window(show_selected_data=self.selected_standard_data_dict)

    def delete_standard_button_clicked(self):
        message = "Are you sure want to delete?"
        button_clicked = self.show_dialog_popup(message)

        if button_clicked == QMessageBox.Yes:
            selected_config_file_name = "std_" + self.selected_standard_data_dict["standard name"]

            try:
                # Delete file config
                if os.path.exists(STANDARD_CONFIG_FILE_PATH + "/" + selected_config_file_name + ".ini"):
                    os.remove(STANDARD_CONFIG_FILE_PATH + "/" + selected_config_file_name + ".ini")
                else:
                    pass

                # Delete in dataframe
                df_copy = self.standard_dataframe.copy()
                df_copy = df_copy.drop(df_copy[(df_copy["standard_name"] ==
                                                self.selected_standard_data_dict["standard name"])].
                                       index, inplace=False)

                # Update dataframe in main window
                self.main_window.standard_dataframe = df_copy

                # Update dataframe in settings window
                self.standard_dataframe = df_copy

                # Update combobox
                self.update_standard_combo_box(show_standard_name='all')

                # Update main window combo box
                self.main_window.update_standard_combobox()

                system_log = f"{selected_config_file_name}.ini deleted successfully"
                logging.info(system_log)

                self.show_info_popup("Deleted successfully!")
            except IOError:
                system_log = f"{selected_config_file_name}.ini fail to delete"
                logging.error(system_log)

                self.show_alert_popup("Fail to delete!")
        else:
            pass

    def cancel_clicked(self):
        self.ui.vehicle_family_cbox.clear()
        self.ui.vehicle_id_cbox.clear()
        self.close()

    def update_vehicle_combo_box(self, show_vehicle_family=None, show_vehicle_id=None):
        # Copy dataframe
        vehicle_df = self.vehicle_dataframe.copy()

        if not vehicle_df.empty:
            # Fill up combo box
            vehicle_family_value = vehicle_df[["vehicle_family"]].values.ravel()
            vehicle_family_unique_value = list(pd.unique(vehicle_family_value))
            vehicle_family_unique_value.insert(0, "--Include All--")
            self.ui.vehicle_family_cbox.clear()
            self.ui.vehicle_family_cbox.addItems(vehicle_family_unique_value)

            if show_vehicle_id is not None and show_vehicle_id is not None:
                if show_vehicle_family == "all" and show_vehicle_id == "all":
                    set_current_family = "--Include All--"
                    set_current_id = "--Include All--"

                    # Filter based on parameter
                    df = VehicleHandler(vehicle_df=vehicle_df,
                                        vehicle_family=vehicle_family_unique_value).filter_result()
                else:
                    set_current_family = show_vehicle_family
                    set_current_id = str(show_vehicle_id)

                    # Filter based on parameter
                    df = VehicleHandler(vehicle_df=vehicle_df,
                                        vehicle_family=[show_vehicle_family],
                                        vehicle_id=[show_vehicle_id]).filter_result()

                self.ui.vehicle_family_cbox.setCurrentText(set_current_family)
                self.ui.vehicle_id_cbox.setCurrentText(set_current_id)

                # View in tableview
                model = PandasModel(df, self.vehicle_header_column)
                self.ui.vehicle_tabv.setModel(model)
                self.vehicle_selectionModel = self.ui.vehicle_tabv.selectionModel()
                self.vehicle_selectionModel.selectionChanged.connect(self.on_vehicle_table_selected)
        else:
            # Update combobox
            self.ui.vehicle_family_cbox.clear()
            self.ui.vehicle_id_cbox.clear()

            # View in tableview
            df = pd.DataFrame()
            model = PandasModel(df, self.vehicle_header_column)
            self.ui.vehicle_tabv.setModel(model)
            self.vehicle_selectionModel = self.ui.vehicle_tabv.selectionModel()
            self.vehicle_selectionModel.selectionChanged.connect(self.on_vehicle_table_selected)

            # Disable button
            self.ui.edit_vehicle_btn.setDisabled(True)
            self.ui.delete_vehicle_btn.setDisabled(True)

    def update_standard_combo_box(self, show_standard_name=None):
        # Copy dataframe
        standard_df = self.standard_dataframe.copy()

        if not standard_df.empty:
            # Fill up combo box
            standard_name_value = standard_df[["standard_name"]].values.ravel()
            standard_name_unique_value = list(pd.unique(standard_name_value))
            standard_name_unique_value.insert(0, "--Include All--")
            self.ui.standard_name_cbox.clear()
            self.ui.standard_name_cbox.addItems(standard_name_unique_value)

            if show_standard_name is not None:
                if show_standard_name == "all":
                    set_current_standard_name = "--Include All--"

                    # Filter based on parameter
                    df = StandardHandler(standard_df=standard_df,
                                         standard_name=standard_name_unique_value).filter_result()
                else:
                    set_current_standard_name = show_standard_name

                    # Filter based on parameter
                    df = StandardHandler(standard_df=standard_df,
                                         standard_name=[show_standard_name]).filter_result()

                self.ui.standard_name_cbox.setCurrentText(set_current_standard_name)

                # View in tableview
                model = PandasModel(df, self.standard_header_column)
                self.ui.standard_tabv.setModel(model)
                self.standard_selectionModel = self.ui.standard_tabv.selectionModel()
                self.standard_selectionModel.selectionChanged.connect(self.on_standard_table_selected)
        else:
            # Update combobox
            self.ui.standard_name_cbox.clear()

            # View in tableview
            df = pd.DataFrame()
            model = PandasModel(df, self.standard_header_column)
            self.ui.standard_tabv.setModel(model)
            self.standard_selectionModel = self.ui.standard_tabv.selectionModel()
            self.standard_selectionModel.selectionChanged.connect(self.on_standard_table_selected)

            # Disable button
            self.ui.edit_standard_btn.setDisabled(True)
            self.ui.delete_standard_btn.setDisabled(True)

    def config_ui(self):
        # ComboBox settings
        self.ui.vehicle_family_cbox.view().setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.ui.vehicle_id_cbox.view().setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.ui.standard_name_cbox.view().setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)

        # Tableview settings
        self.ui.vehicle_tabv.setSelectionBehavior(QTableView.SelectRows)
        self.ui.vehicle_tabv.setSelectionMode(QTableView.SingleSelection)
        self.ui.standard_tabv.setSelectionBehavior(QTableView.SelectRows)
        self.ui.standard_tabv.setSelectionMode(QTableView.SingleSelection)

        # Add, edit, delete vehicle button
        self.ui.add_vehicle_btn.setIcon(QtGui.QIcon(self.add_icon_path))
        self.ui.edit_vehicle_btn.setIcon(QtGui.QIcon(self.edit_icon_path))
        self.ui.delete_vehicle_btn.setIcon(QtGui.QIcon(self.delete_icon_path))

        # Add, edit, delete standard button
        self.ui.add_standard_btn.setIcon(QtGui.QIcon(self.add_icon_path))
        self.ui.edit_standard_btn.setIcon(QtGui.QIcon(self.edit_icon_path))
        self.ui.delete_standard_btn.setIcon(QtGui.QIcon(self.delete_icon_path))

    @staticmethod
    def show_dialog_popup(val, details=None):
        msg = QMessageBox()
        msg.setWindowTitle("Info")
        msg.setText(val)
        msg.setIcon(QMessageBox.Question)
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        if details:
            msg.setDetailedText(details)
        ans = msg.exec_()
        return ans

    @staticmethod
    def show_info_popup(val):
        msg = QMessageBox()
        msg.setWindowTitle("Info")
        msg.setText(val)
        msg.setIcon(QMessageBox.Information)
        msg.setWindowModality(QtCore.Qt.NonModal)
        msg.exec_()

    @staticmethod
    def show_alert_popup(val):
        msg = QMessageBox()
        msg.setWindowTitle("Error")
        msg.setText(val)
        msg.setIcon(QMessageBox.Critical)
        msg.exec_()


class PandasModel(QAbstractTableModel):

    def __init__(self, data, header_list):
        QAbstractTableModel.__init__(self)
        self._data = data
        self.header_list = header_list

    def rowCount(self, parent=None):
        return self._data.shape[0]

    def columnCount(self, parent=None):
        return self._data.shape[1]

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if index.isValid():
            if role == QtCore.Qt.DisplayRole:
                return str(self._data.iloc[index.row(), index.column()])
        return None

    def headerData(self, col, orientation, role=None):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            header_name = self.header_list[col]
            # self._data.columns[col]
            return header_name
        return None


class EditVehicleWindow(QtWidgets.QWidget):
    """
    Edit vehicle window init
    """
    def __init__(self, main_window, settings_window):
        super().__init__()

        # Init GUI window
        self.ui = Ui_edit_vehicle_window()
        self.ui.setupUi(self)
        self.main_window = main_window
        self.settings_window = settings_window

        # Init img path
        self.sensor_width_reference_path = "img/background/sensor_width.png"
        self.rear_distance_reference_path = "img/background/sensor_distance_rear.png"
        self.question_mark_icon_path = "img/icon/question.png"

        # Init variables
        self.selected_data = {}

        # Init save button status
        self.save_button_status = 'add'  # add/edit

        # Config ui
        self.config_ui()

        # Connect save button
        self.ui.save_vehicle_btn.clicked.connect(self.save_clicked)

        # Connect cancel button
        self.ui.cancel_vehicle_btn.clicked.connect(self.cancel_clicked)

    def save_clicked(self):
        valid, msg = self.check_user_input()
        if valid:
            self.save_vehicle_information()
        else:
            self.show_alert_popup(msg)

    def check_user_input(self):
        error_msg = ""

        # Copy dataframe
        vehicle_df = self.settings_window.vehicle_dataframe.copy()

        # Check vehicle family
        vehicle_family = self.ui.vehicle_family_ledit.text()
        if vehicle_family == "":
            valid_input = False
            error_msg = "Vehicle family field cannot be empty!"
        else:
            # Check vehicle ID
            vehicle_id = self.ui.vehicle_id_ledit.text()
            if vehicle_id == "":
                valid_input = False
                error_msg = "Vehicle ID field cannot be empty!"
            else:
                if not vehicle_df.empty:
                    # Filter based on parameter
                    vehicle_family_id_filtered = VehicleHandler(vehicle_df=vehicle_df,
                                                                vehicle_family=[vehicle_family]).filter_result()
                    vehicle_id_value = vehicle_family_id_filtered[["vehicle_id"]].values.ravel()
                    vehicle_id_unique_value = list(pd.unique(vehicle_id_value))
                    vehicle_id_unique_value = [str(i) for i in vehicle_id_unique_value]

                    if self.save_button_status == "add":
                        # Check if ID already exist
                        if vehicle_id in vehicle_id_unique_value:
                            valid_input = False
                            error_msg = f"Vehicle ID already exist in '{vehicle_family}'"
                        else:
                            valid_input = True
                    else:  # Edit
                        valid_input = True
                else:
                    valid_input = True

        return valid_input, error_msg

    def save_vehicle_information(self):
        vehicle_family = self.ui.vehicle_family_ledit.text()
        vehicle_id = self.ui.vehicle_id_ledit.text()
        vehicle_width = str(self.ui.vehicle_width_sbox.value())
        vehicle_width_between_sensor_a = str(self.ui.width_between_sensor_a_sbox.value())
        vehicle_width_between_sensor_b = str(self.ui.width_between_sensor_b_sbox.value())
        vehicle_width_between_sensor_c = str(self.ui.width_between_sensor_c_sbox.value())
        vehicle_width_between_sensor_d = str(self.ui.width_between_sensor_d_sbox.value())
        vehicle_width_between_sensor_z = str(self.ui.width_between_sensor_z_sbox.value())
        height_of_sensor1 = str(self.ui.height_sensor1_sbox.value())
        height_of_sensor2 = str(self.ui.height_sensor2_sbox.value())
        height_of_sensor3 = str(self.ui.height_sensor3_sbox.value())
        height_of_sensor4 = str(self.ui.height_sensor4_sbox.value())
        dist_rear_sensor1 = str(self.ui.dist_rear_face_sensor1_sbox.value())
        dist_rear_sensor2 = str(self.ui.dist_rear_face_sensor2_sbox.value())
        dist_rear_sensor3 = str(self.ui.dist_rear_face_sensor3_sbox.value())
        dist_rear_sensor4 = str(self.ui.dist_rear_face_sensor4_sbox.value())

        model_object = ConfigParser()
        model_object.read(VEHICLE_CONFIG_PATH + "/init.ini")

        model_object.set("INFO", "info_family", vehicle_family)
        model_object.set("INFO", "info_id", vehicle_id)
        model_object.set("INFO", "info_width", vehicle_width)
        model_object.set("INFO", "info_width", vehicle_width)
        model_object.set("WIDTH BETWEEN SENSOR", "wbs_point_a", vehicle_width_between_sensor_a)
        model_object.set("WIDTH BETWEEN SENSOR", "wbs_point_b", vehicle_width_between_sensor_b)
        model_object.set("WIDTH BETWEEN SENSOR", "wbs_point_c", vehicle_width_between_sensor_c)
        model_object.set("WIDTH BETWEEN SENSOR", "wbs_point_d", vehicle_width_between_sensor_d)
        model_object.set("WIDTH BETWEEN SENSOR", "wbs_point_z", vehicle_width_between_sensor_z)
        model_object.set("HEIGHT OF SENSOR", "hos_sensor1", height_of_sensor1)
        model_object.set("HEIGHT OF SENSOR", "hos_sensor2", height_of_sensor2)
        model_object.set("HEIGHT OF SENSOR", "hos_sensor3", height_of_sensor3)
        model_object.set("HEIGHT OF SENSOR", "hos_sensor4", height_of_sensor4)
        model_object.set("DISTANCE REAR FACE", "drf_sensor1", dist_rear_sensor1)
        model_object.set("DISTANCE REAR FACE", "drf_sensor2", dist_rear_sensor2)
        model_object.set("DISTANCE REAR FACE", "drf_sensor3", dist_rear_sensor3)
        model_object.set("DISTANCE REAR FACE", "drf_sensor4", dist_rear_sensor4)

        add_dataframe_model_dict = {"vehicle_family": [vehicle_family],
                                    "vehicle_id": [vehicle_id],
                                    "vehicle_width": [float(vehicle_width)],
                                    "width_between_sensor_point_a": [float(vehicle_width_between_sensor_a)],
                                    "width_between_sensor_point_b": [float(vehicle_width_between_sensor_b)],
                                    "width_between_sensor_point_c": [float(vehicle_width_between_sensor_c)],
                                    "width_between_sensor_point_d": [float(vehicle_width_between_sensor_d)],
                                    "width_between_sensor_point_z": [float(vehicle_width_between_sensor_z)],
                                    "height_of_sensor1": [float(height_of_sensor1)],
                                    "height_of_sensor2": [float(height_of_sensor2)],
                                    "height_of_sensor3": [float(height_of_sensor3)],
                                    "height_of_sensor4": [float(height_of_sensor4)],
                                    "distance_rear_face_sensor1": [float(dist_rear_sensor1)],
                                    "distance_rear_face_sensor2": [float(dist_rear_sensor2)],
                                    "distance_rear_face_sensor3": [float(dist_rear_sensor3)],
                                    "distance_rear_face_sensor4": [float(dist_rear_sensor4)]}

        user_clicked = self.show_save_confirmation_popup()
        if user_clicked == QMessageBox.Save:
            try:
                if self.save_button_status == "add":
                    config_file_name = "vehc_" + vehicle_family + "_id" + vehicle_id
                    with open(VEHICLE_CONFIG_PATH + "/" + config_file_name + ".ini", "w") as configfile:
                        model_object.write(configfile)

                    new_model = pd.DataFrame(add_dataframe_model_dict)

                    # Add model in productmodel_df
                    self.main_window.vehicle_dataframe = self.main_window.vehicle_dataframe.append(new_model,
                                                                                                   ignore_index=True)
                    # Update dataframe in settings window
                    self.settings_window.vehicle_dataframe = self.main_window.vehicle_dataframe
                else:  # edit
                    selected_config_file_name = "vehc_" + self.selected_data["vehicle family"] + "_id" + \
                                                str(self.selected_data["vehicle id"])
                    if os.path.exists(VEHICLE_CONFIG_PATH + "/" + selected_config_file_name + ".ini"):
                        os.remove(VEHICLE_CONFIG_PATH + "/" + selected_config_file_name + ".ini")
                        config_file_name = "vehc_" + vehicle_family + "_id" + vehicle_id
                        with open(VEHICLE_CONFIG_PATH + "/" + config_file_name + ".ini", "w") as configfile:
                            model_object.write(configfile)
                    else:
                        config_file_name = "vehc_" + vehicle_family + "_id" + vehicle_id
                        with open(VEHICLE_CONFIG_PATH + "/" + config_file_name + ".ini", "w") as configfile:
                            model_object.write(configfile)

                    # Drop selected column
                    df_copy = self.main_window.vehicle_dataframe.copy()
                    df_copy = df_copy.drop(df_copy[(df_copy["vehicle_family"] == self.selected_data["vehicle family"]) &
                                                   (df_copy["vehicle_id"] == self.selected_data["vehicle id"]) &
                                                   (df_copy["vehicle_width"] == self.selected_data["vehicle width"]) &
                                                   (df_copy["width_between_sensor_point_a"] ==
                                                    self.selected_data["vehicle width sensor a"]) &
                                                   (df_copy["width_between_sensor_point_b"] ==
                                                    self.selected_data["vehicle width sensor b"]) &
                                                   (df_copy["width_between_sensor_point_c"] ==
                                                    self.selected_data["vehicle width sensor c"]) &
                                                   (df_copy["width_between_sensor_point_d"] ==
                                                    self.selected_data["vehicle width sensor d"]) &
                                                   (df_copy["width_between_sensor_point_z"] ==
                                                    self.selected_data["vehicle width sensor z"]) &
                                                   (df_copy["height_of_sensor1"] ==
                                                    self.selected_data["vehicle height sensor1"]) &
                                                   (df_copy["height_of_sensor2"] ==
                                                    self.selected_data["vehicle height sensor2"]) &
                                                   (df_copy["height_of_sensor3"] ==
                                                    self.selected_data["vehicle height sensor3"]) &
                                                   (df_copy["height_of_sensor4"] ==
                                                    self.selected_data["vehicle height sensor4"]) &
                                                   (df_copy["distance_rear_face_sensor1"] ==
                                                    self.selected_data["vehicle distance rear sensor1"]) &
                                                   (df_copy["distance_rear_face_sensor2"] ==
                                                    self.selected_data["vehicle distance rear sensor2"]) &
                                                   (df_copy["distance_rear_face_sensor3"] ==
                                                    self.selected_data["vehicle distance rear sensor3"]) &
                                                   (df_copy["distance_rear_face_sensor4"] ==
                                                    self.selected_data["vehicle distance rear sensor4"])]
                                           .index, inplace=False)

                    new_model = pd.DataFrame(add_dataframe_model_dict)

                    # Add model in productmodel_df
                    df_copy = df_copy.append(new_model, ignore_index=True)

                    # Update dataframe in main window
                    self.main_window.vehicle_dataframe = df_copy

                    # Update dataframe in settings window
                    self.settings_window.vehicle_dataframe = df_copy

                # Update settings window display
                self.settings_window.update_vehicle_combo_box(show_vehicle_family=vehicle_family,
                                                              show_vehicle_id=vehicle_id)

                # Update main window combo box
                self.main_window.update_vehicle_combobox()

                # Notify after saved successfully
                system_log = f"Vehicle family: {vehicle_family}, Vehicle ID: {vehicle_id} successfully saved"
                logging.info(system_log)
                self.cancel_clicked()
                self.settings_window.ui.edit_vehicle_btn.setDisabled(True)
                self.settings_window.ui.delete_vehicle_btn.setDisabled(True)
                self.show_info_popup("Saved successfully!")
            except IOError:
                # Notify after fail to save
                system_log = f"Vehicle family: {vehicle_family}, Vehicle ID: {vehicle_id} fail to save"
                logging.error(system_log)
                self.cancel_clicked()
                self.settings_window.ui.edit_vehicle_btn.setDisabled(True)
                self.settings_window.ui.delete_vehicle_btn.setDisabled(True)
                self.show_alert_popup("Fail to save!")
        elif user_clicked == QMessageBox.Cancel:
            self.cancel_clicked()
        else:  # Discard
            pass

    def show_window(self, show_selected_data=None):
        self.ui.vehicle_family_ledit.setFocus()

        if show_selected_data is not None:
            # Update selected data attrb
            self.selected_data = show_selected_data

            # Fill up widgets
            self.ui.vehicle_family_ledit.setText(str(show_selected_data["vehicle family"]))
            self.ui.vehicle_id_ledit.setText(str(show_selected_data["vehicle id"]))
            self.ui.vehicle_width_sbox.setValue(float(show_selected_data["vehicle width"]))
            self.ui.width_between_sensor_a_sbox.setValue(float(show_selected_data["vehicle width sensor a"]))
            self.ui.width_between_sensor_b_sbox.setValue(float(show_selected_data["vehicle width sensor b"]))
            self.ui.width_between_sensor_c_sbox.setValue(float(show_selected_data["vehicle width sensor c"]))
            self.ui.width_between_sensor_d_sbox.setValue(float(show_selected_data["vehicle width sensor d"]))
            self.ui.width_between_sensor_z_sbox.setValue(float(show_selected_data["vehicle width sensor z"]))
            self.ui.height_sensor1_sbox.setValue(float(show_selected_data["vehicle height sensor1"]))
            self.ui.height_sensor2_sbox.setValue(float(show_selected_data["vehicle height sensor2"]))
            self.ui.height_sensor3_sbox.setValue(float(show_selected_data["vehicle height sensor3"]))
            self.ui.height_sensor4_sbox.setValue(float(show_selected_data["vehicle height sensor4"]))
            self.ui.dist_rear_face_sensor1_sbox.setValue(float(show_selected_data["vehicle distance rear sensor1"]))
            self.ui.dist_rear_face_sensor2_sbox.setValue(float(show_selected_data["vehicle distance rear sensor2"]))
            self.ui.dist_rear_face_sensor3_sbox.setValue(float(show_selected_data["vehicle distance rear sensor3"]))
            self.ui.dist_rear_face_sensor4_sbox.setValue(float(show_selected_data["vehicle distance rear sensor4"]))

        self.show()

    def cancel_clicked(self):
        self.ui.vehicle_family_ledit.clear()
        self.ui.vehicle_id_ledit.clear()
        self.ui.vehicle_width_sbox.setValue(0.0)
        self.ui.width_between_sensor_a_sbox.setValue(0.0)
        self.ui.width_between_sensor_b_sbox.setValue(0.0)
        self.ui.width_between_sensor_c_sbox.setValue(0.0)
        self.ui.width_between_sensor_d_sbox.setValue(0.0)
        self.ui.width_between_sensor_z_sbox.setValue(0.0)
        self.ui.height_sensor1_sbox.setValue(0.0)
        self.ui.height_sensor2_sbox.setValue(0.0)
        self.ui.height_sensor3_sbox.setValue(0.0)
        self.ui.height_sensor4_sbox.setValue(0.0)
        self.ui.dist_rear_face_sensor1_sbox.setValue(0.0)
        self.ui.dist_rear_face_sensor2_sbox.setValue(0.0)
        self.ui.dist_rear_face_sensor3_sbox.setValue(0.0)
        self.ui.dist_rear_face_sensor4_sbox.setValue(0.0)
        self.close()

    def config_ui(self):
        # Window
        self.setFixedSize(self.size())
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.CustomizeWindowHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMaximizeButtonHint)

        # Question mark icon
        w = self.ui.width_between_sensor_legend_lab.width()
        h = self.ui.width_between_sensor_legend_lab.height()
        question_mark_pixmap = QPixmap(self.question_mark_icon_path)
        sensor_width_question_mark_pixmap = question_mark_pixmap.scaled(w, h)
        self.ui.width_between_sensor_legend_lab.setPixmap(sensor_width_question_mark_pixmap)
        self.ui.width_between_sensor_legend_lab.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.ui.rear_distance_sensor_legend_lab.setPixmap(sensor_width_question_mark_pixmap)
        self.ui.rear_distance_sensor_legend_lab.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.ui.width_between_sensor_legend_lab.setToolTip('<img src=' + self.sensor_width_reference_path + '>')
        self.ui.rear_distance_sensor_legend_lab.setToolTip('<img src=' + self.rear_distance_reference_path + '>')

    @staticmethod
    def show_info_popup(val):
        msg = QMessageBox()
        msg.setWindowTitle("Info")
        msg.setText(val)
        msg.setIcon(QMessageBox.Information)
        msg.setWindowModality(QtCore.Qt.NonModal)
        msg.exec_()

    @staticmethod
    def show_alert_popup(val):
        msg = QMessageBox()
        msg.setWindowTitle("Error")
        msg.setText(val)
        msg.setIcon(QMessageBox.Critical)
        msg.exec_()

    @staticmethod
    def show_save_confirmation_popup():
        msg = QMessageBox()
        msg.setWindowTitle("Info")
        msg.setText("The document has been modified.")
        msg.setInformativeText("Do you want to save your changes?")
        msg.setStandardButtons(QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
        msg.setDefaultButton(QMessageBox.Save)
        ans = msg.exec()
        return ans


class EditStandardWindow(QtWidgets.QWidget):
    """
    Edit standard window init
    """
    def __init__(self, main_window, settings_window):
        super().__init__()

        # Init GUI window
        self.ui = Ui_edit_standard_window()
        self.ui.setupUi(self)
        self.main_window = main_window
        self.settings_window = settings_window

        # Init variables
        self.selected_data = {}

        # Init save button status
        self.save_button_status = 'add'  # add/edit

        # Config ui
        self.config_ui()

        # Connect save button
        self.ui.save_standard_btn.clicked.connect(self.save_clicked)

        # Connect cancel button
        self.ui.cancel_standard_btn.clicked.connect(self.cancel_clicked)

    def config_ui(self):
        # Window
        self.setFixedSize(self.size())
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.CustomizeWindowHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMaximizeButtonHint)

    def save_clicked(self):
        valid, msg = self.check_user_input()
        if valid:
            self.save_standard_information()
        else:
            self.show_alert_popup(msg)

    def check_user_input(self):
        # Copy dataframe
        standard_df = self.settings_window.standard_dataframe.copy()

        # Check standard name
        if not standard_df.empty:
            if self.ui.standard_name_ledit.text() == "":
                valid_input = False
                error_msg = "Standard name field cannot be empty!"
            else:
                standard_name = self.ui.standard_name_ledit.text()

                # Get unique value
                standard_name_value = standard_df[["standard_name"]].values.ravel()
                standard_name_unique_value = list(pd.unique(standard_name_value))

                if self.save_button_status == "add":
                    # Check if standard already exist
                    if standard_name in standard_name_unique_value:
                        valid_input = False
                        error_msg = "Standard name already exist!"
                    else:
                        valid_input = True
                        error_msg = ""
                else:  # Edit
                    valid_input = True
                    error_msg = ""
        else:
            if self.ui.standard_name_ledit.text() == "":
                valid_input = False
                error_msg = "Standard name field cannot be empty!"
            else:
                valid_input = True
                error_msg = ""

        return valid_input, error_msg

    def save_standard_information(self):
        standard_name = self.ui.standard_name_ledit.text()
        horizontal_coverage = str(self.ui.horizontal_min_coverage_sbox.value())
        vertical_coverage = str(self.ui.vertical_min_coverage_sbox.value())

        model_object = ConfigParser()
        model_object.read(STANDARD_CONFIG_FILE_PATH + "/init.ini")

        model_object.set("INFO", "standard_name", standard_name)
        model_object.set("COVERAGE", "coverage_horizontal_minimum_percentage", horizontal_coverage)
        model_object.set("COVERAGE", "coverage_vertical_minimum_percentage", vertical_coverage)

        add_dataframe_model_dict = {"standard_name": [standard_name],
                                    "standard_horizontal_minimum_coverage": [horizontal_coverage],
                                    "standard_vertical_minimum_coverage": [vertical_coverage]}

        user_clicked = self.show_save_confirmation_popup()
        if user_clicked == QMessageBox.Save:
            try:
                if self.save_button_status == "add":
                    config_file_name = "std_" + standard_name
                    with open(STANDARD_CONFIG_FILE_PATH + "/" + config_file_name + ".ini", "w") as configfile:
                        model_object.write(configfile)

                    new_model = pd.DataFrame(add_dataframe_model_dict)

                    # Add model in productmodel_df
                    self.main_window.standard_dataframe = self.main_window.standard_dataframe.append(new_model,
                                                                                                     ignore_index=True)
                    # Update dataframe in settings window
                    self.settings_window.standard_dataframe = self.main_window.standard_dataframe
                else:  # edit
                    selected_config_file_name = "std_" + self.selected_data["standard name"]
                    if os.path.exists(STANDARD_CONFIG_FILE_PATH + "/" + selected_config_file_name + ".ini"):
                        os.remove(STANDARD_CONFIG_FILE_PATH + "/" + selected_config_file_name + ".ini")
                        config_file_name = "std_" + standard_name
                        with open(STANDARD_CONFIG_FILE_PATH + "/" + config_file_name + ".ini", "w") as configfile:
                            model_object.write(configfile)
                    else:
                        config_file_name = "std_" + standard_name
                        with open(STANDARD_CONFIG_FILE_PATH + "/" + config_file_name + ".ini", "w") as configfile:
                            model_object.write(configfile)

                    # Drop selected column
                    df_copy = self.main_window.standard_dataframe.copy()
                    df_copy = df_copy.drop(df_copy[(df_copy["standard_name"] ==
                                                    self.selected_data["standard name"])].index, inplace=False)

                    new_model = pd.DataFrame(add_dataframe_model_dict)

                    # Add model in productmodel_df
                    df_copy = df_copy.append(new_model, ignore_index=True)

                    # Update dataframe in main window
                    self.main_window.standard_dataframe = df_copy

                    # Update dataframe in settings window
                    self.settings_window.standard_dataframe = df_copy

                # Update settings window display
                self.settings_window.update_standard_combo_box(show_standard_name=standard_name)

                # Update main window combo box
                self.main_window.update_standard_combobox()

                # Notify after saved successfully
                system_log = f"{standard_name} successfully saved"
                logging.info(system_log)
                self.cancel_clicked()
                self.settings_window.ui.edit_standard_btn.setDisabled(True)
                self.settings_window.ui.delete_standard_btn.setDisabled(True)
                self.show_info_popup("Saved successfully!")
            except IOError:
                # Notify after fail to save
                system_log = f"{standard_name} fail to save"
                logging.error(system_log)
                self.cancel_clicked()
                self.settings_window.ui.edit_standard_btn.setDisabled(True)
                self.settings_window.ui.delete_standard_btn.setDisabled(True)
                self.show_alert_popup("Fail to save!")
        elif user_clicked == QMessageBox.Cancel:
            self.cancel_clicked()
        else:  # Discard
            pass

    def show_window(self, show_selected_data=None):
        self.ui.standard_name_ledit.setFocus()

        if show_selected_data is not None:
            # Update selected data attrb
            self.selected_data = show_selected_data

            # Fill up widgets
            self.ui.standard_name_ledit.setText(str(show_selected_data["standard name"]))
            self.ui.horizontal_min_coverage_sbox.setValue(float(show_selected_data["minimum horizontal"]))
            self.ui.vertical_min_coverage_sbox.setValue(float(show_selected_data["minimum vertical"]))

        self.show()

    def cancel_clicked(self):
        self.ui.standard_name_ledit.clear()
        self.ui.horizontal_min_coverage_sbox.setValue(0.0)
        self.ui.vertical_min_coverage_sbox.setValue(0.0)
        self.close()

    @staticmethod
    def show_info_popup(val):
        msg = QMessageBox()
        msg.setWindowTitle("Info")
        msg.setText(val)
        msg.setIcon(QMessageBox.Information)
        msg.setWindowModality(QtCore.Qt.NonModal)
        msg.exec_()

    @staticmethod
    def show_alert_popup(val):
        msg = QMessageBox()
        msg.setWindowTitle("Error")
        msg.setText(val)
        msg.setIcon(QMessageBox.Critical)
        msg.exec_()

    @staticmethod
    def show_save_confirmation_popup():
        msg = QMessageBox()
        msg.setWindowTitle("Info")
        msg.setText("The document has been modified.")
        msg.setInformativeText("Do you want to save your changes?")
        msg.setStandardButtons(QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
        msg.setDefaultButton(QMessageBox.Save)
        ans = msg.exec()
        return ans


class VehicleHandler:

    def __init__(self, vehicle_df, vehicle_family, vehicle_id=None):
        self.vehicle_df = vehicle_df
        self.vehicle_family = vehicle_family
        self.vehicle_id = vehicle_id

    def filter_result(self):
        # Filter df
        if self.vehicle_id is not None:
            if not self.vehicle_id:
                filtered_vehicle = self.vehicle_df[self.vehicle_df['vehicle_family'].isin(self.vehicle_family)]
            else:
                filtered_vehicle = self.vehicle_df[(self.vehicle_df['vehicle_family'].isin(self.vehicle_family)) &
                                                   (self.vehicle_df['vehicle_id'].isin(self.vehicle_id))]
        else:
            filtered_vehicle = self.vehicle_df[self.vehicle_df['vehicle_family'].isin(self.vehicle_family)]

        return filtered_vehicle


class StandardHandler:

    def __init__(self, standard_df, standard_name):
        self.standard_df = standard_df
        self.standard_name = standard_name

    def filter_result(self):
        filtered_standard = self.standard_df[self.standard_df['standard_name'].isin(self.standard_name)]
        return filtered_standard


class ReportWindow(QtWidgets.QWidget):
    def __init__(self, main_window):
        super().__init__()

        # Init GUI window
        self.ui = Ui_report_window()
        self.ui.setupUi(self)
        self.main_window = main_window

        # Init save path
        self.save_path = False

        # Init filtered result dataframe
        self.filtered_result_df = pd.DataFrame()

        # Init header column list
        self.header_column_list = ["OPERATOR NAME", "START DATE", "END DATE",
                                   "VEHICLE FAMILY", "VEHICLE ID", "VEHICLE WIDTH",
                                   "WIDTH BETWEEN SENSOR (a)", "WIDTH BETWEEN SENSOR (b)", "WIDTH BETWEEN SENSOR (c)",
                                   "WIDTH BETWEEN SENSOR (d)", "WIDTH BETWEEN SENSOR (z)",
                                   "HEIGHT FROM GROUND SENSOR 1", "HEIGHT FROM GROUND SENSOR 2",
                                   "HEIGHT FROM GROUND SENSOR 3", "HEIGHT FROM GROUND SENSOR 4",
                                   "DISTANCE REAR SENSOR 1", "DISTANCE REAR SENSOR 2", "DISTANCE REAR SENSOR 3",
                                   "DISTANCE REAR SENSOR 4", "STANDARD NAME", "STANDARD COVERAGE SPEC", "TEST TYPE",
                                   "HORIZONTAL TOTAL TEST POINT", "HORIZONTAL DETECTED TEST POINT",
                                   "VERTICAL FIRST SUB TYPE NAME", "VERTICAL FIRST SUB TYPE TOTAL TEST POINT",
                                   "VERTICAL FIRST SUB TYPE DETECTED TEST POINT",
                                   "VERTICAL SECOND SUB TYPE NAME", "VERTICAL SECOND SUB TYPE TOTAL TEST POINT",
                                   "VERTICAL SECOND SUB TYPE DETECTED TEST POINT",
                                   "DETECTED COVERAGE", "OVERALL RESULT"]

        # Choose path to save button
        self.ui.rep_save_to_btn.clicked.connect(self.choose_save_path)

        # Connect cancel button
        self.ui.rep_cancel_btn.clicked.connect(self.cancel_clicked)

        # Connect preview button
        self.ui.rep_preview_btn.clicked.connect(self.show_spec_result)

        # Connect generate report button
        self.ui.rep_generate_btn.clicked.connect(self.generate_report)

    def show_window(self):
        # Copy log df from main window
        log_df = self.main_window.log_df.copy()

        # Get unique value
        if not log_df.empty:
            # Operator name
            operator_name_value = log_df[["operator_name"]].values.ravel()
            operator_name_unique_value = ['--Include All--'] + list(pd.unique(operator_name_value))
            # Vehicle family
            vehicle_family_value = log_df[["vehicle_family"]].values.ravel()
            vehicle_family_unique_value = ['--Include All--'] + list(pd.unique(vehicle_family_value))
            # Vehicle id
            vehicle_id_value = log_df[["vehicle_id"]].values.ravel()
            vehicle_id_unique_value = ['--Include All--'] + list(pd.unique(vehicle_id_value))
            # Standard
            standard_value = log_df[["standard_name"]].values.ravel()
            standard_unique_value = ['--Include All--'] + list(pd.unique(standard_value))
            # Test type
            test_type_value = log_df[["test_type"]].values.ravel()
            test_type_unique_value = ['--Include All--'] + list(pd.unique(test_type_value))
            # Date before, date after
            start_date_list = log_df['start_date'].unique().tolist()
            start_date_split = start_date_list[0].split(" ")[0]
            start_date_split = start_date_split.split('/')
            end_date_split = start_date_list[-1].split(" ")[0]
            end_date_split = end_date_split.split('/')
            start_date = QDate(int(start_date_split[0]), int(start_date_split[1]), int(start_date_split[2]))
            end_date = QDate(int(end_date_split[0]), int(end_date_split[1]), int(end_date_split[2]))
        else:
            operator_name_unique_value = ['No data']
            vehicle_family_unique_value = ['No data']
            vehicle_id_unique_value = ['No data']
            standard_unique_value = ['No data']
            test_type_unique_value = ['No data']
            start_date = QDate(2000, 1, 1)
            end_date = QDate(2000, 1, 1)

        # Update combo box
        # Clear content
        self.ui.rep_operator_name_cbox.clear()
        self.ui.rep_vehicle_family_cbox.clear()
        self.ui.rep_vehicle_id_cbox.clear()
        self.ui.rep_standard_cbox.clear()
        self.ui.rep_test_type_cbox.clear()
        # Add items
        self.ui.rep_operator_name_cbox.addItems(operator_name_unique_value)
        self.ui.rep_vehicle_family_cbox.addItems(vehicle_family_unique_value)
        self.ui.rep_vehicle_id_cbox.addItems(vehicle_id_unique_value)
        self.ui.rep_standard_cbox.addItems(standard_unique_value)
        self.ui.rep_test_type_cbox.addItems(test_type_unique_value)

        self.ui.rep_date_before_dedit.setDate(start_date)
        self.ui.rep_date_after_dedit.setDate(end_date)

        # Show result tableview
        self.show_spec_result()

        self.show()

    def cancel_clicked(self):
        self.ui.rep_save_path_lab.clear()
        self.save_path = False
        self.close()

    def choose_save_path(self):
        path_selected = QFileDialog.getSaveFileName(self, 'Save file', 'C:/', "Excel files (*.xlsx)")
        if path_selected[0]:
            self.save_path = path_selected[0]
            self.ui.rep_save_path_lab.setText(self.save_path)
        else:
            self.save_path = False

    def show_spec_result(self):
        # Copy log df from main window
        log_df = self.main_window.log_df.copy()

        # Gather selected info from user
        if not log_df.empty:
            operator_name = self.ui.rep_operator_name_cbox.currentText()
            if operator_name == '--Include All--':
                operator_name = False
            else:
                operator_name = [operator_name]
            vehicle_family = self.ui.rep_vehicle_family_cbox.currentText()
            if vehicle_family == '--Include All--':
                vehicle_family = False
            else:
                vehicle_family = [vehicle_family]
            vehicle_id = self.ui.rep_vehicle_id_cbox.currentText()
            if vehicle_id == '--Include All--':
                vehicle_id = False
            else:
                vehicle_id = [vehicle_id]
            standard = self.ui.rep_standard_cbox.currentText()
            if standard == '--Include All--':
                standard = False
            else:
                standard = [standard]
            test_type = self.ui.rep_test_type_cbox.currentText()
            if test_type == '--Include All--':
                test_type = False
            else:
                test_type = [test_type]
            start_date = self.ui.rep_date_before_dedit.date().toString("dd/MM/yyyy")
            end_date = self.ui.rep_date_after_dedit.date().toString("dd/MM/yyyy")
        else:
            operator_name = False
            vehicle_family = False
            vehicle_id = False
            standard = False
            test_type = False
            start_date = False
            end_date = False

        # Filter log df
        df = ResultHandler(log_df=log_df, operator_name=operator_name, vehicle_family=vehicle_family,
                           vehicle_id=vehicle_id, standard=standard, test_type=test_type, start_date=start_date,
                           end_date=end_date).filter_result()

        # Update to filtered result dataframe
        self.filtered_result_df = df

        # Convert to qt model
        model = PandasModel(df, self.header_column_list)

        # Set model to table view widget
        self.ui.rep_result_spec_tabv.setModel(model)
        self.ui.rep_result_spec_tabv.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

    def generate_report(self):
        if self.save_path:
            try:
                # Generate excel
                OutputExcelHandler(format_excel_path=FORMAT_REPORT_PATH, output_excel_path=self.save_path,
                                   log_df=self.filtered_result_df).generate()

                # Popup notify
                system_log = f'{self.save_path} successfully saved!'
                logging.info(system_log)
                msg = QMessageBox()
                msg.setWindowTitle("Info")
                msg.setText(system_log)
                msg.setIcon(QMessageBox.Information)
                msg.exec_()
            except PermissionError:
                # Popup notify
                system_log = f'{self.save_path} fail to save!'
                logging.error(system_log)
                msg = QMessageBox()
                msg.setWindowTitle("Info")
                msg.setText(system_log)
                msg.setIcon(QMessageBox.Critical)
                msg.exec_()

        else:
            msg = QMessageBox()
            msg.setWindowTitle("Info")
            msg.setText("Path not selected!")
            msg.setIcon(QMessageBox.Critical)
            msg.exec_()


class ResultHandler:
    def __init__(self, log_df, operator_name, vehicle_family, vehicle_id, standard, test_type, start_date, end_date):
        self.log_df = log_df
        self.operator_name = operator_name
        self.vehicle_family = vehicle_family
        self.vehicle_id = vehicle_id
        self.standard = standard
        self.test_type = test_type
        self.start_date = start_date
        self.end_date = end_date

    def filter_result(self):
        filtered_df = self.log_df

        # Convert column to datetime and arrange data by date
        filtered_df['start_date'] = pd.to_datetime(filtered_df['start_date'], format='%Y/%m/%d %H:%M:%S',
                                                          errors='ignore')
        filtered_df['end_date'] = pd.to_datetime(filtered_df['end_date'], format='%Y/%m/%d %H:%M:%S',
                                                        errors='ignore')
        filtered_df = filtered_df.sort_values('start_date', ascending=True)

        if self.operator_name:
            filtered_df = filtered_df[filtered_df['operator_name'].isin(self.operator_name)]

        if self.vehicle_family:
            filtered_df = filtered_df[filtered_df['vehicle_family'].isin(self.vehicle_family)]

        if self.vehicle_id:
            filtered_df = filtered_df[filtered_df['vehicle_id'].isin(self.vehicle_id)]

        if self.standard:
            filtered_df = filtered_df[filtered_df['standard_name'].isin(self.standard)]

        if self.test_type:
            filtered_df = filtered_df[filtered_df['test_type'].isin(self.test_type)]

        if self.start_date:
            self.start_date = pd.Timestamp(self.start_date).strftime("%d/%m/%Y")
            filtered_df = filtered_df[filtered_df['start_date'] >= self.start_date]

        if self.end_date:
            # Add one day to before date
            new_date = datetime.strptime(self.end_date, "%d/%m/%Y") + timedelta(1)
            str_new_date = new_date.strftime("%d/%m/%Y")
            pd_date = pd.Timestamp(str_new_date).strftime("%d/%m/%Y")
            filtered_df = filtered_df[filtered_df['start_date'] <= pd_date]

        return filtered_df


class OutputExcelHandler:
    def __init__(self, format_excel_path, output_excel_path, log_df):
        self.format_path = format_excel_path
        self.output_path = output_excel_path
        self.log_df = log_df

        # Load excel workbook
        self.xfile = load_workbook(format_excel_path)

    def generate(self):
        # Calculate testing period
        testing_period_str = self.calculate_testing_period()

        # Calculate total testing, pass and fail
        total_test, total_pass, total_fail = self.calculate_total_testing()

        # Write to output path
        sheet = self.xfile.get_sheet_by_name('Sheet1')

        # Put in date
        self.add_report_text(sheet, testing_period_str, 2, 3)

        # Put total testing, pass and fail
        self.add_report_text(sheet, total_test, 3, 3)
        self.add_report_text(sheet, total_pass, 4, 3)
        self.add_report_text(sheet, total_fail, 4, 4)

        # Fill in the results for all tests
        current_row = 10
        df_column_list = self.log_df.columns.tolist()

        for __, row in self.log_df.iterrows():
            current_row += 1
            for idx, each_item in enumerate(df_column_list):
                current_column = idx + 2
                data_val = row[each_item]
                self.add_report_text(sheet, data_val, current_row, current_column)
        total_data = len(self.log_df)

        # Fill product no
        for each_no in range(total_data):
            current_no = each_no + 1
            current_row = 10 + current_no
            self.add_report_text(sheet, current_no, current_row, 1)

        # Merge certain cells
        sheet.merge_cells('C2:D2')
        sheet.merge_cells('C3:D3')

        # Save excel
        self.xfile.save(self.output_path)
        self.xfile.close()

    def calculate_testing_period(self):
        starting = self.log_df.iloc[0]['start_date']
        ending = self.log_df.iloc[-1]['end_date']
        testing_period_str = f'{starting} ~ {ending}'

        return testing_period_str

    def calculate_total_testing(self):
        total_test = len(self.log_df)
        total_pass = len(self.log_df[self.log_df['overall_result'] == 'PASS'])
        total_fail = len(self.log_df[self.log_df['overall_result'] == 'FAIL'])

        return total_test, total_pass, total_fail

    @staticmethod
    def add_report_text(excel_sheet, str_value, text_row: int, text_col: int):
        """
        Add text to the excel report. To match the cell in string reference, the index column is added with
        value one.
        :param excel_sheet:
            Openpyxl Sheet
        :param str_value:
            String to put into report
        :param text_col:
            Cell column selection
        :param text_row:
            Cell row selection
        """
        font = Font(name='calibri', size=11)
        alignment = Alignment(horizontal='center', vertical='center', wrap_text=True)
        excel_sheet.cell(row=text_row, column=text_col).font = font
        excel_sheet.cell(row=text_row, column=text_col).alignment = alignment
        if str_value == "-":
            excel_sheet.cell(row=text_row, column=text_col).value = ""
        else:
            excel_sheet.cell(row=text_row, column=text_col).value = str_value

        # Color background if needed
        if str_value == 'PASS':
            excel_sheet.cell(row=text_row, column=text_col).fill = PatternFill(fgColor="00B050", fill_type="solid")
        elif str_value == 'FAIL':
            excel_sheet.cell(row=text_row, column=text_col).fill = PatternFill(fgColor="FF0000", fill_type="solid")
        elif str_value == '-':
            excel_sheet.cell(row=text_row, column=text_col).fill = PatternFill(fgColor="808080", fill_type="solid")


class AboutWindow(QtWidgets.QWidget):
    """
    Widget that displayed Software Information
    """

    def __init__(self, mainw):
        super().__init__()

        # Init GUI window
        self.ui = Ui_AboutWindow()
        self.ui.setupUi(self)
        self.main_window = mainw

        # Configure table
        self.config_ui()

        # Connect
        self.ui.close_btn.clicked.connect(self.quit)

    def config_ui(self):
        # Get config
        config_object = ConfigParser()
        config_object.read(SOFTWARE_INFO_PATH)

        # Window
        self.setFixedSize(self.size())
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.CustomizeWindowHint)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMaximizeButtonHint)

        # Set logo
        self.ui.logo.setPixmap(QtGui.QPixmap("img/company logo/cc_logo.png"))

        # Set title
        title = config_object.get("INFO", "title")
        self.ui.title.setText(title)

        # Set version
        version = config_object.get("INFO", "version")
        self.ui.version.setText(f"Version {version}")

        # Set description
        description = config_object.get("INFO", "description")
        self.ui.description.setText(description)

        # Set copyright
        copy_right = config_object.get("INFO", "copyright")
        self.ui.copyright.setText(copy_right)

    def show_window(self):
        self.show()

    def quit(self):
        self.close()


if __name__ == "__main__":
    # Init log
    init_log()

    # Log runtime exception
    sys.excepthook = excepthook

    app = QtWidgets.QApplication([])
    window = MainWindow()
    window.showFullScreen()
    app.exec_()

    # Log exit
    logging.info("PROGRAM SHUTDOWN".center(65, "="))
