import pandas as pd
import numpy as np
from datetime import datetime, timedelta
from dask import dataframe as ddf
from openpyxl import load_workbook
from openpyxl.styles import Font, Alignment, PatternFill

pd.set_option('max_columns', None)
pd.set_option('max_colwidth', None)
pd.set_option('max_rows', None)

LOG_DIR = 'logs/*.csv'
EXCEL_PATH = 'config/reportFormat/report_format.xlsx'


class ResultHandler:

    def __init__(self, excel_format_path: str, log_path, excel_output_path='report.xlsx',
                 op_name='', lot_no='', after_date='', before_date='', product_model=''):
        """
        :param excel_format_path: path of excel
        :param excel_output_path: path of excel
        :param op_name: operator name
        :param lot_no: product lot no
        :param after_date: Date of since test start (%d/%m/%Y)
        :param before_date: Date of before test start (%d/%m/%Y)
        :param product_model: Name of product model
        """
        self.df_log = ddf.read_csv(log_path, assume_missing=True, dtype=str)
        self.filtered_data = pd.DataFrame()
        self.op_name = op_name
        self.lot_no = lot_no
        self.after_date = after_date
        self.before_date = before_date
        self.product_model = product_model
        self.xfile = load_workbook(excel_format_path)
        self.excel_output_path = excel_output_path
        self.check_filter_result = False

    def filter_result(self):
        """
        Update and return the filtered_data based on operator name, lot no, product model and the test date
        :return: filtered panda dataframe
        """
        # Filter by product model
        if self.product_model:
            self.filtered_data = self.df_log[self.df_log['product_name'] == self.product_model].compute()
        else:
            self.filtered_data = self.df_log.compute()

        # Convert column to datetime and arrange data by date
        self.filtered_data['start_time'] = pd.to_datetime(self.filtered_data['start_time'], format='%Y/%m/%d %H:%M:%S',
                                                          errors='ignore')
        self.filtered_data['end_time'] = pd.to_datetime(self.filtered_data['end_time'], format='%Y/%m/%d %H:%M:%S',
                                                        errors='ignore')
        self.filtered_data = self.filtered_data.sort_values('start_time', ascending=True)

        # Filter by lot no
        if self.lot_no:
            self.lot_no = int(self.lot_no)
            self.filtered_data['lot_no'] = self.filtered_data['lot_no'].astype(int)
            self.filtered_data = self.filtered_data[self.filtered_data['lot_no'] == self.lot_no]

        # Filter by name
        if self.op_name:
            self.filtered_data = self.filtered_data[self.filtered_data['op_name'] == self.op_name]

        # Filter by date
        if self.after_date:
            self.after_date = pd.Timestamp(self.after_date).strftime("%d/%m/%Y")
            self.filtered_data = self.filtered_data[self.filtered_data['start_time'] >= self.after_date]
        if self.before_date:
            # Add one day to before date
            new_date = datetime.strptime(self.before_date, "%d/%m/%Y") + timedelta(1)
            str_new_date = new_date.strftime("%d/%m/%Y")

            pd_date = pd.Timestamp(str_new_date).strftime("%d/%m/%Y")
            self.filtered_data = self.filtered_data[self.filtered_data['start_time'] <= pd_date]

        # Check all result condition
        condlist = [
            self.filtered_data.eq('NG').any(1),
        ]
        choicelist = ['NG']
        self.filtered_data['Overall Status'] = np.select(condlist, choicelist, default='PASS')

        new_col_arrange = ['op_id', 'op_name', 'start_time', 'end_time', 'product_name', 'lot_no']
        old_cols_arrange = [i for i in self.filtered_data.columns.tolist() if i not in new_col_arrange]

        self.filtered_data = self.filtered_data[new_col_arrange + old_cols_arrange]
        self.check_filter_result = True

        return self.filtered_data

    def available_selection(self):
        """
        Return list of available operator name, lot no, product model and date available for selection
        [[operator name], [lot no], [product model], [date]]. Run filter_result first
        :return: list of option availability
        """
        # Prevent updating the filtered data if filtering already run
        if not self.check_filter_result:
            self.filter_result()

        operator_list = self.filtered_data['op_name'].unique().tolist()
        lot_list = self.filtered_data['lot_no'].unique().astype(int).astype(str).tolist()
        product_list = self.filtered_data['product_name'].unique().tolist()
        start_df = self.filtered_data['start_time'].dt.strftime("%d/%m/%Y").unique().astype(str).tolist()

        return [operator_list, lot_list, product_list, start_df]

    def result_info(self):
        """
        Return list containing int total test, pass and fail. Run filter_result first
        :return: [total pass, pass, fail]
        """
        # Prevent updating the filtered data if filtering already run
        if not self.check_filter_result:
            self.filter_result()

        total_test = len(self.filtered_data)
        total_pass = len(self.filtered_data[self.filtered_data['Overall Status'] == 'PASS'])
        total_fail = len(self.filtered_data[self.filtered_data['Overall Status'] == 'NG'])

        return [total_test, total_pass, total_fail]

    def result_date(self):
        """
        Return string of date start and end. Run filter_result first
        :return: string of date
        """
        # Prevent updating the filtered data if filtering already run
        if not self.check_filter_result:
            self.filter_result()
        starting = self.filtered_data.iloc[0]['start_time'].strftime("%d/%m/%Y %H:%M:%S")
        ending = self.filtered_data.iloc[-1]['end_time'].strftime("%d/%m/%Y %H:%M:%S")

        return f'{starting} ~ {ending}'

    def output_excel(self):
        """
        Output the test report in excel file. Run filter_result first
        """
        # Prevent updating the filtered data if filtering already run
        if not self.check_filter_result:
            self.filter_result()
        total_test, total_pass, total_fail = self.result_info()
        date_info = self.result_date()
        sheet = self.xfile.get_sheet_by_name('Sheet1')

        # Put in date
        self.add_report_text(sheet, date_info, 4, 2)

        # Put total testing, pass and fail
        self.add_report_text(sheet, total_test, 4, 3)
        self.add_report_text(sheet, total_pass, 4, 4)
        self.add_report_text(sheet, total_fail, 5, 4)

        # Fill in the results for all tests
        current_row = 10
        df_column_list = self.filtered_data.columns.tolist()

        for __, row in self.filtered_data.iterrows():
            current_row += 1
            for idx, each_item in enumerate(df_column_list):
                current_column = idx + 2
                data_val = row[each_item]
                self.add_report_text(sheet, data_val, current_column, current_row)

        total_data = len(self.filtered_data)

        # Fill product no
        for each_no in range(total_data):
            current_no = each_no + 1
            current_row = 10 + current_no
            self.add_report_text(sheet, current_no, 1, current_row)

        # Merge certain cells
        sheet.merge_cells('D2:E2')
        sheet.merge_cells('D3:E3')

        # Save excel
        self.xfile.save(self.excel_output_path)
        self.xfile.close()

    @staticmethod
    def add_report_text(excel_sheet, str_value, text_col: int, text_row: int):
        """
        Add text to the excel report. To match the cell in string reference, the index column is added with
        value one.
        :param excel_sheet:
            Openpyxl Sheet
        :param str_value:
            String to put into report
        :param text_col:
            Cell column selection
        :param text_row:
            Cell row selection
        """
        font = Font(name='calibri', size=11)
        alignment = Alignment(horizontal='center', vertical='bottom', wrap_text=True)
        excel_sheet.cell(row=text_row, column=text_col).value = str_value
        excel_sheet.cell(row=text_row, column=text_col).font = font
        excel_sheet.cell(row=text_row, column=text_col).alignment = alignment

        # Color background if needed
        if str_value == 'PASS':
            excel_sheet.cell(row=text_row, column=text_col).fill = PatternFill(fgColor="00B050", fill_type="solid")
        elif str_value == 'NG':
            excel_sheet.cell(row=text_row, column=text_col).fill = PatternFill(fgColor="FF0000", fill_type="solid")


def main():
    test = ResultHandler(EXCEL_PATH,  log_path=LOG_DIR, excel_output_path='test_excel.xlsx', op_name='',
                         lot_no='', product_model='', after_date='', before_date='')
    test.filter_result()
    test.output_excel()


if __name__ == '__main__':
    main()
