"""
Communication interface with connected MCUs
"""
import re
import time
import serial
import pandas as pd
from serial import Serial, SerialException


class NgCounter:
    """
    Class for interfacing with NG Counter
    """

    def __init__(self, ng_com: str, ng_baud_rate: int, ng_timeout: float, ng_start_delay: float, ng_file_path: str):
        """
        ng_com: COM port
        ng_baud_rate: baud rate
        ng_start_delay: Time delay when opening COM Port
        ng_file_path: Path to storing Master PC Ng count
        """
        self.ng_com = ng_com
        self.ng_baud_rate = ng_baud_rate
        self.ng_timeout = ng_timeout
        self.ng_start_delay = ng_start_delay
        self.ng_file_path = ng_file_path
        self.ng_serial = Serial()

        # Read and store current value
        self.df_count = pd.read_csv(ng_file_path)
        self.ng_count = int(self.df_count['counter'][0])

    def open_port(self):
        try:
            self.ng_serial.port = self.ng_com
            self.ng_serial.baudrate = self.ng_baud_rate
            self.ng_serial.timeout = self.ng_timeout
            self.ng_serial.open()
            time.sleep(self.ng_start_delay)
            port_opened = True
        except SerialException:
            port_opened = False

        return port_opened

    def reset_counter(self):
        """
        Reset Counter for both Master PC and NG Counter
        """
        self.ng_count = 0
        self.df_count['counter'][0] = 0

        # Save to file
        self.df_count.to_csv(self.ng_file_path, index=False)

        # Reset ng box
        command_value = 'R'  # either char or hex value [in string format]
        chg_cmd_to_hex = True  # if char, change to True
        list_of_data = []
        zfill_list = []
        reset_byte = RequestByte(command_val=command_value, char_cond=chg_cmd_to_hex,
                                 data_val_list=list_of_data, data_zfill_list=zfill_list).out_bytearray()

        # Check if open/not open
        if not self.ng_serial.isOpen():
            port_open = self.open_port()
        else:
            port_open = True

        # Write request/Read respond
        if port_open:
            self.ng_serial.write(reset_byte)
            read_byte = self.ng_serial.read(5)
            if read_byte == b'[T\x00T]':
                resp_status = True
            else:
                resp_status = False
        else:
            resp_status = False

        return resp_status

    def update_counter(self):
        """
        Update NG box with value from Master PC NG count
        """
        command_value = 'V'  # either char or hex value [in string format]
        chg_cmd_to_hex = True  # if char, change to True
        list_of_data = [self.ng_count]
        zfill_list = [4]
        update_byte = RequestByte(command_val=command_value, char_cond=chg_cmd_to_hex,
                                  data_val_list=list_of_data, data_zfill_list=zfill_list).out_bytearray()

        # Check if open/not open
        if not self.ng_serial.isOpen():
            port_open = self.open_port()
        else:
            port_open = True

        # Write request/Read respond
        if port_open:
            self.ng_serial.write(update_byte)
            read_byte = self.ng_serial.read(5)
            if read_byte == b'[T\x00T]':
                update_status = True
            else:
                update_status = False
        else:
            update_status = False

        return update_status

    def get_counter_val(self):
        """
        Update PC Master Ng count with NG box count
        """
        # Prep for getting counter value request
        command_value = 'N'  # either char or hex value [in string format]
        chg_cmd_to_hex = True  # if char, change to True
        list_of_data = []
        zfill_list = []

        get_count_byte = RequestByte(command_val=command_value, char_cond=chg_cmd_to_hex,
                                     data_val_list=list_of_data, data_zfill_list=zfill_list).out_bytearray()

        # Check if open/not open
        if not self.ng_serial.isOpen():
            port_open = self.open_port()
        else:
            port_open = True

        # Write request/Read respond
        if port_open:
            self.ng_serial.write(get_count_byte)
            read_byte = self.ng_serial.read(9)

            if read_byte == b'[F\x00F]':
                get_count_status = False
            else:
                get_count_status = True

            # Check byte
            check_byte = CheckResponse(response_byte=read_byte, cmd_str='N').check_data_chksum_cmdstr()

            if not check_byte:
                get_count_status = False

            # Get value from ng box
            if get_count_status:
                dict_val = GetData(response_byte=read_byte, data_pos=['ng_count']).split_data()

                # Update csv file
                self.ng_count = int(dict_val['ng_count'])
                self.df_count['counter'][0] = self.ng_count
                self.df_count.to_csv(self.ng_file_path, index=False)
        else:
            get_count_status = False

        return get_count_status

    def compare_box_file(self):
        """
        Compare the Master PC count and NG box count. If counter values matches, returns True.
        Else, will return False (bool)
        """
        print("COMPARE NG BOX")
        # Get value from NG box
        command_value = 'N'  # either char or hex value [in string format]
        chg_cmd_to_hex = True  # if char, change to True
        list_of_data = []
        zfill_list = []

        get_count_byte = RequestByte(command_val=command_value, char_cond=chg_cmd_to_hex,
                                     data_val_list=list_of_data, data_zfill_list=zfill_list).out_bytearray()

        # Check if open/not open
        if not self.ng_serial.isOpen():
            port_open = self.open_port()
        else:
            port_open = True

        # Write request/Read respond
        if port_open:
            self.ng_serial.write(get_count_byte)
            read_byte = self.ng_serial.read(9)

            if read_byte == b'[F\x00F]':
                get_count_status = False
            else:
                get_count_status = True

            # Check byte
            check_byte = CheckResponse(response_byte=read_byte, cmd_str='N').check_data_chksum_cmdstr()

            if not check_byte:
                get_count_status = False

            # If response byte OK, compare with file ng amount
            if get_count_status:
                dict_val = GetData(response_byte=read_byte, data_pos=['ng_count']).split_data()
                ng_box_val = int(dict_val['ng_count'])
                print("COMPARE VALUE: PC EXPECTATION:", self.ng_count, "NG BOX:", ng_box_val)
                if ng_box_val != self.ng_count:
                    get_count_status = False

        else:
            get_count_status = False

        return get_count_status

    def add_one(self):
        """
        Add one ng sample count to Master PC counter
        """
        self.ng_count = self.ng_count + 1
        self.df_count['counter'][0] = self.ng_count
        self.df_count.to_csv(self.ng_file_path, index=False)

    def add_total_item(self, total_item):
        """
        Add total item ng sample count to Master PC counter
        """
        print("BEFORE ADD", self.ng_count)
        self.ng_count = self.ng_count + total_item
        self.df_count['counter'][0] = self.ng_count
        self.df_count.to_csv(self.ng_file_path, index=False)


class RequestByte:
    """
    Class to create a request byte. Use out_bytearray to get the bytearray for transmitting request.
    """
    def __init__(self, command_val, char_cond: bool, data_val_list: list, data_zfill_list: list):
        """
        command_val: either string or hex value(string format, example '1' or '3e') indicating command type

        char_cond: if command_val is string, put True (bool). Else, False.

        data_val_list: The value to put in the Data field.
         For example if 1st data=12 and 2nd data=456, put in [12, 456]

        data_zfill_list: put the amount of integer in for each data field.
        For example if 1st data=0012 and 2nd data=456, put in [4,3]
        """
        self.command_val = command_val
        self.char_cond = char_cond
        self.data_val_list = data_val_list
        self.data_zfill_list = data_zfill_list

    @staticmethod
    def convert_str_to_dec(str_to_convert):
        """
        Convert string to decimal
        :param str_to_convert: string
        :return: decimal value of string
        """
        return ord(str_to_convert)

    @staticmethod
    def calculate_checksum(dec_list):
        """
        Calculate checksum from list
        :param dec_list: list containing decimals
        :return: checksum value in decimal
        """
        new_dec_list = dec_list.copy()
        new_dec_list.pop(0)
        new_dec_list.pop(-1)
        xor_val = 0

        for each_item in new_dec_list:
            xor_val = xor_val ^ each_item

        return xor_val

    def get_hex(self, cmd_hex: str, list_data: list, char_cond: bool):
        """
        Return list of decimals based on command value and data list
        :param cmd_hex: string containing the char or hex value of command
        :param list_data: list containing the data and amount of zero fill
        :param char_cond: Indicate whether cmd_hex is char or hex. True if char
        :return: list of decimals
        """
        # Convert command to decimal
        if char_cond:
            cmd_hex = format(ord(cmd_hex), "x")
            cmd_hex = cmd_hex.zfill(2)
            command_dec = int(f'0x{cmd_hex}', 0)
        else:
            command_dec = int(cmd_hex, 0)

        start_dec = [self.convert_str_to_dec('[')]
        end_dec = [self.convert_str_to_dec(']')]

        # Get total length of data and data in decimal
        sum_data_len = 0
        data_dec = []
        for each_item in list_data:
            item_len = len(each_item)
            sum_data_len += item_len

            item_dec = [self.convert_str_to_dec(i) for i in each_item]
            data_dec.extend(item_dec)
        # Convert data_len to hex
        data_len_hex = hex(sum_data_len)
        # Ensure correct conversion of dec
        sum_data_int = int(f'{data_len_hex}', 16)

        # Create entire hex
        total_command_transmit = []
        total_command_transmit.extend(start_dec + [command_dec] + [sum_data_int] + data_dec + end_dec)

        # Calculate checksum and put into entire hex
        checksum_dec = self.calculate_checksum(total_command_transmit)
        total_command_transmit.insert(-1, checksum_dec)

        return total_command_transmit

    @staticmethod
    def combine_data_zfill(data_val, data_zfill):
        """
        Combine data value list and zfill criteria list into one list
        :param data_val: data value list
        :param data_zfill: zfill criteria list
        :return: list containing both data value and zfill
        """
        data_output = []
        for each_data_elem, each_zfill_elem in zip(data_val, data_zfill):
            data_one_str = str(each_data_elem).zfill(each_zfill_elem)
            data_output.append(data_one_str)
        return data_output

    def out_bytearray(self):
        """
        Return the resulting request array in bytearray
        """
        data_total = self.combine_data_zfill(self.data_val_list, self.data_zfill_list)
        return bytearray(self.get_hex(self.command_val, data_total, self.char_cond))

    def out_hex_string(self):
        """
        Return the resulting request array in hex with 0x00 format (string type)
        """
        data_total = self.combine_data_zfill(self.data_val_list, self.data_zfill_list)
        total_dec_list = self.get_hex(self.command_val, data_total, self.char_cond)
        start_str = ''
        byte_string = ["0x{:02x}".format(i) for i in total_dec_list]
        byte_string = start_str.join(byte_string)
        return byte_string


class CheckResponse:
    """
    Check data length and checksum value. The methods below returns a boolean
    indicating if the bytes passed the test:
    check_data_chksum: data length, checksum
    check_data_chksum_cmd: data length, checksum, cmd (put in the expected hex value for cmd in cmd_hex)
    check_data_chksum_cmdstr: data length, checksum, cmd (put in the expected cmd string in cmd_str)
    """

    def __init__(self, response_byte, cmd_hex='0x00', cmd_str=''):
        self.response_byte = response_byte
        self.response_dec = self.byte_to_decimal(self.response_byte)
        self.cmd_hex = cmd_hex
        self.cmd_str = cmd_str

    @staticmethod
    def byte_to_decimal(byte_array):
        int_list = [int(i) for i in byte_array]
        return int_list

    def checksum(self):
        chk_comparison = self.response_dec[-2]

        # Gather the data for checksum
        checksum_data = self.response_dec.copy()

        del checksum_data[0]
        del checksum_data[-2:]

        # Calculate data checksum
        xor_val = 0
        for each_item in checksum_data:
            xor_val = xor_val ^ each_item

        if chk_comparison == xor_val:
            return True
        else:
            return False

    def check_data_length(self):
        length_comparison = hex(self.response_byte[2])
        length_comparison = int(length_comparison.replace('0x', ''))

        # Get data only
        data_total = self.response_dec.copy()

        del data_total[:3]
        del data_total[-2:]

        length_data = len(data_total)

        if length_comparison == length_data:
            return True
        else:
            return False

    def check_cmd(self):
        # Obtain cmd from response byte
        cmd_hex = int(self.cmd_hex, 0)
        cmd_compare = hex(self.response_byte[1])
        cmd_compare = int(cmd_compare, 0)

        if cmd_compare == cmd_hex:
            return True
        else:
            return False

    def check_cmd_str(self):
        # Obtain cmd from response byte
        cmd_compare = chr(self.response_byte[1])
        if cmd_compare == self.cmd_str:
            return True
        else:
            return False

    def check_data_chksum(self):
        if self.checksum() and self.check_data_length():
            return True
        else:
            return False

    def check_data_chksum_cmd(self):
        print("checksum:", self.checksum(), "data length:", self.check_data_length(), "check cmd:", self.check_cmd())
        if self.checksum() and self.check_data_length() and self.check_cmd():
            return True
        else:
            return False

    def check_data_chksum_cmdstr(self):
        if self.checksum() and self.check_data_length() and self.check_cmd_str():
            return True
        else:
            return False


class GetData:
    """
    Extract data from bytes. Use the split_data function to obtain the dictionary of result.
    In data_pos, give list explaining the data type:
    'true_false': 1 integer
    'buzzer_high': 4 integer
    'buzzer_low': 4 integer
    'current_val': 5 integer (3 before dec, 2 after dec)
    'volt_val': 2 integer
    'led_val': 1 integer
    'fault_buzzer': 1 integer
    'ng_count': 4 integer

    For example, if response byte data has Volt value and Led value (according to list order),
    the data_pos input should be ['volt_val', 'led_val']

    Get the data value in dict format using function split_data()
    """

    def __init__(self, response_byte, data_pos: list):
        if type(response_byte) == bytes:
            response_byte = bytearray(response_byte)
        self.response_byte = response_byte
        del self.response_byte[:3]
        del self.response_byte[-2:]

        self.data_pos = data_pos

    def split_data(self):
        data_int_list = {'true_false': 1, 'buzzer_high': 4,
                         'buzzer_low': 4, 'current_val': 5,
                         'volt_val': 2, 'led_val': 1, 'fault_buzzer': 1, 'ng_count': 4}

        split_data_dict = {}
        for each_data_type in self.data_pos:
            int_length = data_int_list[each_data_type]

            data_byte = self.response_byte[:int_length]

            data_value = self.get_data_val(data_byte.decode('ascii'), each_data_type)

            del self.response_byte[:int_length]
            split_data_dict[each_data_type] = data_value

        return split_data_dict

    @staticmethod
    def get_data_val(str_val, data_type):
        if data_type in ['buzzer_high', 'buzzer_low', 'volt_val', 'led_val', 'ng_count']:
            numeric_filter = filter(str.isdigit, str_val)
            numeric_str = "".join(numeric_filter)
            if numeric_str:
                return int(str_val)
            else:
                return 0

        elif data_type == 'true_false':
            print(str_val)
            if str_val == '1':
                return True
            else:
                return False

        elif data_type in ['led_val', 'fault_buzzer']:
            if int(str_val) == 0:
                return 'Low'
            else:
                return 'High'

        # For  Current
        else:
            # before_dec = str_val[:3]
            # after_dec = str_val[-2:]

            # formatted_current = f'{before_dec}.{after_dec}'
            dec_num = str_val

            formatted_current = f'{dec_num}'

            return int(formatted_current)


class SoundLevelDevice:

    def __init__(self, port, baudrate, timeout, sound_serial):
        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        self.sound_serial = sound_serial

    @ staticmethod
    def _check_buffer(serial_port):
        buffer_list = []
        timeout_start = time.time()
        while time.time() < timeout_start + 0.5:
            buffer_list.append(str(serial_port.in_waiting))
        match = all(buffer == buffer_list[0] for buffer in buffer_list)
        if match:
            status = 0  # No data receive
        else:
            status = 1  # Data received

        return status

    def _turn_on_setup_button(self, serial_port):
        button_status = self._check_buffer(serial_port)
        start_attempt = time.time()
        device_status = False
        while time.time() < start_attempt + 3:
            if button_status == 0:
                serial_port.reset_output_buffer()
                serial_port.write(bytes.fromhex('AC'))  # Turn setup button ON
                button_status = self._check_buffer(serial_port)
                if button_status == 1:
                    device_status = True
                    break
            else:
                device_status = True
                break

        return device_status

    @ staticmethod
    def _read_sound_device(serial_port):

        serial_string = serial_port.read(30)  # default 6
        serial_string_hex = serial_string.hex()
        data = re.search('a50d(.*)a50b', serial_string_hex)

        if data:
            dba_value = data.group(1)
            first_string = dba_value[0]
            try:
                if first_string == '0':
                    new_dba_value = float(dba_value[1:3] + '.' + dba_value[3:])
                else:
                    new_dba_value = float(dba_value[:3] + '.' + dba_value[3:])
            except ValueError:
                new_dba_value = False
        else:
            new_dba_value = False

        return new_dba_value

    def get_db_value(self):
        """
        Blueprint to get dB value from sound level device
        """

        # try:
        #     with Serial(self.port, int(self.baudrate), bytesize=8, stopbits=1, timeout=int(self.timeout)) as ser:
        #         # Check and turn on SETUP button
        #         device_status = self._turn_on_setup_button(ser)
        #
        #         if device_status:
        #             db_value_list = []
        #             while len(db_value_list) < 2:
        #                 if ser.in_waiting > 0:
        #                     db_value = self._read_sound_device(ser)
        #                     if db_value:
        #                         db_value_list.append(db_value)
        #             db_value = max(db_value_list)
        #         else:
        #             db_value = False
        # except SerialException:
        #     db_value = False
        if self.sound_serial:
            if not self.sound_serial.isOpen():
                try:
                    self.sound_serial.open()
                    sound_serial_status = True
                except SerialException:
                    sound_serial_status = False
            else:
                sound_serial_status = True
        else:
            sound_serial_status = False

        if sound_serial_status:
            # Check and turn on SETUP button
            device_status = self._turn_on_setup_button(self.sound_serial)

            if device_status:
                db_value_list = []
                while len(db_value_list) < 10:
                    if self.sound_serial.in_waiting > 0:
                        db_value = self._read_sound_device(self.sound_serial)
                        if db_value:
                            db_value_list.append(db_value)
                db_value = max(db_value_list)
                print('SOUND SERIAL DEVICE STATUS OK:', db_value_list)
            else:
                print('SOUND SERIAL DEVICE STATUS NOK')
                db_value = False
        else:
            print('SOUND SERIAL EXCEPTION OCCUR OR SOUND LEVEL NOT USED')
            db_value = False

        return db_value


class ProgrammablePowerSupply:
    """
    Class for interfacing with Programmable Power Supply
    """
    def __init__(self):
        self.port = ""
        self.baudrate = 0
        self.timeout = 0.0
        self.dev_serial = None

    def set_port(self, com_port, baudrate, timeout):
        self.port = com_port
        self.baudrate = baudrate
        self.timeout = timeout

        self.dev_serial = Serial(port=com_port,
                                 baudrate=baudrate,
                                 parity=serial.PARITY_NONE,
                                 stopbits=serial.STOPBITS_ONE,
                                 timeout=timeout)

    def close_port(self):
        self.dev_serial.close()

    def _send_command(self, command):
        self.dev_serial.write(command.encode('ascii'))
        # time.sleep(0.2)

    def _read_output(self):
        """
        Read serial output as a string
        """
        out = ""
        while self.dev_serial.inWaiting() > 0:
            out += self.dev_serial.read(1).decode('ascii')

        return out

    def _read_bytes(self):
        """
        Read serial output as a bytes
        """
        out = []
        while self.dev_serial.inWaiting() > 0:
            bytes_response = self.dev_serial.read(1)
            print(bytes_response)
            out.append(ord(bytes_response))

        return out

    def set_output_current(self, channel, milli_ampere):
        command = "ISET{channel}:{amperes:.3f}"

        ampere = float(milli_ampere) / 1000.0
        command = command.format(channel=channel, amperes=ampere)
        print(command)
        self._send_command(command)
        read_current = self.get_output_current_settings(channel)
        return read_current

    def get_output_current_settings(self, channel):
        command = "ISET{channel}?".format(channel=channel)
        print(command)
        self._send_command(command)
        return float(self._read_output())

    def set_output_voltage(self, channel, milli_volt):
        command = "VSET{channel}:{volt:.3f}"

        volt = float(milli_volt) / 1000.0
        command = command.format(channel=channel, volt=volt)
        print(command)
        self._send_command(command)
        read_current = self.get_output_voltage_settings(channel)
        return read_current

    def get_output_voltage_settings(self, channel):
        command = "VSET{channel}?".format(channel=channel)
        print(command)
        self._send_command(command)
        read_volt = self._read_output()
        return read_volt

    def get_actual_output_current(self, channel):
        command = "IOUT{channel}?".format(channel=channel)
        print(command)
        self._send_command(command)
        read_current = self._read_output()
        return read_current

    def get_actual_output_voltage(self, channel):
        command = "VOUT{channel}?".format(channel=channel)
        print(command)
        self._send_command(command)
        read_volt = self._read_output()
        return read_volt

    def turn_on_output(self):
        command = "OUT1"
        print(command)
        self._send_command(command)

    def turn_off_output(self):
        command = "OUT0"
        print(command)
        self._send_command(command)

    def get_status(self):
        """
        Returns the power supply status as a dictionary of values

        * ch1Mode: "C.V | C.C"
        * ch2Mode: "C.V | C.C"
        * tracking:
            -> 00=Independent
            -> 01=Tracking series
            -> 11=Tracking parallel
        * BeepEnabled: True | False
        * lockEnabled: True | False
        * outEnabled: True | False
        """
        command = "STATUS?"
        print(command)
        self._send_command(command)
        status_bytes = self._read_bytes()

        status = status_bytes[0]

        ch1mode = (status & 0x01)
        ch2mode = (status & 0x02)
        tracking = (status & 0x0C) >> 2
        beep = (status & 0x10)
        lock = (status & 0x20)
        out = (status & 0x40)

        if tracking == 0:
            tracking = "Independent"
        elif tracking == 1:
            tracking = "Tracking Series"
        elif tracking == 3:
            tracking = "Tracking Parallel"
        else:
            tracking = "Unknown"

        return_dict = {"ch1Mode": "C.V" if ch1mode else "C.C",
                       "ch2Mode": "C.V" if ch2mode else "C.C",
                       "Tracking": tracking,
                       "BeepEnabled": bool(beep),
                       "lockEnabled": bool(lock),
                       "outEnabled": bool(out)
                       }

        return return_dict

    def get_version(self):
        """
        Returns a single string with the version of the Tenma Device and Protocol user
        """
        command = "*IDN?"
        print(command)
        self._send_command(command)
        return self._read_output()

    def recall_settings(self, memory_number):
        """
        Load existing configuration in Memory. Same as pressing any Mx button on the unit
        """
        command = "RCL{conf}".format(conf=memory_number)
        print(command)
        self._send_command(command)

    def save_settings(self, memory_number):
        """
        Save existing configuration in Memory. Same as pressing any Mx button on the unit
        """
        command = "SAV{conf}".format(conf=memory_number)
        print(command)
        self._send_command(command)

    def set_ocp(self, enable=True):
        """
        Enable or disable OCP

        There's no feedback from the serial connection to determine
        whether OCP was set or not.

        :param enable: Boolean to enable or disable
        """
        conf = 1 if enable else 0
        command = "OCP{conf}".format(conf=conf)
        print(command)
        self._send_command(command)

    def set_lock(self, lock=True):
        """
        Lock or unlock

        There's no feedback from the serial connection to determine
        whether it was lock or unlock.

        :param lock: Boolean to enable or disable
        """
        conf = 1 if lock else 0
        command = "LOCK{conf}".format(conf=conf)
        print(command)
        self._send_command(command)


def mcu_only(test_no, mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
             mcu_delay, mcu_resp_len, data_pos: list, sound_test, sound_serial, sound_port, sound_baud_rate,
             sound_timeout,
             con_test):
    """
    Blueprint to run test requiring MCU response only
    test_no: the current test number
    mcu_port: MCU COM Port
    mcu_baud_rate: MCU Baud Rate
    timeout_set: Timeout for MCU communication
    mcu_delay: Start delay when opening MCU COM port
    mcu_resp_len: Expected byte amount for MCU
    data_pos: Expected data postion list for MCU. For example: ['volt_val', 'led_val']
    sound_test: Bool True if need to test sound level, else False
    sound_port: Sound level device COM port
    sound_baud_rate: Sound level device Baud Rate
    sound_timeout: Timeout for sound level device communication
    run_type: 0:Mandatory; 1:Optional
    con_test: The condition to continue test depends on mandatory/optional item result
    """
    run_stat = True  # The condition to check if the entire process was run without issues
    db_value = False  # Variable for dB value readings
    print("\n" + mcu_cmd)
    print("TEST NO:", test_no)
    print("CON TEST:", con_test)

    if con_test:
        # Extract MCU start false cmd
        mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                           data_zfill_list=[]).out_bytearray()

        # MCU Request
        mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                                   data_val_list=[], data_zfill_list=[]).out_bytearray()
        print("REQUEST", mcu_req_byte.hex())
        # MCU communication
        try:
            with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                # time.sleep(float(mcu_delay))

                # Send MCU request
                ser.write(mcu_req_byte)

                # Test dB level
                if sound_test:
                    db_value = SoundLevelDevice(sound_port, sound_baud_rate, sound_timeout, sound_serial).get_db_value()
                else:
                    db_value = False

                # Get MCU respond
                mcu_respond = ser.read(int(mcu_resp_len))
        except SerialException:
            db_value = False
            mcu_respond = False
        print("DB LEVEL", db_value)
        if mcu_respond:
            print("RESPOND", mcu_respond.hex())
        else:
            print("RESPOND", mcu_respond)

        # If no respond from MCU, request for N times
        if not mcu_respond:
            i = 0
            while i < int(mcu_repetition_request):
                print("RE-REQUEST FOR " + str(i + 1) + " TIMES")
                print("REQUEST", mcu_req_byte.hex())
                # MCU communication
                try:
                    with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                        # time.sleep(float(mcu_delay))

                        # Send MCU request
                        ser.write(mcu_req_byte)

                        # Test dB level
                        if sound_test:
                            db_value = SoundLevelDevice(sound_port, sound_baud_rate, sound_timeout, sound_serial).get_db_value()
                        else:
                            db_value = False

                        # Get MCU respond
                        mcu_respond = ser.read(int(mcu_resp_len))
                except SerialException:
                    db_value = False
                    mcu_respond = False
                print("DB LEVEL", db_value)
                if mcu_respond:
                    print("RESPOND", mcu_respond.hex())
                else:
                    print("RESPOND", mcu_respond)
                if mcu_respond:
                    break
                else:
                    i += 1

            if not mcu_respond:
                mcu_respond = mcu_start_false_byte

        # If MCU returns False Start, to stop the rest of the test
        if mcu_respond == mcu_start_false_byte:
            run_stat = False
            result_list = [test_no, False]
        else:
            check_response = CheckResponse(response_byte=mcu_respond,
                                           cmd_hex=mcu_cmd).check_data_chksum_cmd()
            # If MCU response contains errors, to stop the rest of the test
            if check_response:
                # Append result if no errors in the response
                dict_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()

                result_list = [test_no]
                for i in data_pos:
                    test_value = dict_val[i]
                    result_list.append(test_value)
            else:
                run_stat = False
                result_list = [test_no, False]
    else:
        result_list = [test_no, False]

    return_list = [run_stat, result_list, db_value, con_test]

    return return_list


def mcu_motor(test_no, mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
              mcu_delay, mcu_resp_len, data_pos, x_speed, x_dist, y_speed, y_dist, motor_timeout, motor_ser,
              sound_test, sound_serial, sound_port, sound_baud_rate, sound_timeout, con_test, sensor_enabled, sensor_enable_cmd,
              sensor_enable_timeout, sensor_enable_resp_len):
    """
    Blueprint to run test requiring Motor movement and MCU response
    test_no: the current test number
    mcu_cmd: MCU command in hex
    mcu_port: MCU COM Port
    mcu_baud_rate: MCU Baud Rate
    mcu_start_false_cmd: MCU Start False command in hex
    timeout_set: Timeout for MCU communication
    mcu_delay: Start delay when opening MCU COM port
    mcu_resp_len: Expected byte amount for MCU
    data_pos: Expected data position list for MCU. For example: ['volt_val', 'led_val']
    x_speed: Speed for motor x
    x_dist: Distance for motor x
    y_speed: Speed for motor y
    y_dist: Distance for motor y
    motor_timeout: Timeout for Motor communication
    motor_ser: Opened COM Port for motor unit
    sound_test: Bool True if need to test sound level, else False
    sound_port: Sound level device COM port
    sound_baud_rate: Sound level device Baud Rate
    sound_timeout: Timeout for sound level device communication
    run_type: 0:Mandatory; 1:Optional
    con_test: The condition to continue test depends on mandatory/optional item result
    sensor_enabled: The condition to check if sensor has been enabled or disabled
    sensor_enable_cmd: MCU command in hex
    sensor_enable_timeout: Timeout for MCU communication
    sensor_enable_resp_len: Expected byte amount for MCU
    """
    run_stat = True
    sensor_enable_respond = False
    x_speed = int(x_speed)
    x_dist = int(x_dist)
    y_speed = int(y_speed)
    y_dist = int(y_dist)

    print("\n" + mcu_cmd)
    print("TEST NO:", test_no)
    print("CON TEST:", con_test)

    if con_test:
        # Extract MCU start false cmd
        mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                           data_zfill_list=[]).out_bytearray()

        # MCU request
        enable_sensor_req_byte = RequestByte(command_val=f'{sensor_enable_cmd}', char_cond=False, data_val_list=[],
                                             data_zfill_list=[]).out_bytearray()
        mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                                   data_val_list=[], data_zfill_list=[]).out_bytearray()

        # Motor request
        motor_req_byte = RequestByte(command_val='M', char_cond=True, data_val_list=[x_speed, x_dist, y_speed, y_dist],
                                     data_zfill_list=[3, 4, 3, 4]).out_bytearray()

        """ Move motor """
        print("x-speed:", str(x_speed))
        print("x-distance:", str(x_dist))
        print("y-speed:", str(y_speed))
        print("y_distance:", str(y_dist))
        print("MOTOR REQUEST:", motor_req_byte.hex())
        if motor_ser:
            motor_ser.timeout = int(motor_timeout)
            motor_ser.write(motor_req_byte)
            motor_ser.reset_input_buffer()
            serial_string = motor_ser.read(5)
            serial_string_hex = serial_string.hex()
            print("MOTOR RESPONSE", serial_string_hex)

            if serial_string == b'[T\x00T]':
                print("CONFIRM START SIGNAL: OK")
                motor_response = motor_ser.read(5)
                # Confirm End
                if motor_response == b'[E\x00E]':
                    print("CONFIRM END SIGNAL RECEIVED:", motor_response.hex())
                    motor_respond = True
                # Waiting for confirm end timed out
                else:
                    print("CONFIRM END SIGNAL NOT RECEIVED", motor_response.hex())
                    motor_respond = False
            elif serial_string == b'[F\x00F]':
                print("CONFIRM START SIGNAL: NOK")
                motor_respond = False
            else:
                print("CONFIRM START SIGNAL NOT RECEIVED", serial_string.hex())
                motor_respond = False
        else:
            motor_respond = True

        if motor_respond:
            if not sensor_enabled:
                """ Enabling sensor """
                try:
                    with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(sensor_enable_timeout)) as \
                            sensor_ser:
                        # time.sleep(float(mcu_delay))
                        # Send MCU request
                        print("SENSOR ENABLE REQUEST", enable_sensor_req_byte.hex())
                        sensor_ser.write(enable_sensor_req_byte)
                        # Get MCU respond
                        sensor_enable_respond = sensor_ser.read(int(sensor_enable_resp_len))
                except SerialException:
                    sensor_enable_respond = False
                if sensor_enable_respond:
                    print("SENSOR ENABLE RESPOND", sensor_enable_respond.hex())
                else:
                    print("SENSOR ENABLE RESPOND", sensor_enable_respond)

                # Retrying request
                if not sensor_enable_respond:
                    i = 0
                    while i < int(mcu_repetition_request):
                        print("SENSOR ENABLE RE-REQUEST FOR " + str(i + 1) + " TIMES")
                        print("SENSOR ENABLE REQUEST", enable_sensor_req_byte.hex())
                        try:
                            with Serial(mcu_port, int(mcu_baud_rate), bytesize=8,
                                        timeout=float(sensor_enable_timeout)) as sensor_ser:
                                # time.sleep(float(mcu_delay))
                                # Send MCU request
                                sensor_ser.write(enable_sensor_req_byte)
                                # Get MCU respond
                                sensor_enable_respond = sensor_ser.read(int(sensor_enable_resp_len))
                        except SerialException:
                            sensor_enable_respond = False
                        if sensor_enable_respond:
                            print("SENSOR ENABLE RESPOND", sensor_enable_respond.hex())
                            break
                        else:
                            print("SENSOR ENABLE RESPOND", sensor_enable_respond)
                            i += 1

                    if sensor_enable_respond:
                        if sensor_enable_respond == mcu_start_false_byte:
                            sensor_enable_respond = False
                        else:
                            pass
                    else:
                        sensor_enable_respond = False
                else:
                    pass

                # If MCU returns False Start, to stop the rest of the test
                if sensor_enable_respond:
                    check_response = CheckResponse(response_byte=sensor_enable_respond,
                                                   cmd_hex=sensor_enable_cmd).check_data_chksum_cmd()
                    # If MCU response contains errors, to stop the rest of the test
                    if check_response:
                        # Append result if no errors in the response
                        data_val = GetData(response_byte=sensor_enable_respond, data_pos=['true_false']).split_data()
                        if data_val.get('true_false'):
                            sensor_enable_respond = True
                            sensor_enabled = True
                        else:
                            sensor_enable_respond = False
                    else:
                        sensor_enable_respond = False
                else:
                    sensor_enable_respond = False

                if sensor_enable_respond:
                    """ Send test item """
                    try:
                        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                            # time.sleep(float(mcu_delay))
                            print('MCU REQUEST', mcu_req_byte.hex())
                            # Send MCU request
                            ser.write(mcu_req_byte)

                            # Test dB level
                            if sound_test:
                                db_value = SoundLevelDevice(sound_port, sound_baud_rate, sound_timeout, sound_serial).get_db_value()
                            else:
                                db_value = False

                            # Get MCU respond
                            mcu_respond = ser.read(int(mcu_resp_len))
                    except SerialException:
                        db_value = False
                        mcu_respond = False
                    print("DB VALUE", db_value)
                    if mcu_respond:
                        print("MCU RESPOND", mcu_respond.hex())
                    else:
                        print("MCU RESPOND", mcu_respond)

                    # Retrying request
                    if not mcu_respond:
                        i = 0
                        while i < int(mcu_repetition_request):
                            print("RE-REQUEST FOR " + str(i + 1) + " TIMES")
                            print("MCU REQUEST", mcu_req_byte.hex())
                            # MCU communication
                            try:
                                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8,
                                            timeout=float(timeout_set)) as ser:
                                    # time.sleep(float(mcu_delay))

                                    # Send MCU request
                                    ser.write(mcu_req_byte)

                                    # Test dB level
                                    if sound_test:
                                        db_value = SoundLevelDevice(sound_port, sound_baud_rate,
                                                                    sound_timeout, sound_serial).get_db_value()
                                    else:
                                        db_value = False

                                    # Get MCU respond
                                    mcu_respond = ser.read(int(mcu_resp_len))
                            except SerialException:
                                db_value = False
                                mcu_respond = False
                            print("DB VALUE", db_value)
                            if mcu_respond:
                                print("MCU RESPOND", mcu_respond.hex())
                            else:
                                print("MCU RESPOND", mcu_respond)
                            if mcu_respond:
                                break
                            else:
                                i += 1

                        if mcu_respond:
                            if mcu_respond == mcu_start_false_byte:
                                mcu_respond = False
                            else:
                                pass
                        else:
                            mcu_respond = False

                    if mcu_respond:
                        # If MCU returns False Start, to stop the rest of the test
                        check_response = CheckResponse(response_byte=mcu_respond,
                                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
                        print("CHECK RESPONSE", check_response)
                        # If MCU response contains errors, to stop the rest of the test
                        if check_response:
                            # Append result if no errors in the response
                            dict_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()

                            result_list = [test_no]
                            for i in data_pos:
                                test_value = dict_val[i]
                                result_list.append(test_value)
                        else:
                            run_stat = False
                            result_list = [test_no, False]
                            db_value = False
                    else:
                        run_stat = False
                        result_list = [test_no, False]
                        db_value = False

                else:
                    run_stat = False
                    result_list = [test_no, False]
                    db_value = False
            else:
                """ Send test item """
                try:
                    with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                        # time.sleep(float(mcu_delay))

                        # Send MCU request
                        ser.write(mcu_req_byte)

                        # Test dB level
                        if sound_test:
                            db_value = SoundLevelDevice(sound_port, sound_baud_rate, sound_timeout, sound_serial).get_db_value()
                        else:
                            db_value = False

                        # Get MCU respond
                        mcu_respond = ser.read(int(mcu_resp_len))
                except SerialException:
                    db_value = False
                    mcu_respond = False
                print("DB VALUE", db_value)
                if mcu_respond:
                    print("MCU RESPOND", mcu_respond.hex())
                else:
                    print("MCU RESPOND", mcu_respond)

                # Retrying request
                if not mcu_respond:
                    i = 0
                    while i < int(mcu_repetition_request):
                        print("RE-REQUEST FOR " + str(i + 1) + " TIMES")
                        print("MCU REQUEST", mcu_req_byte.hex())
                        # MCU communication
                        try:
                            with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                                # time.sleep(float(mcu_delay))

                                # Send MCU request
                                ser.write(mcu_req_byte)

                                # Test dB level
                                if sound_test:
                                    db_value = SoundLevelDevice(sound_port, sound_baud_rate,
                                                                sound_timeout, sound_serial).get_db_value()
                                else:
                                    db_value = False

                                # Get MCU respond
                                mcu_respond = ser.read(int(mcu_resp_len))
                        except SerialException:
                            db_value = False
                            mcu_respond = False
                        print("DB VALUE", db_value)
                        if mcu_respond:
                            print("MCU RESPOND", mcu_respond.hex())
                        else:
                            print("MCU RESPOND", mcu_respond)
                        if mcu_respond:
                            break
                        else:
                            i += 1

                    if mcu_respond:
                        if mcu_respond == mcu_start_false_byte:
                            mcu_respond = False
                        else:
                            pass
                    else:
                        mcu_respond = False

                if mcu_respond:
                    # If MCU returns False Start, to stop the rest of the test
                    check_response = CheckResponse(response_byte=mcu_respond, cmd_hex=mcu_cmd).check_data_chksum_cmd()
                    print("CHECK RESPONSE", check_response)
                    # If MCU response contains errors, to stop the rest of the test
                    if check_response:
                        # Append result if no errors in the response
                        dict_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()

                        result_list = [test_no]
                        for i in data_pos:
                            test_value = dict_val[i]
                            result_list.append(test_value)
                    else:
                        run_stat = False
                        result_list = [test_no, False]
                        db_value = False
                else:
                    run_stat = False
                    result_list = [test_no, False]
                    db_value = False
        else:
            run_stat = True
            result_list = [test_no, False]
            db_value = False
    else:
        run_stat = True
        result_list = [test_no, False]
        db_value = False

    print("RUN STAT", run_stat)
    print("RESULT LIST", result_list)
    print("DB VALUE", db_value)

    return_list = [run_stat, result_list, db_value, con_test, sensor_enabled]

    return return_list


def mcu_start_operation_cmd(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
                            mcu_delay, mcu_resp_len, data_pos):

    # Extract MCU start false cmd
    mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                       data_zfill_list=[]).out_bytearray()

    mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                               data_val_list=[], data_zfill_list=[]).out_bytearray()

    print("\nSTART OPERATION REQUEST:", mcu_req_byte.hex())
    # MCU communication
    try:
        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
            # time.sleep(float(mcu_delay))

            # Send MCU request
            ser.write(mcu_req_byte)

            # Get MCU respond
            mcu_respond = ser.read(int(mcu_resp_len))
    except SerialException:
        mcu_respond = False

    if mcu_respond:
        print("RESPOND", mcu_respond.hex())
    else:
        print("RESPOND", mcu_respond)

    # If no respond from MCU, request for N times
    if not mcu_respond:
        i = 0
        while i < int(mcu_repetition_request):
            print("RE-REQUEST FOR " + str(i + 1) + " TIMES")
            print("REQUEST", mcu_req_byte.hex())
            # MCU communication
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                    # time.sleep(float(mcu_delay))

                    # Send MCU request
                    ser.write(mcu_req_byte)

                    # Get MCU respond
                    mcu_respond = ser.read(int(mcu_resp_len))
            except SerialException:
                mcu_respond = False

            if mcu_respond:
                print("RESPOND", mcu_respond.hex())
            else:
                print("RESPOND", mcu_respond)

            if mcu_respond:
                break
            else:
                i += 1

        if not mcu_respond:
            mcu_respond = mcu_start_false_byte

    if mcu_respond == mcu_start_false_byte:
        mcu_respond = False
    else:
        check_response = CheckResponse(response_byte=mcu_respond,
                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
        # If MCU response contains errors, to stop the rest of the test
        if check_response:
            # Append result if no errors in the response
            data_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()
            if data_val.get('true_false'):
                print("START OPERATION: OK")
                mcu_respond = True
            else:
                print("START OPERATION: NOK")
                mcu_respond = False
        else:
            print("START OPERATION: WRONG RESPONSE")
            mcu_respond = False

    return mcu_respond


def mcu_end_operation_cmd(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
                          mcu_resp_len, end_error, data_pos):

    # Extract MCU start false cmd
    mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                       data_zfill_list=[]).out_bytearray()

    if end_error:
        mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                                   data_val_list=[0], data_zfill_list=[1]).out_bytearray()
    else:
        mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                                   data_val_list=[1], data_zfill_list=[1]).out_bytearray()

    if end_error:
        print("\nEND OPERATION (ERROR) REQUEST:", mcu_req_byte.hex())
    else:
        print("\nEND OPERATION (NO ERROR) REQUEST:", mcu_req_byte.hex())

    # MCU communication
    try:
        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
            # time.sleep(float(mcu_delay))

            # Send MCU request
            ser.write(mcu_req_byte)

            # Get MCU respond
            mcu_respond = ser.read(int(mcu_resp_len))
    except SerialException:
        mcu_respond = False

    if mcu_respond:
        print("RESPOND", mcu_respond.hex())
    else:
        print("RESPOND", mcu_respond)

    # If no respond from MCU, request for N times
    if not mcu_respond:
        i = 0
        while i < int(mcu_repetition_request):
            print("RE-REQUEST FOR " + str(i + 1) + " TIMES")
            print("REQUEST", mcu_req_byte.hex())
            # MCU communication
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                    # time.sleep(float(mcu_delay))

                    # Send MCU request
                    ser.write(mcu_req_byte)

                    # Get MCU respond
                    mcu_respond = ser.read(int(mcu_resp_len))
            except SerialException:
                mcu_respond = False

            if mcu_respond:
                print("RESPOND", mcu_respond.hex())
            else:
                print("RESPOND", mcu_respond)

            if mcu_respond:
                break
            else:
                i += 1

        if not mcu_respond:
            mcu_respond = mcu_start_false_byte

    if mcu_respond == mcu_start_false_byte:
        mcu_respond = False
    else:
        check_response = CheckResponse(response_byte=mcu_respond,
                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
        # If MCU response contains errors, to stop the rest of the test
        if check_response:
            # Append result if no errors in the response
            data_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()
            if data_val.get('true_false'):
                mcu_respond = True
            else:
                mcu_respond = False
        else:
            mcu_respond = False

    return mcu_respond


def mcu_dot_marking_cmd(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
                        mcu_delay, mcu_resp_len, data_pos):
    """
    Blueprint to run dot marking machine that requiring MCU response
    mcu_cmd: MCU command in hex
    mcu_port: MCU COM Port
    mcu_baud_rate: MCU Baud Rate
    mcu_start_false_cmd: MCU Start False command in hex
    mcu_repetition_request: Number of MCU repetition to send request in case there's no respond on initial request
    timeout_set: Timeout for PC<->MCU communication, also for MCU<->Dot Marking Machine communication
    mcu_delay: Start delay when opening MCU COM port
    mcu_resp_len: Expected byte amount for MCU
    data_pos: Expected data postion list for MCU. For example: ['volt_val', 'led_val']
    """
    # Extract MCU start false cmd
    mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                       data_zfill_list=[]).out_bytearray()
    print('TIMEOUT SET', timeout_set)
    mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                               data_val_list=[int(timeout_set)], data_zfill_list=[3]).out_bytearray()

    buffer_timeout = 0.5

    print("\nDOT MARKING REQUEST:", mcu_req_byte.hex())
    # MCU communication
    try:
        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set) + buffer_timeout) as ser:
            # time.sleep(float(mcu_delay))

            # Send MCU request
            ser.write(mcu_req_byte)

            # Get MCU respond
            mcu_respond = ser.read(int(mcu_resp_len))
    except SerialException:
        mcu_respond = False

    if mcu_respond:
        print("RESPOND", mcu_respond.hex())
    else:
        print("RESPOND", mcu_respond)

    # If no respond from MCU, request for N times
    if not mcu_respond:
        i = 0
        while i < int(mcu_repetition_request):
            print("RE-REQUEST FOR " + str(i + 1) + " TIMES")
            print("REQUEST", mcu_req_byte.hex())
            # MCU communication
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                    # time.sleep(float(mcu_delay))

                    # Send MCU request
                    ser.write(mcu_req_byte)

                    # Get MCU respond
                    mcu_respond = ser.read(int(mcu_resp_len))
            except SerialException:
                mcu_respond = False

            if mcu_respond:
                print("RESPOND", mcu_respond.hex())
            else:
                print("RESPOND", mcu_respond)

            if mcu_respond:
                break
            else:
                i += 1

        if not mcu_respond:
            mcu_respond = mcu_start_false_byte

    if mcu_respond == mcu_start_false_byte:
        mcu_respond = False
    else:
        check_response = CheckResponse(response_byte=mcu_respond,
                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
        # If MCU response contains errors, to stop the rest of the test
        if check_response:
            # Append result if no errors in the response
            data_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()
            if data_val.get('true_false'):
                mcu_respond = True
            else:
                mcu_respond = False
        else:
            mcu_respond = False

    return mcu_respond


def mcu_operational_button(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
                           mcu_resp_len, data_pos):
    # Extract MCU start false cmd
    mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                       data_zfill_list=[]).out_bytearray()

    mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                               data_val_list=[], data_zfill_list=[]).out_bytearray()

    # MCU communication
    try:
        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
            # Send MCU request
            ser.write(mcu_req_byte)

            # Get MCU respond
            mcu_respond = ser.read(int(mcu_resp_len))
    except SerialException:
        mcu_respond = False

    # If no respond from MCU, request for N times
    if not mcu_respond:
        i = 0
        while i < int(mcu_repetition_request):
            # MCU communication
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                    # Send MCU request
                    ser.write(mcu_req_byte)

                    # Get MCU respond
                    mcu_respond = ser.read(int(mcu_resp_len))
            except SerialException:
                mcu_respond = False

            if mcu_respond:
                break
            else:
                i += 1

        if not mcu_respond:
            mcu_respond = mcu_start_false_byte

    if mcu_respond == mcu_start_false_byte:
        mcu_respond = False
    else:
        check_response = CheckResponse(response_byte=mcu_respond,
                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
        # If MCU response contains errors, to stop the rest of the test
        if check_response:
            # Append result if no errors in the response
            data_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()
            if data_val.get('true_false'):
                mcu_respond = True
            else:
                mcu_respond = False
        else:
            mcu_respond = False

    return mcu_respond


def mcu_buzzer_fault_move_test_cmd(test_no, mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request,
                                   timeout_set, mcu_delay, mcu_resp_len, data_pos, x_speed, x_dist, y_speed, y_dist,
                                   motor_timeout, motor_ser, sound_test, sound_serial, sound_port, sound_baud_rate, sound_timeout,
                                   con_test):
    run_stat = True
    x_speed = int(x_speed)
    x_dist = int(x_dist)
    y_speed = int(y_speed)
    y_dist = int(y_dist)

    print("\n" + mcu_cmd)
    print("TEST NO:", test_no)
    print("CON TEST:", con_test)

    if con_test:
        # Extract MCU start false cmd
        mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                           data_zfill_list=[]).out_bytearray()

        # MCU request
        mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                                   data_val_list=[], data_zfill_list=[]).out_bytearray()

        # Motor request
        motor_req_byte = RequestByte(command_val='M', char_cond=True, data_val_list=[x_speed, x_dist, y_speed, y_dist],
                                     data_zfill_list=[3, 4, 3, 4]).out_bytearray()

        """
        MOVE MOTOR
        """
        if motor_ser:
            motor_ser.timeout = int(motor_timeout)
            motor_ser.write(motor_req_byte)
            motor_ser.reset_input_buffer()
            serial_string = motor_ser.read(5)
            print(serial_string)
            serial_string_hex = serial_string.hex()
            print("RESPONSE", serial_string_hex)

            if serial_string == b'[T\x00T]':
                print("CONFIRM START SIGNAL: OK")
                motor_response = motor_ser.read(5)
                # Confirm End
                if motor_response == b'[E\x00E]':
                    print("CONFIRM END SIGNAL RECEIVED:", motor_response.hex())
                    motor_respond = True
                # Waiting for confirm end timed out
                else:
                    print("CONFIRM END SIGNAL NOT RECEIVED", motor_response.hex())
                    motor_respond = False
            elif serial_string == b'[F\x00F]':
                print("CONFIRM START SIGNAL: NOK")
                motor_respond = False
            else:
                print("CONFIRM START SIGNAL NOT RECEIVED", serial_string.hex())
                motor_respond = False
        else:
            motor_respond = True

        if motor_respond:
            """ Send test item """
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                    # time.sleep(float(mcu_delay))

                    # Send MCU request
                    ser.write(mcu_req_byte)

                    # Test dB level
                    if sound_test:
                        db_value = SoundLevelDevice(sound_port, sound_baud_rate, sound_timeout, sound_serial).get_db_value()
                    else:
                        db_value = False

                    # Get MCU respond
                    mcu_respond = ser.read(int(mcu_resp_len))
            except SerialException:
                db_value = False
                mcu_respond = False
            print("DB VALUE", db_value)
            if mcu_respond:
                print("RESPOND", mcu_respond.hex())
            else:
                print("RESPOND", mcu_respond)

            # Retrying request
            if not mcu_respond:
                i = 0
                while i < int(mcu_repetition_request):
                    print("RE-REQUEST FOR " + str(i + 1) + " TIMES")
                    print("REQUEST", mcu_req_byte.hex())
                    # MCU communication
                    try:
                        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                            # time.sleep(float(mcu_delay))

                            # Send MCU request
                            ser.write(mcu_req_byte)

                            # Test dB level
                            if sound_test:
                                db_value = SoundLevelDevice(sound_port, sound_baud_rate,
                                                            sound_timeout, sound_serial).get_db_value()
                            else:
                                db_value = False

                            # Get MCU respond
                            mcu_respond = ser.read(int(mcu_resp_len))
                    except SerialException:
                        db_value = False
                        mcu_respond = False
                    print("DB VALUE", db_value)
                    if mcu_respond:
                        print("RESPOND", mcu_respond.hex())
                    else:
                        print("RESPOND", mcu_respond)
                    if mcu_respond:
                        break
                    else:
                        i += 1

                if mcu_respond:
                    if mcu_respond == mcu_start_false_byte:
                        mcu_respond = False
                    else:
                        pass
                else:
                    mcu_respond = False

            if mcu_respond:
                # If MCU returns False Start, to stop the rest of the test
                check_response = CheckResponse(response_byte=mcu_respond, cmd_hex=mcu_cmd).check_data_chksum_cmd()
                print("CHECK RESPONSE", check_response)
                # If MCU response contains errors, to stop the rest of the test
                if check_response:
                    # Append result if no errors in the response
                    dict_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()

                    result_list = [test_no]
                    for i in data_pos:
                        test_value = dict_val[i]
                        result_list.append(test_value)
                else:
                    run_stat = False
                    result_list = [test_no, False]
                    db_value = False
            else:
                run_stat = False
                result_list = [test_no, False]
                db_value = False
        else:
            run_stat = False
            result_list = [test_no, False]
            db_value = False
    else:
        run_stat = True
        result_list = [test_no, False]
        db_value = False

    return_list = [run_stat, result_list, db_value, con_test]

    return return_list


def mcu_check_mcu_status(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
                           mcu_resp_len, data_pos):
    # Extract MCU start false cmd
    mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                       data_zfill_list=[]).out_bytearray()

    mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                               data_val_list=[], data_zfill_list=[]).out_bytearray()

    # MCU communication
    try:
        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
            # Send MCU request
            ser.write(mcu_req_byte)

            # Get MCU respond
            mcu_respond = ser.read(int(mcu_resp_len))
    except SerialException:
        mcu_respond = False

    # If no respond from MCU, request for N times
    if not mcu_respond:
        i = 0
        while i < int(mcu_repetition_request):
            # MCU communication
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                    # Send MCU request
                    ser.write(mcu_req_byte)

                    # Get MCU respond
                    mcu_respond = ser.read(int(mcu_resp_len))
            except SerialException:
                mcu_respond = False

            if mcu_respond:
                break
            else:
                i += 1

        if not mcu_respond:
            mcu_respond = mcu_start_false_byte

    if mcu_respond == mcu_start_false_byte:
        mcu_respond = False
    else:
        check_response = CheckResponse(response_byte=mcu_respond,
                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
        # If MCU response contains errors, to stop the rest of the test
        if check_response:
            # Append result if no errors in the response
            data_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()
            if data_val.get('true_false'):
                mcu_respond = True
            else:
                mcu_respond = False
        else:
            mcu_respond = False

    return mcu_respond


def mcu_enable_sensor(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
                           mcu_resp_len, data_pos):
    """ Enabling sensor """

    # Extract MCU start false cmd
    mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                       data_zfill_list=[]).out_bytearray()

    mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                               data_val_list=[], data_zfill_list=[]).out_bytearray()

    try:
        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as \
                sensor_ser:
            # time.sleep(float(mcu_delay))
            # Send MCU request
            print("SENSOR ENABLE REQUEST", mcu_req_byte.hex())
            sensor_ser.write(mcu_req_byte)
            # Get MCU respond
            sensor_enable_respond = sensor_ser.read(int(mcu_resp_len))
    except SerialException:
        sensor_enable_respond = False
    if sensor_enable_respond:
        print("SENSOR ENABLE RESPOND", sensor_enable_respond.hex())
    else:
        print("SENSOR ENABLE RESPOND", sensor_enable_respond)

    # Retrying request
    if not sensor_enable_respond:
        i = 0
        while i < int(mcu_repetition_request):
            print("SENSOR ENABLE RE-REQUEST FOR " + str(i + 1) + " TIMES")
            print("SENSOR ENABLE REQUEST", mcu_req_byte.hex())
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8,
                            timeout=float(timeout_set)) as sensor_ser:
                    # time.sleep(float(mcu_delay))
                    # Send MCU request
                    sensor_ser.write(mcu_req_byte)
                    # Get MCU respond
                    sensor_enable_respond = sensor_ser.read(int(mcu_resp_len))
            except SerialException:
                sensor_enable_respond = False
            if sensor_enable_respond:
                print("SENSOR ENABLE RESPOND", sensor_enable_respond.hex())
                break
            else:
                print("SENSOR ENABLE RESPOND", sensor_enable_respond)
                i += 1

        if sensor_enable_respond:
            if sensor_enable_respond == mcu_start_false_byte:
                sensor_enable_respond = False
            else:
                pass
        else:
            sensor_enable_respond = False
    else:
        pass

    # If MCU returns False Start, to stop the rest of the test
    if sensor_enable_respond:
        check_response = CheckResponse(response_byte=sensor_enable_respond,
                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
        # If MCU response contains errors, to stop the rest of the test
        if check_response:
            # Append result if no errors in the response
            data_val = GetData(response_byte=sensor_enable_respond, data_pos=data_pos).split_data()
            if data_val.get('true_false'):
                sensor_enable_respond = True
                # sensor_enabled = True
            else:
                sensor_enable_respond = False
        else:
            sensor_enable_respond = False
    else:
        sensor_enable_respond = False

    return sensor_enable_respond


def mcu_check_sensor(test_no, mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request,
                     timeout_set, mcu_resp_len, data_pos):
    """ Send test item """
    run_stat = True

    # Extract MCU start false cmd
    mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                       data_zfill_list=[]).out_bytearray()

    mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                               data_val_list=[], data_zfill_list=[]).out_bytearray()
    try:
        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
            # time.sleep(float(mcu_delay))
            print('MCU REQUEST', mcu_req_byte.hex())
            # Send MCU request
            ser.write(mcu_req_byte)

            # Get MCU respond
            mcu_respond = ser.read(int(mcu_resp_len))
    except SerialException:
        mcu_respond = False

    if mcu_respond:
        print("MCU RESPOND", mcu_respond.hex())
    else:
        print("MCU RESPOND", mcu_respond)

    # Retrying request
    if not mcu_respond:
        i = 0
        while i < int(mcu_repetition_request):
            print("RE-REQUEST FOR " + str(i + 1) + " TIMES")
            print("MCU REQUEST", mcu_req_byte.hex())
            # MCU communication
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8,
                            timeout=float(timeout_set)) as ser:
                    # time.sleep(float(mcu_delay))

                    # Send MCU request
                    ser.write(mcu_req_byte)

                    # Get MCU respond
                    mcu_respond = ser.read(int(mcu_resp_len))
            except SerialException:
                mcu_respond = False

            if mcu_respond:
                print("MCU RESPOND", mcu_respond.hex())
            else:
                print("MCU RESPOND", mcu_respond)
            if mcu_respond:
                break
            else:
                i += 1

        if mcu_respond:
            if mcu_respond == mcu_start_false_byte:
                mcu_respond = False
            else:
                pass
        else:
            mcu_respond = False

    if mcu_respond:
        # If MCU returns False Start, to stop the rest of the test
        check_response = CheckResponse(response_byte=mcu_respond,
                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
        print("CHECK RESPONSE", check_response)
        # If MCU response contains errors, to stop the rest of the test
        if check_response:
            # Append result if no errors in the response
            dict_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()

            result_list = [test_no]
            for i in data_pos:
                test_value = dict_val[i]
                result_list.append(test_value)
        else:
            run_stat = False
            result_list = [test_no, False]
    else:
        run_stat = False
        result_list = [test_no, False]

    return run_stat, result_list


def mcu_turn_on_ecu_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
                          mcu_resp_len, data_pos):
    # Extract MCU start false cmd
    mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                       data_zfill_list=[]).out_bytearray()

    mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                               data_val_list=[], data_zfill_list=[]).out_bytearray()

    # MCU communication
    try:
        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
            # Send MCU request
            ser.write(mcu_req_byte)

            # Get MCU respond
            mcu_respond = ser.read(int(mcu_resp_len))
    except SerialException:
        mcu_respond = False

    # If no respond from MCU, request for N times
    if not mcu_respond:
        i = 0
        while i < int(mcu_repetition_request):
            # MCU communication
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                    # Send MCU request
                    ser.write(mcu_req_byte)

                    # Get MCU respond
                    mcu_respond = ser.read(int(mcu_resp_len))
            except SerialException:
                mcu_respond = False

            if mcu_respond:
                break
            else:
                i += 1

        if not mcu_respond:
            mcu_respond = mcu_start_false_byte

    if mcu_respond == mcu_start_false_byte:
        mcu_respond = False
    else:
        check_response = CheckResponse(response_byte=mcu_respond,
                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
        # If MCU response contains errors, to stop the rest of the test
        if check_response:
            # Append result if no errors in the response
            data_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()
            if data_val.get('true_false'):
                mcu_respond = True
            else:
                mcu_respond = False
        else:
            mcu_respond = False

    return mcu_respond


def mcu_turn_off_ecu_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
                           mcu_resp_len, data_pos):
    # Extract MCU start false cmd
    mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                       data_zfill_list=[]).out_bytearray()

    mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                               data_val_list=[], data_zfill_list=[]).out_bytearray()

    # MCU communication
    try:
        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
            # Send MCU request
            ser.write(mcu_req_byte)

            # Get MCU respond
            mcu_respond = ser.read(int(mcu_resp_len))
    except SerialException:
        mcu_respond = False

    # If no respond from MCU, request for N times
    if not mcu_respond:
        i = 0
        while i < int(mcu_repetition_request):
            # MCU communication
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                    # Send MCU request
                    ser.write(mcu_req_byte)

                    # Get MCU respond
                    mcu_respond = ser.read(int(mcu_resp_len))
            except SerialException:
                mcu_respond = False

            if mcu_respond:
                break
            else:
                i += 1

        if not mcu_respond:
            mcu_respond = mcu_start_false_byte

    if mcu_respond == mcu_start_false_byte:
        mcu_respond = False
    else:
        check_response = CheckResponse(response_byte=mcu_respond,
                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
        # If MCU response contains errors, to stop the rest of the test
        if check_response:
            # Append result if no errors in the response
            data_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()
            if data_val.get('true_false'):
                mcu_respond = True
            else:
                mcu_respond = False
        else:
            mcu_respond = False

    return mcu_respond


def mcu_turn_on_rev_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
                          mcu_resp_len, data_pos):
    # Extract MCU start false cmd
    mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                       data_zfill_list=[]).out_bytearray()

    mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                               data_val_list=[], data_zfill_list=[]).out_bytearray()

    # MCU communication
    try:
        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
            # Send MCU request
            ser.write(mcu_req_byte)

            # Get MCU respond
            mcu_respond = ser.read(int(mcu_resp_len))
    except SerialException:
        mcu_respond = False

    # If no respond from MCU, request for N times
    if not mcu_respond:
        i = 0
        while i < int(mcu_repetition_request):
            # MCU communication
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                    # Send MCU request
                    ser.write(mcu_req_byte)

                    # Get MCU respond
                    mcu_respond = ser.read(int(mcu_resp_len))
            except SerialException:
                mcu_respond = False

            if mcu_respond:
                break
            else:
                i += 1

        if not mcu_respond:
            mcu_respond = mcu_start_false_byte

    if mcu_respond == mcu_start_false_byte:
        mcu_respond = False
    else:
        check_response = CheckResponse(response_byte=mcu_respond,
                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
        # If MCU response contains errors, to stop the rest of the test
        if check_response:
            # Append result if no errors in the response
            data_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()
            if data_val.get('true_false'):
                mcu_respond = True
            else:
                mcu_respond = False
        else:
            mcu_respond = False

    return mcu_respond


def mcu_turn_off_rev_power(mcu_cmd, mcu_port, mcu_baud_rate, mcu_start_false_cmd, mcu_repetition_request, timeout_set,
                           mcu_resp_len, data_pos):
    # Extract MCU start false cmd
    mcu_start_false_byte = RequestByte(command_val=f'{mcu_start_false_cmd}', char_cond=False, data_val_list=[],
                                       data_zfill_list=[]).out_bytearray()

    mcu_req_byte = RequestByte(command_val=f'{mcu_cmd}', char_cond=False,
                               data_val_list=[], data_zfill_list=[]).out_bytearray()

    # MCU communication
    try:
        with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
            # Send MCU request
            ser.write(mcu_req_byte)

            # Get MCU respond
            mcu_respond = ser.read(int(mcu_resp_len))
    except SerialException:
        mcu_respond = False

    # If no respond from MCU, request for N times
    if not mcu_respond:
        i = 0
        while i < int(mcu_repetition_request):
            # MCU communication
            try:
                with Serial(mcu_port, int(mcu_baud_rate), bytesize=8, timeout=float(timeout_set)) as ser:
                    # Send MCU request
                    ser.write(mcu_req_byte)

                    # Get MCU respond
                    mcu_respond = ser.read(int(mcu_resp_len))
            except SerialException:
                mcu_respond = False

            if mcu_respond:
                break
            else:
                i += 1

        if not mcu_respond:
            mcu_respond = mcu_start_false_byte

    if mcu_respond == mcu_start_false_byte:
        mcu_respond = False
    else:
        check_response = CheckResponse(response_byte=mcu_respond,
                                       cmd_hex=mcu_cmd).check_data_chksum_cmd()
        # If MCU response contains errors, to stop the rest of the test
        if check_response:
            # Append result if no errors in the response
            data_val = GetData(response_byte=mcu_respond, data_pos=data_pos).split_data()
            if data_val.get('true_false'):
                mcu_respond = True
            else:
                mcu_respond = False
        else:
            mcu_respond = False

    return mcu_respond