import os
import sys
import stat
import shutil
import logging
import traceback
from winreg import *
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox

ROAMING_PATH = os.getenv('APPDATA')
SOFTWARE_PATH = ROAMING_PATH + "/RPAS Simulator/"
VERSION_INFO_TXTFILE_PATH = SOFTWARE_PATH + "version_info.txt"


def extract_textfile():
    appid = ""
    stats = ""
    current_ver = ""
    installer_ver = ""
    with open(VERSION_INFO_TXTFILE_PATH, "r") as file:
        appid_str = "App Id"
        status_str = "Status"
        current_version_str = "Current version"
        installer_version_str = "Installer version"

        for line in file:
            if appid_str in line:
                appid = line.split("=")
                appid = appid[-1].replace("\n", "")
            elif status_str in line:
                stats = line.split("=")
                stats = stats[-1].replace("\n", "")
            elif current_version_str in line:
                current_ver = line.split("=")
                current_ver = current_ver[-1].replace("\n", "")
            elif installer_version_str in line:
                installer_ver = line.split("=")
                installer_ver = installer_ver[-1].replace("\n", "")

    return appid, stats, current_ver, installer_ver


def check_reformat(current_ver, installer_ver, install_path):
    print(install_path)
    rformat_data_files = False
    if current_ver == "1.0.0" and installer_ver == "1.0.1":
        rformat_data_files = True
        # update_product_model_format(install_path)  Example of function call if update any data files format
        pass
    else:
        pass

    return rformat_data_files


def update_product_model_format(install_path):
    # Read available data
    model_list = os.listdir(install_path + "config/vehicle/")
    print(model_list)


def delete_textfile():
    try:
        os.remove(VERSION_INFO_TXTFILE_PATH)
    except FileNotFoundError:
        pass


def remove_error(func, path, exc_info):
    print('Handling Error for file ', path)
    print(exc_info)
    # Check if file access issue
    if not os.access(path, os.W_OK):
        # Try to change the permision of file
        os.chmod(path, stat.S_IWUSR)
        # call the calling function again
        func(path)


def delete_temp():
    # path
    path = os.path.join(SOFTWARE_PATH, "temp")
    print(path)

    # removing directory
    shutil.rmtree(path, ignore_errors=True, onerror=remove_error)


def locate_installed_path(appid):
    path = fr"SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{appid}_is1"
    a_reg = ConnectRegistry(None, HKEY_LOCAL_MACHINE)
    try:
        a_key = OpenKey(a_reg, path, 0, KEY_READ)
        installed_p = QueryValueEx(a_key, "InstallLocation")[0]
        a_key.Close()
    except WindowsError:
        installed_p = False

    a_reg.Close()

    return installed_p


def show_alert_popup(msg_val):
    app = QtWidgets.QApplication([])
    msg = QMessageBox()
    msg.setWindowTitle("Warning")
    msg.setText(msg_val)
    msg.setIcon(QMessageBox.Warning)
    msg.exec_()
    app.quit()


def remove_data_files(installed_p):
    folder_data_files = ["config", "img", "logs"]
    for folder in folder_data_files:
        # path
        path = f'{installed_p}{folder}'

        # removing directory
        shutil.rmtree(path, ignore_errors=True)


def move_existing_data_files(installed_p):
    folder_data_files = ["config", "img", "logs"]

    for folder in folder_data_files:
        src = f'{SOFTWARE_PATH}temp/old/{folder}'
        dest = f'{installed_p}{folder}'
        shutil.move(src, dest)


def move_fresh_data_files(installed_p):
    folder_data_files = ["config", "img", "logs"]

    for folder in folder_data_files:
        src = f'{SOFTWARE_PATH}temp/updated/{folder}'
        dest = f'{installed_p}{folder}'
        shutil.move(src, dest)


def excepthook(exc_type, exc_value, exc_tb):
    error_msg = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    logging.error(error_msg)


def init_log():
    log_format = "[%(asctime)s] %(levelname)s - %(funcName)s:%(lineno)d - %(message)s"
    filename = f"{SOFTWARE_PATH}/system_log.log"
    logging.basicConfig(filename=filename, filemode='a', format=log_format,
                        datefmt='%d-%m-%y %H:%M:%S', level=logging.INFO)


if __name__ == "__main__":

    # Log runtime exception
    sys.excepthook = excepthook

    print("Extract txtfile")
    # Extract textfile
    app_id, status, current_version, installer_version = extract_textfile()
    print("Locate path")
    # Locate installed path
    installed_path = locate_installed_path(app_id)
    if not installed_path:
        show_alert_popup("Installed path not found.")
    # Process update/write data files
    if status == "Update" and installed_path:
        reformat_data_files = check_reformat(current_version, installer_version, installed_path)
        # Remove data files in installed path
        remove_data_files(installed_path)
        # Move existing data in temp to installed path
        print("Move existing")
        move_existing_data_files(installed_path)
    elif status == "New" and installed_path:
        print("Move fresh")
        # move_fresh_data_files(installed_path)

    print("Delete")
    # Delete text file
    delete_textfile()

    # Delete temp folder
    delete_temp()
